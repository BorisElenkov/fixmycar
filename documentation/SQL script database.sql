-- --------------------------------------------------------
-- Хост:                         127.0.0.1
-- Server version:               5.5.53-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Версия:              9.3.0.4984
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping database structure for smart_garage
CREATE DATABASE IF NOT EXISTS `smart_garage` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `smart_garage`;


-- Dumping structure for table smart_garage.cars
CREATE TABLE IF NOT EXISTS `cars` (
  `car_id` int(11) NOT NULL AUTO_INCREMENT,
  `model_id` int(11) NOT NULL,
  `license_plate` varchar(8) NOT NULL,
  `vin` varchar(17) NOT NULL,
  `year` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`car_id`),
  KEY `cars_users__fk` (`user_id`),
  KEY `cars_models__fk` (`model_id`),
  CONSTRAINT `cars_models__fk` FOREIGN KEY (`model_id`) REFERENCES `models` (`model_id`),
  CONSTRAINT `cars_users__fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=latin1;

-- Dumping data for table smart_garage.cars: ~38 rows (approximately)
/*!40000 ALTER TABLE `cars` DISABLE KEYS */;
INSERT INTO `cars` (`car_id`, `model_id`, `license_plate`, `vin`, `year`, `user_id`) VALUES
	(2, 1, 'CA5487CB', 'HGFBT93586XL6MERT', 2010, 28),
	(3, 2, 'CA2354CB', '5474493586XL6MERT', 2012, 29),
	(4, 3, 'CA3750CB', 'JGYFD93586XL6MERT', 2015, 30),
	(5, 4, 'CA3906CB', 'KYKJG93586XL6MERT', 2020, 3),
	(6, 5, 'CA3759CB', '5GIFY93586XL6MERT', 2018, 3),
	(7, 6, 'CA3396CB', '5UGGT93586XL6MERT', 2019, 3),
	(8, 17, 'CA1245AS', '5UMBT93586XL6MERT', 2005, 7),
	(9, 28, 'CB1575DS', '5UMBT935GGGL6MERT', 2001, 8),
	(10, 24, 'CA4377BB', '5R3BT93586XL6MERT', 2007, 9),
	(11, 14, 'BP8577BB', '5UMCL93491G7LBLWU', 2007, 10),
	(12, 12, 'CA1775AS', '6BMBT93586XL6MERT', 2000, 11),
	(13, 27, 'CB4775AS', '875BT93586XL6MERT', 2011, 12),
	(14, 12, 'M1145BB', 'WERDT93586XL6MERT', 2015, 13),
	(15, 20, 'CA5577HA', '444BT93586XL6MERT', 2015, 14),
	(16, 10, 'CA5757DD', 'JHGBT93586XL6MERT', 2003, 15),
	(17, 30, 'CA1575RR', 'HJTRT93586XL6MERT', 2011, 16),
	(18, 2, 'CA5757TR', 'PORFT93586XL6MERT', 2017, 17),
	(19, 6, 'CB1774BB', 'OKYTT93586XL6MERT', 2015, 18),
	(20, 25, 'BH5444BB', '578BT93586XL6MERT', 2000, 19),
	(21, 16, 'M7777BB', '575SFDJ877SDD777F', 2004, 20),
	(22, 11, 'CA5784OO', 'PORST93586XL6MERT', 2007, 21),
	(23, 21, 'CA5474AD', 'DASBT93586XL6MERT', 2008, 22),
	(24, 31, 'CA1277FD', 'GFTBT93586XL6MERT', 2009, 23),
	(25, 1, 'CA1266TT', '784BT93586XL6MERT', 2014, 24),
	(26, 18, 'CA2255DD', 'IEABT93586XL6MERT', 2015, 25),
	(27, 15, 'CA9988AS', 'HHGBT93586XL6MERT', 2012, 26),
	(28, 1, 'CA1745AS', '577BT93586XL6MERT', 2001, 26),
	(29, 16, 'M1254BB', 'MMMBT93586XL6MERT', 2017, 27),
	(30, 29, 'CA1121KK', 'BBBBT93586XL6MERT', 2000, 28),
	(31, 13, 'CA2525BB', 'RRRBT93586XL6MERT', 2007, 29),
	(32, 8, 'CB5545AC', 'KKASFDJ877SDD777F', 2015, 30),
	(33, 22, 'M7877BB', 'JHY1BT93586XL6MER', 2008, 31),
	(34, 5, 'CA1245AE', 'HJKBT93586XL6MERT', 2000, 22),
	(35, 23, 'C1313AS', '575BT93586XL6MERT', 1999, 23),
	(36, 18, 'C7121KK', '6TFBT93586XL6MERT', 2011, 24),
	(37, 14, 'C8245AS', 'NBVBT93586XL6MERT', 2017, 25),
	(38, 19, 'C9745AB', 'GBABT93586XL6MERT', 2003, 26),
	(40, 4, 'M1111CC', 'ASDFGHJKLZXCVBNJH', 2020, 5);
/*!40000 ALTER TABLE `cars` ENABLE KEYS */;


-- Dumping structure for table smart_garage.currencies
CREATE TABLE IF NOT EXISTS `currencies` (
  `currency_id` int(11) NOT NULL AUTO_INCREMENT,
  `code` enum('EUR','USD','JPY','GBP','AUD','CAD','CHF','CNY','SEK','MXN','NZD','SGD','HKD','NOK','KRW','BGN') NOT NULL,
  PRIMARY KEY (`currency_id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

-- Dumping data for table smart_garage.currencies: ~16 rows (approximately)
/*!40000 ALTER TABLE `currencies` DISABLE KEYS */;
INSERT INTO `currencies` (`currency_id`, `code`) VALUES
	(1, 'EUR'),
	(2, 'USD'),
	(3, 'JPY'),
	(4, 'GBP'),
	(5, 'AUD'),
	(6, 'CAD'),
	(7, 'CHF'),
	(8, 'CNY'),
	(9, 'SEK'),
	(10, 'MXN'),
	(11, 'NZD'),
	(12, 'SGD'),
	(13, 'HKD'),
	(14, 'NOK'),
	(15, 'KRW'),
	(16, 'BGN');
/*!40000 ALTER TABLE `currencies` ENABLE KEYS */;


-- Dumping structure for table smart_garage.levels
CREATE TABLE IF NOT EXISTS `levels` (
  `level_id` int(11) NOT NULL AUTO_INCREMENT,
  `level_name` enum('START','BRONZE','SILVER','GOLD') NOT NULL,
  `discount` int(11) NOT NULL,
  `min_expenditure` double NOT NULL,
  PRIMARY KEY (`level_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Dumping data for table smart_garage.levels: ~4 rows (approximately)
/*!40000 ALTER TABLE `levels` DISABLE KEYS */;
INSERT INTO `levels` (`level_id`, `level_name`, `discount`, `min_expenditure`) VALUES
	(1, 'START', 0, 0),
	(2, 'BRONZE', 5, 1000),
	(3, 'SILVER', 10, 5000),
	(4, 'GOLD', 15, 10000);
/*!40000 ALTER TABLE `levels` ENABLE KEYS */;


-- Dumping structure for table smart_garage.makes
CREATE TABLE IF NOT EXISTS `makes` (
  `make_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  PRIMARY KEY (`make_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

-- Dumping data for table smart_garage.makes: ~10 rows (approximately)
/*!40000 ALTER TABLE `makes` DISABLE KEYS */;
INSERT INTO `makes` (`make_id`, `name`) VALUES
	(1, 'Toyota'),
	(2, 'Ford'),
	(3, 'Hyundai'),
	(4, 'Opel'),
	(5, 'VW'),
	(6, 'Audi'),
	(7, 'BMW'),
	(8, 'Fiat'),
	(9, 'Peugeot'),
	(10, 'Mazda');
/*!40000 ALTER TABLE `makes` ENABLE KEYS */;


-- Dumping structure for table smart_garage.models
CREATE TABLE IF NOT EXISTS `models` (
  `model_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `make_id` int(11) NOT NULL,
  PRIMARY KEY (`model_id`),
  KEY `models_makes__fk` (`make_id`),
  CONSTRAINT `models_makes__fk` FOREIGN KEY (`make_id`) REFERENCES `makes` (`make_id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=latin1;

-- Dumping data for table smart_garage.models: ~30 rows (approximately)
/*!40000 ALTER TABLE `models` DISABLE KEYS */;
INSERT INTO `models` (`model_id`, `name`, `make_id`) VALUES
	(1, 'Corolla', 1),
	(2, 'Yaris', 1),
	(3, 'Avensis', 1),
	(4, 'Focus', 2),
	(5, 'Fiesta', 2),
	(6, 'Mondeo', 2),
	(8, 'Elantra', 3),
	(9, 'Accent', 3),
	(10, 'Getz', 3),
	(11, 'Astra', 4),
	(12, 'Corsa', 4),
	(13, 'Vectra', 4),
	(14, 'Golf', 5),
	(15, 'Passat', 5),
	(16, 'Polo', 5),
	(17, 'A3', 6),
	(18, 'A4', 6),
	(19, 'A6', 6),
	(20, '320', 7),
	(21, '525', 7),
	(22, 'X5', 7),
	(23, 'Tipo', 8),
	(24, 'Punto', 8),
	(25, 'Bravo', 8),
	(26, '307SW', 9),
	(27, '206CC', 9),
	(28, '207', 9),
	(29, '323', 10),
	(30, '626', 10),
	(31, '121', 10);
/*!40000 ALTER TABLE `models` ENABLE KEYS */;


-- Dumping structure for table smart_garage.repairs
CREATE TABLE IF NOT EXISTS `repairs` (
  `repair_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `price` float(9,2) NOT NULL,
  `is_active` tinyint(4) NOT NULL,
  PRIMARY KEY (`repair_id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;

-- Dumping data for table smart_garage.repairs: ~21 rows (approximately)
/*!40000 ALTER TABLE `repairs` DISABLE KEYS */;
INSERT INTO `repairs` (`repair_id`, `name`, `price`, `is_active`) VALUES
	(1, 'Oil change', 300.00, 1),
	(2, 'Filter replacement', 120.00, 1),
	(3, 'Serpentine belt inspection', 100.00, 1),
	(4, 'Wheel alignment', 85.00, 1),
	(5, 'Tire pressure check', 50.00, 1),
	(6, 'Brake fluid exchange', 150.00, 0),
	(7, 'Car Wash', 50.00, 0),
	(8, 'Test repair', 50.00, 1),
	(9, 'Change the water pump', 100.00, 1),
	(10, 'Replace all the hose pipes', 250.00, 1),
	(11, 'Change the air filter', 30.00, 1),
	(12, 'Cooling system checking', 50.00, 1),
	(13, 'Checking the functioning of ABS', 100.00, 1),
	(14, 'Gearbox oil topping up', 150.00, 1),
	(15, 'Service lights resetting', 100.00, 1),
	(16, 'Inspecting brake pads', 100.00, 1),
	(17, 'Testing antifreeze strength', 20.00, 1),
	(18, 'Checking mirrors', 20.00, 1),
	(19, 'Checking shock absorber condition', 150.00, 1),
	(20, 'Inspecting hand brakes', 100.00, 1),
	(21, 'Checking power steering fluid', 50.00, 1);
/*!40000 ALTER TABLE `repairs` ENABLE KEYS */;


-- Dumping structure for table smart_garage.roles
CREATE TABLE IF NOT EXISTS `roles` (
  `role_id` int(11) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(15) NOT NULL,
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table smart_garage.roles: ~2 rows (approximately)
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` (`role_id`, `role_name`) VALUES
	(1, 'Customer'),
	(2, 'Employee');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;


-- Dumping structure for table smart_garage.statuses
CREATE TABLE IF NOT EXISTS `statuses` (
  `status_id` int(11) NOT NULL AUTO_INCREMENT,
  `status_name` enum('NOT_STARTED','IN_PROGRESS','READY_FOR_PICKUP','COMPLETED','CANCELLED') NOT NULL,
  PRIMARY KEY (`status_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Dumping data for table smart_garage.statuses: ~5 rows (approximately)
/*!40000 ALTER TABLE `statuses` DISABLE KEYS */;
INSERT INTO `statuses` (`status_id`, `status_name`) VALUES
	(1, 'NOT_STARTED'),
	(2, 'IN_PROGRESS'),
	(3, 'READY_FOR_PICKUP'),
	(4, 'COMPLETED'),
	(5, 'CANCELLED');
/*!40000 ALTER TABLE `statuses` ENABLE KEYS */;


-- Dumping structure for table smart_garage.tokens
CREATE TABLE IF NOT EXISTS `tokens` (
  `token_Id` int(11) NOT NULL AUTO_INCREMENT,
  `token` varchar(30) NOT NULL,
  `user_id` int(11) NOT NULL,
  `time_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`token_Id`),
  KEY `tokens_users_user_id_fk` (`user_id`),
  CONSTRAINT `tokens_users_user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- Dumping data for table smart_garage.tokens: ~0 rows (approximately)
/*!40000 ALTER TABLE `tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `tokens` ENABLE KEYS */;


-- Dumping structure for table smart_garage.users
CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(20) NOT NULL,
  `last_name` varchar(20) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(60) NOT NULL,
  `phone` varchar(15) NOT NULL,
  `email` varchar(35) NOT NULL,
  `level_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `users_pk2` (`username`),
  KEY `users_levels__fk` (`level_id`),
  KEY `users_roles__fk` (`role_id`),
  CONSTRAINT `users_levels__fk` FOREIGN KEY (`level_id`) REFERENCES `levels` (`level_id`),
  CONSTRAINT `users_roles__fk` FOREIGN KEY (`role_id`) REFERENCES `roles` (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=latin1;

-- Dumping data for table smart_garage.users: ~34 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`user_id`, `first_name`, `last_name`, `username`, `password`, `phone`, `email`, `level_id`, `role_id`) VALUES
	(2, 'Ivan', 'Ivanov', 'ivan', '$2a$12$TSxUCmTl7PWOu/mG7MFgq.sOc5pQsjE1sMc5XPSduXL1ZMQrdAjvG', '0888888811', 'ivan@mail.bg', 1, 2),
	(3, 'Petar', 'Petrov', 'petar', '$2a$12$AXf3o89ydCwCdOTIkQoRKOwqSjmdxdpftjgRnTl3ZjwYwSKUyKKc.', '0899999999', 'petar@mail.bg', 1, 1),
	(5, 'Albena', 'Alexandrova', 'Albena7115', '$2a$12$PlO9n55v7KQkUaS6LNNnKO8.802vmpNJQe41WMnGTQ6Vz2FBgcx4G', '0885227067', 'smiling_stars@mail.bg', 2, 1),
	(7, 'Georgi', 'Ivanov', 'Georgi3010', '$2a$12$vc94/KZl/B1o60gtIQqrh./0bVlq3uiRnAjKu2dM.JYvQt1nAEO5u', '0887778877', 'georgi_iv@mail.bg', 1, 1),
	(8, 'Petar', 'Donkov', 'Petar1537', '$2a$12$uJ5VGJyQaQaZk8aR.rH7qehKgtH9HHt9mapO9aK5qAWYNB9M4OYNG', '0879884455', 'petar90@abv.bg', 1, 1),
	(9, 'Mario', 'Vaniov', 'Mario9784', '$2a$12$T2FiLhsKeHkpTDbheqEq7O.6HiP5yqv8e.0cFxKm2HH0JAQx691F2', '0897445544', 'vaniov00@gmail.com', 1, 1),
	(10, 'Teodosi', 'Lilov', 'Teodosi9822', '$2a$12$x.co53WSokF6gjTxkqKAveWlntlDHfGDG/LflCsqJs.ErCZEaY8fu', '0887998899', 'lilov_dos@abv.bg', 1, 1),
	(11, 'Gavril', 'Genov', 'Gavril1400', '$2a$12$46KwUK6z4mnfWmLN6IUHiOaJTUqfWjbpOkfY66yz4DRyN3hF/rLUC', '0877774475', 'gavril_gg@mail.bg', 1, 1),
	(12, 'Maia', 'Ivanova', 'Maia3584', '$2a$12$ERBn.80U..Ks/tXDfBXCPeyUtyGTO7BZUT4N2R8VPc/NruL8laAW6', '0897887744', 'maia_ivanova77@abv.bg', 1, 1),
	(13, 'Lili', 'Borisova', 'Lili7420', '$2a$12$mecI5GQm2NXDsJd6Q1YTKeLt9VVbF1XyPiW8QWcuLDbjvS9v3rVN.', '0887654544', 'borisova_lili@mail.bg', 1, 1),
	(14, 'Diman', 'Donchev', 'Diman3192', '$2a$12$wmAHDkjPCWSAZ6w0BSLQ6.rEFbqKyq4CaXZs/SoipxsJaEABOhKl6', '0897854545', 'donchev_dim@abv.bg', 1, 1),
	(15, 'Branko', 'Kirov', 'Branko4814', '$2a$12$GMRlV7SoZZLZtI2OkeSXGOixKvREhqo./S.tE2j/Gw98705eO9kje', '0877112212', 'brani_tin7@abv.bg', 1, 1),
	(16, 'Milena', 'Kikova', 'Milena1674', '$2a$12$HNaBV8GqJg9k72fMvEM6hO8CQRsRhXsN2Gv36Qd3.O5YfyFSbVVEi', '0887544455', 'kikova_sun@abv.bg', 1, 1),
	(17, 'Rosen', 'Gekov', 'Rosen3821', '$2a$12$SJ2ZjrzpmF5FyhZd82YXueh/58LehTcvnzIFlkOu2W7e.5bJnXE22', '08775123421', 'rosen_gekov80@gmail.com', 1, 1),
	(18, 'Miroslav', 'Milev', 'Miroslav8608', '$2a$12$Vp4EUIY9V38maGmoNiE.a.1nMUUXFdCFYLIVMNmBX7CI4ussWimPG', '0885441425', 'milev_miroslav@gmail.com', 1, 1),
	(19, 'Genadi', 'Denkov', 'Genadi1579', '$2a$12$jO6uaE5ksIjKVnJUtrutEupFlIm26fDJ0yIxmNheeppIjiGCA5R82', '0885444141', 'gen_denkov22@mail.bg', 1, 1),
	(20, 'Svilen', 'Trifonov', 'Svilen8521', '$2a$12$4NJB0nk9mzDgUL5b9TdZpOWGHURqZ9XZPM5QUBGbEQTOmTcGBIQ/O', '0897445543', 'svilen1987@mail.bg', 1, 1),
	(21, 'Venelin', 'Marinov', 'Venelin3830', '$2a$12$R1rVQpgalwtDgt17e/bBXunFiahaQ0RE1..l6ssjoBq4IcNpHoufi', '0878326854', 'venelin_mar8@abv.bg', 1, 1),
	(22, 'Mirko', 'Kostov', 'Mirko3043', '$2a$12$RfapQ2H1W3t.Vq3TfaBkC.17whAK0ArTpdSNh8rS7iwKBiyoi3rTi', '0897478745', 'kostov_mirko@mail.bg', 1, 1),
	(23, 'Bonko', 'Donev', 'Bonko4755', '$2a$12$SZ.zNidDICRw4hynfzbR8uwvB6RcUtwR60TE8lwI8kax6nl2ScV5S', '0879554411', 'bonko_bonko@gmail.bg', 1, 1),
	(24, 'Denica', 'Gerasimova', 'Denica3023', '$2a$12$PLXX6lUiYzU8F4tMBRPIceQi9TbfI4zCtmo3Yje24sCmvarpMqgZy', '0887459754', 'gerasimova111@abv.bg', 1, 1),
	(25, 'Milcho', 'Ivanov', 'Milcho8190', '$2a$12$Gu2PWYQ80qfGap.dBhgpTemqKTO8hdhWZaG6d5GtSBfeRAfIW4YWO', '0884127495', 'milcho_super@abv.bg', 1, 1),
	(26, 'Denislav', 'Derev', 'Denislav4627', '$2a$12$vrodlVrKI1SsPFapsoWQwuG6fAyfT37gYhjRj1W2/caJkvfYfWJw.', '0884567511', 'derev_denislav@mail.bg', 1, 1),
	(27, 'Stilian', 'Denchev', 'Stilian9481', '$2a$12$KUBQK8uZOcGyjjsTZl6tV.4v5d4kYtxi15oz/XBsPeOlWamniD..2', '0875125125', 'denchev_stil@mail.bg', 1, 1),
	(28, 'Sashko', 'Kemov', 'Sashko6876', '$2a$12$JnRG9UdqkPCCjhnohOpjg.7zOF1vsBSQMr3Q.c/yTLo8u72Yk2IUK', '0879685412', 'kemov_sashko@abv.bg', 1, 1),
	(29, 'Toshko', 'Penov', 'Toshko7158', '$2a$12$yqdJAbMVU84BZQMKykLq4uSybk.wvvzXxcKNgTQAiaYgI/6tf7t6e', '0874545454', 'penovpenov@abv.bg', 2, 1),
	(30, 'Plamena', 'Gergova', 'Plamena8769', '$2a$12$UVybk.coesAZrsFSDPNXxe7lC1iWvaO4ZkbV8IrmxI9dKIvXowYL.', '0899363626', 'gergova.88@abv.bg', 2, 1),
	(31, 'Martin', 'Martinov', 'Martin3964', '$2a$12$lTYnRegm20wmQGj7Ff/7ve.p2U29hcWj3Hj1/QrTlHvXJ93wXMUTW', '0899747457', 'martin_mmm@mail.bg', 1, 1),
	(32, 'Mirian', 'Iliev', 'Mirian4820', '$2a$12$O5Q1t/p7Vt9kJ0/0bn67CuNIvTSnNQfho0jusPhIZiioApO3mMBrO', '0887366524', 'mirian_iliev@abv.bg', 1, 2),
	(33, 'Boiko', 'Hristov', 'Boiko3048', '$2a$12$drWvv2rhouIz6hVjce8qFuK6TpfTgzfx0bRcIVPFnkpS6cpUHeGqm', '0886577871', 'boiko_hr1@abv.bg', 1, 2),
	(34, 'Stanimir', 'Gigov', 'Stanimir2400', '$2a$12$HgR/2..5edgQch5Sg5a./.eclz05OtRak1DlYOMRLdQpH2U8B.4X2', '0897633565', 'stanimir_gt@mail.bg', 1, 2),
	(35, 'Stefan', 'Hristov', 'Stefan1992', '$2a$12$k8yfy4CKicNRp1cqGg/cmej0s4F9lZr.aMLRp.CVEMGSHX403Rx3a', '0897365412', 'stefanhristov@gmail.com', 1, 2),
	(36, 'Biser', 'Miletiev', 'Biser6962', '$2a$12$iI1s7wP.fe7UPOgYnqY.b.D8IqeKeTY7Q.yslMb6po.exF5Gt1xbi', '0877447441', 'miletiev_80@abv.bg', 1, 2),
	(37, 'Bogdan', 'Bogdanov', 'Bogdan6223', '$2a$12$4wuyX55zk1PR54DIV./OVeO.8B7RkPJn52eLpt8gDebKZCsc3wypm', '0888888866', 'bogdan@mail.bg', 1, 1);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;


-- Dumping structure for table smart_garage.visits
CREATE TABLE IF NOT EXISTS `visits` (
  `visit_id` int(11) NOT NULL AUTO_INCREMENT,
  `car_id` int(11) NOT NULL,
  `status_id` int(11) NOT NULL,
  `visit_date` date NOT NULL,
  PRIMARY KEY (`visit_id`),
  KEY `visits_cars__fk` (`car_id`),
  KEY `visits_statuses__fk` (`status_id`),
  CONSTRAINT `visits_cars__fk` FOREIGN KEY (`car_id`) REFERENCES `cars` (`car_id`),
  CONSTRAINT `visits_statuses__fk` FOREIGN KEY (`status_id`) REFERENCES `statuses` (`status_id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=latin1;

-- Dumping data for table smart_garage.visits: ~20 rows (approximately)
/*!40000 ALTER TABLE `visits` DISABLE KEYS */;
INSERT INTO `visits` (`visit_id`, `car_id`, `status_id`, `visit_date`) VALUES
	(2, 2, 4, '2023-01-11'),
	(3, 3, 4, '2023-02-14'),
	(4, 4, 4, '2023-03-14'),
	(5, 5, 4, '2023-04-03'),
	(6, 5, 4, '2023-04-11'),
	(7, 3, 4, '2023-04-12'),
	(8, 3, 4, '2023-04-12'),
	(16, 13, 4, '2023-04-12'),
	(17, 14, 4, '2023-04-13'),
	(18, 15, 5, '2023-04-13'),
	(21, 18, 4, '2023-04-14'),
	(22, 19, 4, '2023-04-14'),
	(23, 20, 3, '2023-04-18'),
	(24, 30, 3, '2023-04-18'),
	(25, 31, 3, '2023-04-18'),
	(26, 32, 3, '2023-04-21'),
	(27, 33, 2, '2023-04-21'),
	(28, 34, 1, '2023-04-25'),
	(32, 35, 1, '2023-04-27'),
	(33, 40, 4, '2023-04-28');
/*!40000 ALTER TABLE `visits` ENABLE KEYS */;


-- Dumping structure for table smart_garage.visits_history
CREATE TABLE IF NOT EXISTS `visits_history` (
  `visits_history_id` int(11) NOT NULL AUTO_INCREMENT,
  `visit_id` int(11) NOT NULL,
  `repair_name` varchar(50) NOT NULL,
  `price` float(9,2) NOT NULL,
  `repair_id` int(11) NOT NULL,
  PRIMARY KEY (`visits_history_id`),
  KEY `visits_history_visits__fk` (`visit_id`),
  CONSTRAINT `visits_history_visits__fk` FOREIGN KEY (`visit_id`) REFERENCES `visits` (`visit_id`)
) ENGINE=InnoDB AUTO_INCREMENT=96 DEFAULT CHARSET=latin1;

-- Dumping data for table smart_garage.visits_history: ~54 rows (approximately)
/*!40000 ALTER TABLE `visits_history` DISABLE KEYS */;
INSERT INTO `visits_history` (`visits_history_id`, `visit_id`, `repair_name`, `price`, `repair_id`) VALUES
	(6, 4, 'Oil change', 290.00, 1),
	(7, 4, 'Serpentine belt inspection', 100.00, 3),
	(8, 4, 'Tire pressure check', 50.00, 5),
	(9, 5, 'Oil change', 300.00, 1),
	(10, 6, 'Oil change', 300.00, 1),
	(11, 21, 'Oil change', 300.00, 1),
	(12, 21, 'Filter replacement', 120.00, 2),
	(13, 21, 'Serpentine belt inspection', 100.00, 3),
	(14, 22, 'Oil change', 300.00, 1),
	(15, 22, 'Serpentine belt inspection', 100.00, 3),
	(16, 22, 'Tire pressure check', 50.00, 5),
	(26, 24, 'Tire pressure check', 50.00, 5),
	(27, 24, 'Wheel alignment', 85.00, 4),
	(34, 26, 'Serpentine belt inspection', 100.00, 3),
	(35, 26, 'Filter replacement', 120.00, 2),
	(36, 26, 'Change the water pump', 100.00, 9),
	(37, 26, 'Replace all the hose pipes', 250.00, 10),
	(38, 26, 'Checking the functioning of ABS', 95.00, 13),
	(39, 26, 'Gearbox oil topping up', 142.50, 14),
	(40, 26, 'Inspecting brake pads', 95.00, 16),
	(41, 26, 'Checking shock absorber condition', 142.50, 19),
	(42, 26, 'Inspecting hand brakes', 95.00, 20),
	(43, 27, 'Serpentine belt inspection', 95.00, 3),
	(44, 28, 'Filter replacement', 114.00, 2),
	(45, 28, 'Serpentine belt inspection', 95.00, 3),
	(46, 28, 'Wheel alignment', 80.75, 4),
	(56, 32, 'Checking shock absorber condition', 142.50, 19),
	(57, 32, 'Inspecting hand brakes', 95.00, 20),
	(58, 32, 'Checking power steering fluid', 47.50, 21),
	(59, 33, 'Serpentine belt inspection', 100.00, 3),
	(60, 7, 'Checking the functioning of ABS', 100.00, 13),
	(61, 7, 'Gearbox oil topping up', 150.00, 14),
	(62, 7, 'Service lights resetting', 100.00, 15),
	(63, 8, 'Change the air filter', 30.00, 11),
	(64, 8, 'Checking the functioning of ABS', 100.00, 13),
	(65, 8, 'Service lights resetting', 100.00, 15),
	(66, 8, 'Inspecting brake pads', 100.00, 16),
	(69, 16, 'Replace all the hose pipes', 250.00, 10),
	(70, 16, 'Service lights resetting', 100.00, 15),
	(71, 17, 'Gearbox oil topping up', 150.00, 14),
	(72, 17, 'Inspecting brake pads', 100.00, 16),
	(73, 17, 'Testing antifreeze strength', 20.00, 17),
	(74, 18, 'Change the water pump', 100.00, 9),
	(75, 2, 'Serpentine belt inspection', 100.00, 3),
	(76, 2, 'Replace all the hose pipes', 250.00, 10),
	(77, 3, 'Filter replacement', 120.00, 2),
	(78, 3, 'Serpentine belt inspection', 100.00, 3),
	(79, 25, 'Filter replacement', 120.00, 2),
	(80, 25, 'Serpentine belt inspection', 100.00, 3),
	(81, 25, 'Wheel alignment', 85.00, 4),
	(82, 25, 'Replace all the hose pipes', 250.00, 10),
	(93, 23, 'Change the air filter', 30.00, 11),
	(94, 23, 'Cooling system checking', 50.00, 12),
	(95, 23, 'Checking the functioning of ABS', 100.00, 13);
/*!40000 ALTER TABLE `visits_history` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
