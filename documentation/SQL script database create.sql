create table currencies
(
    currency_id int auto_increment
        primary key,
    code        enum ('EUR', 'USD', 'JPY', 'GBP', 'AUD', 'CAD', 'CHF', 'CNY', 'SEK', 'MXN', 'NZD', 'SGD', 'HKD', 'NOK', 'KRW', 'BGN') not null
)
    engine = InnoDB;

create table levels
(
    level_id        int auto_increment
        primary key,
    level_name      enum ('START', 'BRONZE', 'SILVER', 'GOLD') not null,
    discount        int                                        not null,
    min_expenditure double                                     not null
)
    engine = InnoDB;

create table makes
(
    make_id int auto_increment
        primary key,
    name    varchar(20) not null
)
    engine = InnoDB;

create table models
(
    model_id int auto_increment
        primary key,
    name     varchar(20) not null,
    make_id  int         not null,
    constraint models_makes__fk
        foreign key (make_id) references makes (make_id)
)
    engine = InnoDB;

create table repairs
(
    repair_id int auto_increment
        primary key,
    name      varchar(50) not null,
    price     float(9, 2) not null,
    is_active tinyint     not null
)
    engine = InnoDB;

create table roles
(
    role_id   int auto_increment
        primary key,
    role_name varchar(15) not null
)
    engine = InnoDB;

create table statuses
(
    status_id   int auto_increment
        primary key,
    status_name enum ('NOT_STARTED', 'IN_PROGRESS', 'READY_FOR_PICKUP', 'COMPLETED', 'CANCELLED') not null
)
    engine = InnoDB;

create table users
(
    user_id    int auto_increment
        primary key,
    first_name varchar(20) not null,
    last_name  varchar(20) not null,
    username   varchar(20) not null,
    password   varchar(60) not null,
    phone      varchar(15) not null,
    email      varchar(35) not null,
    level_id   int         not null,
    role_id    int         not null,
    constraint users_pk2
        unique (username),
    constraint users_levels__fk
        foreign key (level_id) references levels (level_id),
    constraint users_roles__fk
        foreign key (role_id) references roles (role_id)
)
    engine = InnoDB;

create table cars
(
    car_id        int auto_increment
        primary key,
    model_id      int         not null,
    license_plate varchar(8)  not null,
    vin           varchar(17) not null,
    year          int         not null,
    user_id       int         not null,
    constraint cars_models__fk
        foreign key (model_id) references models (model_id),
    constraint cars_users__fk
        foreign key (user_id) references users (user_id)
)
    engine = InnoDB;

create table tokens
(
    token_Id     int auto_increment
        primary key,
    token        varchar(30)                         not null,
    user_id      int                                 not null,
    time_created timestamp default CURRENT_TIMESTAMP not null on update CURRENT_TIMESTAMP,
    constraint tokens_users_user_id_fk
        foreign key (user_id) references users (user_id)
)
    engine = InnoDB;

create table visits
(
    visit_id   int auto_increment
        primary key,
    car_id     int  not null,
    status_id  int  not null,
    visit_date date not null,
    constraint visits_cars__fk
        foreign key (car_id) references cars (car_id),
    constraint visits_statuses__fk
        foreign key (status_id) references statuses (status_id)
)
    engine = InnoDB;

create table visits_history
(
    visits_history_id int auto_increment
        primary key,
    visit_id          int         not null,
    repair_name       varchar(50) not null,
    price             float(9, 2) not null,
    repair_id         int         not null,
    constraint visits_history_visits__fk
        foreign key (visit_id) references visits (visit_id)
)
    engine = InnoDB;

