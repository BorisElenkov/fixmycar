package com.example.smartgarage.repositories;

import com.example.smartgarage.exceptions.EntityNotFoundException;
import com.example.smartgarage.models.Make;
import com.example.smartgarage.repositories.contracts.MakeRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class MakeRepositoryImpl implements MakeRepository {
    private final SessionFactory sessionFactory;

    public MakeRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Make> getMakes() {
        try (Session session = sessionFactory.openSession()) {
            Query<Make> query = session.createQuery("from Make", Make.class);
            return query.list();
        }
    }

    @Override
    public Make getMakeById(int makeId) {
        try (Session session = sessionFactory.openSession()) {
            Make make = session.get(Make.class, makeId);
            if (make == null) {
                throw new EntityNotFoundException("Make", makeId);
            }
            return make;
        }
    }

    @Override
    public Make getMakeByName(String name) {
        try (Session session = sessionFactory.openSession()) {
            Query<Make> query = session.createQuery("from Make where makeName = :name", Make.class);
            query.setParameter("name", name);
            List<Make> result = query.list();
            if (result.size() == 0) {
                throw new EntityNotFoundException("Make", "name", name);
            }
            return result.get(0);
        }
    }

    @Override
    public Make createMake(Make make) {
        try (Session session = sessionFactory.openSession()) {
            session.save(make);
        }
        return make;
    }

    @Override
    public Make updateMake(Make make) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(make);
            session.getTransaction().commit();
        }
        return make;
    }

    @Override
    public void deleteMake(int makeId) {
        Make makeToDelete = getMakeById(makeId);
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(makeToDelete);
            session.getTransaction().commit();
        }
    }
}
