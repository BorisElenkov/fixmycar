package com.example.smartgarage.repositories;

import com.example.smartgarage.exceptions.EntityNotFoundException;
import com.example.smartgarage.models.Visit;
import com.example.smartgarage.models.VisitFilterOptions;
import com.example.smartgarage.repositories.contracts.VisitsHistoryRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.example.smartgarage.repositories.contracts.VisitRepository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class VisitRepositoryImpl implements VisitRepository {
    private final SessionFactory sessionFactory;
    private final VisitsHistoryRepository visitsHistoryRepository;

    @Autowired
    public VisitRepositoryImpl(SessionFactory sessionFactory, VisitsHistoryRepository visitsHistoryRepository) {
        this.sessionFactory = sessionFactory;
        this.visitsHistoryRepository = visitsHistoryRepository;
    }

    @Override
    public List<Visit> getVisits(VisitFilterOptions filterOptions) {
        try (Session session = sessionFactory.openSession()) {
            StringBuilder queryString = new StringBuilder(" select v from Visit v join v.car c " +
                    "join c.user u join v.status s ");
            Map<String, Object> queryParams = new HashMap<>();
            List<String> queryParts = new ArrayList<>();

            filterOptions.getCarId().ifPresent(value -> {
                queryParts.add(" c.carId = :carId ");
                queryParams.put("carId", value);
            });

            filterOptions.getLicensePlate().ifPresent(value -> {
                queryParts.add(" c.licensePlate like :licensePlate ");
                queryParams.put("licensePlate", "%" + value + "%");
            });

            filterOptions.getCustomerId().ifPresent(value -> {
                queryParts.add(" u.userId = :customerId");
                queryParams.put("customerId", value);
            });

            filterOptions.getUsername().ifPresent(value -> {
                queryParts.add(" u.username like :username ");
                queryParams.put("username", "%" + value + "%");
            });

            filterOptions.getStatusId().ifPresent(value -> {
                queryParts.add(" s.statusId = :statusId");
                queryParams.put("statusId", value);
            });

            if (!queryParts.isEmpty()) {
                queryString.append(" where ").append(String.join(" and ", queryParts));
            }

            String sortString = generateSortString(filterOptions);
            queryString.append(sortString);

            Query<Visit> query = session.createQuery(queryString.toString(), Visit.class);
            query.setProperties(queryParams);
            return query.list();
        }
    }

    private String generateSortString(VisitFilterOptions filterOptions) {
        if (filterOptions.getSortBy().isEmpty()) {
            return "";
        }

        StringBuilder queryString = new StringBuilder(" order by ");
        switch (filterOptions.getSortBy().get()) {
            case "status id":
                queryString.append(" s.statusId ");
                break;
            case "status name":
                queryString.append(" s.name ");
                break;
            case "date":
                queryString.append(" v.visitDate ");
                break;
            case "customer id":
                queryString.append(" u.userId ");
                break;
            case "username":
                queryString.append(" u.username ");
                break;
            case "car id":
                queryString.append(" c.carId ");
                break;
            case "license plate":
                queryString.append(" c.licensePlate ");
                break;
            default:
                return "";
        }

        if (filterOptions.getSortOrder().isPresent() &&
                filterOptions.getSortOrder().get().equalsIgnoreCase("desc")) {
            queryString.append(" desc ");
        }

        return queryString.toString();
    }

    @Override
    public Visit getVisitById(int visitId) {
        try (Session session = sessionFactory.openSession()) {
            Visit visit = session.get(Visit.class, visitId);
            if (visit == null) {
                throw new EntityNotFoundException("Visit", visitId);
            }
            return visit;
        }
    }

    @Override
    public List<Visit> getVisitsByCarId(int carId) {
        try (Session session = sessionFactory.openSession()) {
            Query<Visit> query = session.createQuery("from Visit where car.carId = :carId ", Visit.class);
            query.setParameter("carId", carId);
            return query.list();
        }
    }

    @Override
    public Double getTotalPrice(int visitId) {
        try (Session session = sessionFactory.openSession()) {
            Query<Double> query = session.createQuery(" select sum(h.price) from Visit v " +
                    " join v.visitsHistory h where v.visitId = :visitId", Double.class);
            query.setParameter("visitId", visitId);
            return query.uniqueResult();
        }
    }

    @Override
    public void createVisit(Visit visit) {
        try (Session session = sessionFactory.openSession()) {
            session.save(visit);
        }
    }

    @Override
    public void updateVisit(Visit visit) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(visit);
            session.getTransaction().commit();
        }
    }

    @Override
    public void deleteVisit(int visitId) {
        Visit visitToDelete = getVisitById(visitId);
        visitsHistoryRepository.delete(visitId);
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(visitToDelete);
            session.getTransaction().commit();
        }
    }

    @Override
    public void deleteVisits(int carId) {
        List<Visit> visitsByCar = getVisitsByCarId(carId);
        if (!(visitsByCar == null || visitsByCar.isEmpty())) {
            for (Visit visit : visitsByCar) {
                deleteVisit(visit.getVisitId());
            }
        }
    }
}
