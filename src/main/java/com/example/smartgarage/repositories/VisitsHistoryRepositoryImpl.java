package com.example.smartgarage.repositories;

import com.example.smartgarage.exceptions.EntityNotFoundException;
import com.example.smartgarage.models.VisitsHistory;
import com.example.smartgarage.models.VisitsHistoryFilterOptions;
import com.example.smartgarage.repositories.contracts.VisitsHistoryRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class VisitsHistoryRepositoryImpl implements VisitsHistoryRepository {
    private final SessionFactory sessionFactory;

    @Autowired
    public VisitsHistoryRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<VisitsHistory> getVisitsHistory(VisitsHistoryFilterOptions filterOptions) {
        try (Session session = sessionFactory.openSession()) {
            StringBuilder queryString = new StringBuilder(" select vh from Visit v" +
                    " join v.visitsHistory vh " +
                    " join v.car c " +
                    " join c.user u ");
            Map<String, Object> queryParams = new HashMap<>();
            List<String> queryParts = new ArrayList<>();

            filterOptions.getCarId().ifPresent(value -> {
                queryParts.add(" c.carId = :carId ");
                queryParams.put("carId", value);
            });

            filterOptions.getLicensePlate().ifPresent(value -> {
                queryParts.add(" c.licensePlate like :licensePlate");
                queryParams.put("licensePlate", "%" + value + "%");
            });

            filterOptions.getVisitDate().ifPresent(value -> {
                queryParts.add(" v.visitDate = :date");
                queryParams.put("date", value);
            });

            filterOptions.getRepairName().ifPresent(value -> {
                queryParts.add(" vh.repairName like :name");
                queryParams.put("name", "%" + value + "%");
            });

            filterOptions.getCustomerId().ifPresent(value -> {
                queryParts.add(" u.userId = :customerId");
                queryParams.put("customerId", value);
            });

            filterOptions.getUsername().ifPresent(value -> {
                queryParts.add(" u.username like :username");
                queryParams.put("username", "%" + value + "%");
            });

            if (!queryParts.isEmpty()) {
                queryString.append(" where ").append(String.join(" and ", queryParts));
            }

            String sortString = generateSortString(filterOptions);
            queryString.append(sortString);

            Query<VisitsHistory> query = session.createQuery(queryString.toString(), VisitsHistory.class);
            query.setProperties(queryParams);
            return query.list();
        }
    }

    @Override
    public List<VisitsHistory> getVisitsHistory(int visitId) {
        try (Session session = sessionFactory.openSession()) {
            Query<VisitsHistory> query = session.createQuery("from VisitsHistory " +
                    " where visitId = :visitId", VisitsHistory.class);
            query.setParameter("visitId", visitId);
            return query.list();
        }
    }

    @Override
    public VisitsHistory getHistory(int visitId, int repairId) {
        try (Session session = sessionFactory.openSession()) {
            Query<VisitsHistory> query = session.createQuery("from VisitsHistory " +
                    " where visitId = :visitId and repairId = :repairId", VisitsHistory.class);
            query.setParameter("visitId", visitId);
            query.setParameter("repairId", repairId);
            List<VisitsHistory> result = query.list();
            if (result.isEmpty()) {
                throw new EntityNotFoundException("VisitsRepairsRelation", "visitId", String.valueOf(visitId));
            }
            return result.get(0);
        }
    }

    @Override
    public double getTotalPaidByUserId(int userId) {
        try (Session session = sessionFactory.openSession()) {
            Query<Double> query = session.createQuery(" select sum(h.price) from Visit v " +
                    " join v.visitsHistory h where v.car.user.userId = :userId" +
                    " and v.status.statusId in (3,4) ", Double.class);
            query.setParameter("userId", userId);
            if (query.uniqueResult() == null) {
                return 0.0;
            }
            return query.uniqueResult();
        }
    }

    @Override
    public void create(VisitsHistory visitsHistory) {
        try (Session session = sessionFactory.openSession()) {
            session.save(visitsHistory);
        }
    }

    @Override
    public void update(VisitsHistory visitsHistory) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(visitsHistory);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(int visitId) {
        List<VisitsHistory> visitsHistoryToDelete = getVisitsHistory(visitId);
        for (VisitsHistory vh : visitsHistoryToDelete) {
            try (Session session = sessionFactory.openSession()) {
                session.beginTransaction();
                session.delete(vh);
                session.getTransaction().commit();
            }
        }
    }

    @Override
    public void delete(int visitId, int repairId) {
        VisitsHistory visitsHistory = getHistory(visitId, repairId);
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(visitsHistory);
            session.getTransaction().commit();
        }
    }

    private String generateSortString(VisitsHistoryFilterOptions filterOptions) {
        if (filterOptions.getSortBy().isEmpty()) {
            return "";
        }

        StringBuilder queryString = new StringBuilder(" order by ");
        switch (filterOptions.getSortBy().get()) {
            case "car id":
                queryString.append(" c.carId ");
                break;
            case "license plate":
                queryString.append(" c.licensePlate ");
                break;
            case "date":
                queryString.append(" v.visitDate ");
                break;
            case "customer id":
                queryString.append(" u.userId ");
                break;
            case "username":
                queryString.append(" u.username ");
                break;
            case "repair name":
                queryString.append(" vh.repairName ");
                break;
            default:
                return "";
        }

        if (filterOptions.getSortOrder().isPresent() &&
                filterOptions.getSortOrder().get().equalsIgnoreCase("desc")) {
            queryString.append(" desc ");
        }

        return queryString.toString();
    }
}
