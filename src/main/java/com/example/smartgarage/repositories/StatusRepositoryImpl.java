package com.example.smartgarage.repositories;

import com.example.smartgarage.exceptions.EntityNotFoundException;
import com.example.smartgarage.models.Status;
import com.example.smartgarage.repositories.contracts.StatusRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class StatusRepositoryImpl implements StatusRepository {
    private final SessionFactory sessionFactory;

    @Autowired
    public StatusRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Status> getStatuses() {
        try (Session session = sessionFactory.openSession()) {
            Query<Status> query = session.createQuery("from Status", Status.class);
            return query.list();
        }
    }

    @Override
    public Status getStatusById(int statusId) {
        try (Session session = sessionFactory.openSession()) {
            Status status = session.get(Status.class, statusId);
            if (status == null) {
                throw new EntityNotFoundException("Status", statusId);
            }
            return status;
        }
    }
}
