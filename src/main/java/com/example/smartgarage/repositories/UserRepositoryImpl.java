package com.example.smartgarage.repositories;

import com.example.smartgarage.exceptions.EntityNotFoundException;
import com.example.smartgarage.models.User;
import com.example.smartgarage.models.UserFilterOptions;
import com.example.smartgarage.repositories.contracts.UserRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.*;

@Repository
public class UserRepositoryImpl implements UserRepository {
    private final SessionFactory sessionFactory;

    @Autowired
    public UserRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<User> getUsers(UserFilterOptions userFilterOptions) {
        try (Session session = sessionFactory.openSession()) {
            StringBuilder queryString = new StringBuilder
                    (" select distinct u from User u " +
                            " left join Car c on u.userId = c.user.userId " +
                            " left join Visit v on c.carId = v.car.carId ");
            Map<String, Object> queryParams = new HashMap<>();
            ArrayList<String> filter = new ArrayList<>();

            userFilterOptions.getFirstName().ifPresent(v -> {
                filter.add(" u.firstName like :firstName ");
                queryParams.put("firstName", "%" + v + "%");
            });

            userFilterOptions.getLastName().ifPresent(v -> {
                filter.add(" u.lastName like :lastName ");
                queryParams.put("lastName", "%" + v + "%");
            });

            userFilterOptions.getPhone().ifPresent(v -> {
                filter.add(" u.phone like :phone ");
                queryParams.put("phone", "%" + v + "%");
            });

            userFilterOptions.getEmail().ifPresent(v -> {
                filter.add(" u.email like :email ");
                queryParams.put("email", "%" + v + "%");
            });

            userFilterOptions.getUsername().ifPresent(v -> {
                filter.add(" u.username like :username ");
                queryParams.put("username", "%" + v + "%");
            });

            userFilterOptions.getLicensePlate().ifPresent(v -> {
                filter.add(" c.licensePlate like :licensePlate ");
                queryParams.put("licensePlate", "%" + v + "%");
            });

            userFilterOptions.getVin().ifPresent(v -> {
                filter.add(" c.vin like :vin ");
                queryParams.put("vin", "%" + v + "%");
            });

            userFilterOptions.getModelName().ifPresent(v -> {
                filter.add(" c.carModel.modelName like :modelName ");
                queryParams.put("modelName", "%" + v + "%");
            });

            userFilterOptions.getMakeName().ifPresent(v -> {
                filter.add(" c.carModel.make.makeName like :makeName ");
                queryParams.put("makeName", "%" + v + "%");
            });

            userFilterOptions.getRoleId().ifPresent(v -> {
                filter.add(" u.role.roleId = :roleId ");
                queryParams.put("roleId", v);
            });

            userFilterOptions.getStartDate().ifPresent(v -> {
                filter.add(" v.visitDate > :startDate");
                queryParams.put("startDate", v);
            });

            userFilterOptions.getEndDate().ifPresent(v -> {
                filter.add(" v.visitDate < :endDate");
                queryParams.put("endDate", v);
            });

            if (!filter.isEmpty()) {
                queryString
                        .append(" where ")
                        .append(String.join(" and ", filter));
            }
            queryString.append(generateOrderBy(userFilterOptions));

            Query<User> queryList = session.createQuery(queryString.toString(), User.class);
            queryList.setProperties(queryParams);
            return queryList.list();
        }
    }

    private String generateOrderBy(UserFilterOptions userFilterOptions) {
        if (userFilterOptions.getOrderBy().isEmpty()) {
            return "";
        }
        String sortBy = "";
        switch (userFilterOptions.getSortBy().get()) {
            case "firstName":
                sortBy = "u.firstName";
                break;
            case "lastName":
                sortBy = "u.lastName";
                break;
            case "phone":
                sortBy = "u.phone";
                break;
            case "email":
                sortBy = "u.email";
                break;
            case "username":
                sortBy = "u.username";
                break;
            case "date":
                sortBy = "v.visitDate";
                break;
            default:
                return "";
        }

        sortBy = String.format(" order by %s", sortBy);
        if (userFilterOptions.getOrderBy().isPresent() &&
                userFilterOptions.getOrderBy().get().equalsIgnoreCase("desc")) {
            sortBy = String.format(" %s desc", sortBy);
        }
        return sortBy;
    }

    @Override
    public User getUserById(int userId) {
        try (Session session = sessionFactory.openSession()) {
            User user = session.get(User.class, userId);
            if (user == null) {
                throw new EntityNotFoundException("User", userId);
            }
            return user;
        }
    }

    @Override
    public User getUserByUsername(String username) {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User where username = :username", User.class);
            query.setParameter("username", username);

            List<User> result = query.list();
            if (result.size() == 0) {
                throw new EntityNotFoundException("User", "username", username);
            }
            return result.get(0);
        }
    }

    @Override
    public User getUserByEmail(String email) {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User where email = :email", User.class);
            query.setParameter("email", email);

            List<User> result = query.list();
            if (result.size() == 0) {
                throw new EntityNotFoundException("User", "email", email);
            }
            return result.get(0);
        }
    }

    @Override
    public User getUserByToken(String token) {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("select u from Token t join t.user u where t.token = :token", User.class);
            query.setParameter("token", token);

            List<User> result = query.list();
            if (result.size() == 0) {
                throw new EntityNotFoundException("User", "token", token);
            }
            return result.get(0);
        }
    }

    @Override
    public User createUser(User user) {
        try (Session session = sessionFactory.openSession()) {
            session.save(user);
        }
        return user;
    }

    @Override
    public User updateUser(User user) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(user);
            session.getTransaction().commit();
        }
        return user;
    }

    @Override
    public void deleteUser(int userId) {
        User userToDelete = getUserById(userId);
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(userToDelete);
            session.getTransaction().commit();
        }
    }

    @Override
    public Long getUsersCount() {
        try (Session session = sessionFactory.openSession()) {
            Query<Long> query = session.createQuery(
                    "select count(*) from User", Long.class);
            return query.uniqueResult();
        }
    }

    @Override
    public Long getEmployeeCount() {
        try (Session session = sessionFactory.openSession()) {
            Query<Long> query = session.createQuery(
                    "select count(*) from User where role.roleName = :roleName", Long.class);
            query.setParameter("roleName", "Employee");
            return query.uniqueResult();
        }
    }

    @Override
    public Long getCustomerCount() {
        try (Session session = sessionFactory.openSession()) {
            Query<Long> query = session.createQuery(
                    "select count(*) from User where role.roleName = :roleName", Long.class);
            query.setParameter("roleName", "Customer");
            return query.uniqueResult();
        }
    }
}
