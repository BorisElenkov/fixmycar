package com.example.smartgarage.repositories;

import com.example.smartgarage.exceptions.EntityNotFoundException;
import com.example.smartgarage.models.Repair;
import com.example.smartgarage.models.RepairFilterOptions;
import com.example.smartgarage.repositories.contracts.RepairRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.NativeQuery;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class RepairRepositoryImpl implements RepairRepository {
    private final SessionFactory sessionFactory;

    @Autowired
    public RepairRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Repair> getRepairs(RepairFilterOptions filterOptions) {
        try (Session session = sessionFactory.openSession()) {
            StringBuilder queryString = new StringBuilder("from Repair r");
            Map<String, Object> queryParams = new HashMap<>();
            List<String> queryParts = new ArrayList<>();

            filterOptions.getName().ifPresent(value -> {
                queryParts.add(" r.name like :name ");
                queryParams.put("name", "%" + value + "%");
            });

            filterOptions.getMinPrice().ifPresent(value -> {
                queryParts.add("r.price >= :minPrice");
                queryParams.put("minPrice", value);
            });

            filterOptions.getMaxPrice().ifPresent(value -> {
                queryParts.add("r.price <= :maxPrice");
                queryParams.put("maxPrice", value);
            });

            filterOptions.getIsActive().ifPresent(value -> {
                queryParts.add("r.isActive = :isActive");
                queryParams.put("isActive", value);
            });

            if (!queryParts.isEmpty()) {
                queryString.append(" where ").append(String.join(" and ", queryParts));
            }

            String sortString = generateSortString(filterOptions);
            queryString.append(sortString);

            Query<Repair> query = session.createQuery(queryString.toString(), Repair.class);
            query.setProperties(queryParams);
            return query.list();
        }
    }

    @Override
    public Repair getRepairById(int repairId) {
        try (Session session = sessionFactory.openSession()) {
            Repair repair = session.get(Repair.class, repairId);
            if (repair == null) {
                throw new EntityNotFoundException("Repair service", repairId);
            }
            return repair;
        }
    }

    @Override
    public Repair getRepairByName(String name) {
        try (Session session = sessionFactory.openSession()) {
            Query<Repair> query = session.createQuery("from Repair where name = :name", Repair.class);
            query.setParameter("name", name);
            List<Repair> repairs = query.list();
            if (repairs.isEmpty()) {
                throw new EntityNotFoundException("Repair service", "name", name);
            }
            return repairs.get(0);
        }
    }

    @Override
    public void createRepair(Repair repair) {
        try (Session session = sessionFactory.openSession()) {
            session.save(repair);
        }
    }

    @Override
    public void updateRepair(Repair repair) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(repair);
            session.getTransaction().commit();
        }
    }

    private String generateSortString(RepairFilterOptions repairFilterOptions) {
        if (repairFilterOptions.getSortBy().isEmpty()) {
            return "";
        }

        StringBuilder queryString = new StringBuilder(" order by ");
        switch (repairFilterOptions.getSortBy().get()) {
            case "name":
                queryString.append(" r.name ");
                break;
            case "price":
                queryString.append(" r.price ");
                break;
            case "active":
                queryString.append(" r.isActive ");
                break;
            default:
                return "";
        }
        if (repairFilterOptions.getSortOrder().isPresent() &&
                repairFilterOptions.getSortOrder().get().equalsIgnoreCase("desc")) {
            queryString.append(" desc ");
        }
        return queryString.toString();
    }
}
