package com.example.smartgarage.repositories;

import com.example.smartgarage.exceptions.EntityNotFoundException;
import com.example.smartgarage.models.Token;
import com.example.smartgarage.repositories.contracts.TokenRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class TokenRepositoryImpl implements TokenRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public TokenRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Token getTokenByUserId(int userId) {
        try (Session session = sessionFactory.openSession()) {
            Query<Token> query = session.createQuery("from Token t where t.user.userId = :userId", Token.class);
            query.setParameter("userId", userId);
            if (query.list().size() == 0) {
                throw new EntityNotFoundException("Token", "user", String.valueOf(userId));
            }
            return query.list().get(0);
        }
    }

    @Override
    public Token createToken(Token token) {
        try (Session session = sessionFactory.openSession()) {
            session.save(token);
        }
        return token;
    }

    @Override
    public void deleteToken(int userId) {
        Token tokenToDelete = getTokenByUserId(userId);
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(tokenToDelete);
            session.getTransaction().commit();
        }
    }
}
