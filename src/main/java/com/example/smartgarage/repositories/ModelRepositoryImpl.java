package com.example.smartgarage.repositories;

import com.example.smartgarage.exceptions.EntityNotFoundException;
import com.example.smartgarage.models.CarModel;
import com.example.smartgarage.repositories.contracts.ModelRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;


import java.util.List;

@Repository
public class ModelRepositoryImpl implements ModelRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public ModelRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<CarModel> getModels() {
        try (Session session = sessionFactory.openSession()) {
            Query<CarModel> query = session.createQuery("from CarModel", CarModel.class);
            return query.list();
        }
    }

    @Override
    public List<CarModel> getAllModelsByMake(String makeName) {
        try (Session session = sessionFactory.openSession()) {
            Query<CarModel> query = session.createQuery("from CarModel where make.makeName = :makeName", CarModel.class);
            query.setParameter("makeName", makeName);
            return query.list();
        }
    }

    @Override
    public CarModel getModelById(int modelId) {
        try (Session session = sessionFactory.openSession()) {
            CarModel carModel = session.get(CarModel.class, modelId);
            if (carModel == null) {
                throw new EntityNotFoundException("Model", modelId);
            }
            return carModel;
        }
    }

    @Override
    public CarModel getModelByName(String name) {
        try (Session session = sessionFactory.openSession()) {
            Query<CarModel> query = session.createQuery("from CarModel where modelName = :name", CarModel.class);
            query.setParameter("name", name);
            List<CarModel> result = query.list();
            if (result.size() == 0) {
                throw new EntityNotFoundException("Make", "name", name);
            }
            return result.get(0);
        }
    }

    @Override
    public CarModel getModelById(int makeId, int modelId) {
        try (Session session = sessionFactory.openSession()) {
            Query<CarModel> query = session.
                    createQuery("from CarModel where make.makeId = :make_id and modelId = :modelId", CarModel.class);
            query.setParameter("make_id", makeId);
            query.setParameter("modelId", modelId);
            if (query.list().size() == 0) {
                throw new EntityNotFoundException("Model", modelId);
            }
            return query.list().get(0);
        }
    }

    @Override
    public CarModel createModel(CarModel carModel) {
        try (Session session = sessionFactory.openSession()) {
            session.save(carModel);
        }
        return carModel;
    }

    @Override
    public CarModel updateModel(CarModel carModel) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(carModel);
            session.getTransaction().commit();
        }
        return carModel;
    }

    @Override
    public void deleteModel(int modelId) {
        CarModel carModelToDelete = getModelById(modelId);
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(carModelToDelete);
            session.getTransaction().commit();
        }
    }
}
