package com.example.smartgarage.repositories;

import com.example.smartgarage.exceptions.EntityNotFoundException;
import com.example.smartgarage.models.Role;
import com.example.smartgarage.repositories.contracts.RoleRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class RoleRepositoryImpl implements RoleRepository {
    private final SessionFactory sessionFactory;

    @Autowired
    public RoleRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Role> getRoles() {
        try (Session session = sessionFactory.openSession()) {
            Query<Role> query = session.createQuery("from Role", Role.class);
            return query.list();
        }
    }

    @Override
    public Role getRoleById(int roleId) {
        try (Session session = sessionFactory.openSession()) {
            Role role = session.get(Role.class, roleId);
            if (role == null) {
                throw new EntityNotFoundException("Role", roleId);
            }
            return role;
        }
    }

    @Override
    public Role getRoleByName(String name) {
        try (Session session = sessionFactory.openSession()) {
            Query<Role> query = session.createQuery("from Role where roleName = :name", Role.class);
            query.setParameter("name", name);
            List<Role> result = query.list();
            if (result.size() == 0) {
                throw new EntityNotFoundException("Role", "name", name);
            }
            return result.get(0);
        }
    }
}
