package com.example.smartgarage.repositories.contracts;

import com.example.smartgarage.models.Car;
import com.example.smartgarage.models.CarFilterOptions;

import java.util.List;

public interface CarRepository {
    List<Car> getCars();

    List<Car> getCarsByUserId(int id);

    List<Car> getCarsByModelId(int modelId);

    List<Car> getCars(CarFilterOptions carFilterOptions);

    Car getCarById(int id);

    Car getCarByLicensePlate(String licensePlate);

    Car createCar(Car car);

    Car updateCar(Car car);

    void deleteCar(int id);

    Long getCarCount();
}
