package com.example.smartgarage.repositories.contracts;

import com.example.smartgarage.models.Token;

public interface TokenRepository {
    Token getTokenByUserId(int userId);

    Token createToken(Token token);

    void deleteToken(int userId);
}
