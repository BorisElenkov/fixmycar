package com.example.smartgarage.repositories.contracts;

import com.example.smartgarage.models.User;
import com.example.smartgarage.models.UserFilterOptions;

import java.util.List;

public interface UserRepository {
    List<User> getUsers(UserFilterOptions userFilterOptions);

    User getUserById(int userId);

    User getUserByUsername(String username);

    User getUserByEmail(String email);

    User getUserByToken(String token);

    User createUser(User user);

    User updateUser(User user);

    void deleteUser(int userId);

    Long getUsersCount();

    Long getEmployeeCount();

    Long getCustomerCount();
}
