package com.example.smartgarage.repositories.contracts;

import com.example.smartgarage.models.Role;

import java.util.List;

public interface RoleRepository {
    List<Role> getRoles();

    Role getRoleById(int id);

    Role getRoleByName(String name);
}