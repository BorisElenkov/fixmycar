package com.example.smartgarage.repositories.contracts;

import com.example.smartgarage.models.Visit;
import com.example.smartgarage.models.VisitFilterOptions;

import java.util.List;

public interface VisitRepository {
    List<Visit> getVisits(VisitFilterOptions visitFilterOptions);

    Visit getVisitById(int visitId);

    List<Visit> getVisitsByCarId(int carId);

    Double getTotalPrice(int visitId);

    void createVisit(Visit visit);

    void updateVisit(Visit visit);

    void deleteVisit(int visitId);

    void deleteVisits(int carId);
}
