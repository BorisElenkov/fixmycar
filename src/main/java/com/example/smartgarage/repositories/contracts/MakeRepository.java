package com.example.smartgarage.repositories.contracts;

import com.example.smartgarage.models.Make;

import java.util.List;

public interface MakeRepository {
    List<Make> getMakes();

    Make getMakeById(int id);

    Make getMakeByName(String name);

    Make createMake(Make make);

    Make updateMake(Make make);

    void deleteMake(int id);
}
