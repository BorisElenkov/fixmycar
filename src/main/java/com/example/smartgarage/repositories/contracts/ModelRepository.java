package com.example.smartgarage.repositories.contracts;

import com.example.smartgarage.models.CarModel;

import java.util.List;

public interface ModelRepository {
    List<CarModel> getModels();

    List<CarModel> getAllModelsByMake(String makeName);

    CarModel getModelById(int id);

    CarModel getModelByName(String name);

    CarModel getModelById(int makeId, int modelId);

    CarModel createModel(CarModel carModel);

    CarModel updateModel(CarModel carModel);

    void deleteModel(int id);
}
