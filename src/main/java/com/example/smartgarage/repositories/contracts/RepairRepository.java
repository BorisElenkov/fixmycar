package com.example.smartgarage.repositories.contracts;

import com.example.smartgarage.models.Repair;
import com.example.smartgarage.models.RepairFilterOptions;

import java.util.List;

public interface RepairRepository {
    List<Repair> getRepairs(RepairFilterOptions repairFilterOptions);

    Repair getRepairById(int repairId);

    Repair getRepairByName(String name);

    void createRepair(Repair repair);

    void updateRepair(Repair repair);
}
