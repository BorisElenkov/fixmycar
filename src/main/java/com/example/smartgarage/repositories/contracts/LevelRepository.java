package com.example.smartgarage.repositories.contracts;

import com.example.smartgarage.models.Level;

import java.util.List;

public interface LevelRepository {
    List<Level> getLevels();

    Level getLevelById(int levelId);

    Level getLevelByExpenditure(double amount);
}
