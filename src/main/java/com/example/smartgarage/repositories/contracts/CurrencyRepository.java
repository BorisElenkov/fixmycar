package com.example.smartgarage.repositories.contracts;

import com.example.smartgarage.models.VisitCurrency;
import com.example.smartgarage.models.enums.CurrencyCode;

import java.util.Currency;
import java.util.List;

public interface CurrencyRepository {
    List<VisitCurrency> getCurrencies();

    VisitCurrency getCurrencyById(int currencyId);
    VisitCurrency getCurrencyByName(CurrencyCode name);
}
