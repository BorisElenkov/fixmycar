package com.example.smartgarage.repositories.contracts;

import com.example.smartgarage.models.Status;

import java.util.List;

public interface StatusRepository {
    List<Status> getStatuses();

    Status getStatusById(int statusId);
}
