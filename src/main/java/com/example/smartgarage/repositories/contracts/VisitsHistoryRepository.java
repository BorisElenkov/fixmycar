package com.example.smartgarage.repositories.contracts;

import com.example.smartgarage.models.VisitsHistory;
import com.example.smartgarage.models.VisitsHistoryFilterOptions;

import java.util.List;

public interface VisitsHistoryRepository {

    List<VisitsHistory> getVisitsHistory(VisitsHistoryFilterOptions filterOptions);

    List<VisitsHistory> getVisitsHistory(int visitId);

    VisitsHistory getHistory(int visitId, int repairId);

    double getTotalPaidByUserId(int userId);

    void create(VisitsHistory visitsHistory);

    void update(VisitsHistory visitsHistory);

    void delete(int visitId);

    void delete(int visitId, int repairId);
}
