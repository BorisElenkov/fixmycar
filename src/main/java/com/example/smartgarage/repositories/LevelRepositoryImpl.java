package com.example.smartgarage.repositories;

import com.example.smartgarage.exceptions.EntityNotFoundException;
import com.example.smartgarage.models.Level;
import com.example.smartgarage.repositories.contracts.LevelRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class LevelRepositoryImpl implements LevelRepository{
    private final SessionFactory sessionFactory;

    @Autowired
    public LevelRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Level> getLevels() {
        try(Session session = sessionFactory.openSession()){
            Query<Level> query = session.createQuery("from Level", Level.class);
            return query.list();
        }
    }

    @Override
    public Level getLevelById(int levelId) {
        try(Session session = sessionFactory.openSession()){
            Level level = session.get(Level.class, levelId);
            if(level == null){
                throw new EntityNotFoundException("Level", levelId);
            }
            return level;
        }
    }

    @Override
    public Level getLevelByExpenditure(double amount) {
        try (Session session = sessionFactory.openSession()) {
            Query<Level> query = session.createQuery("from Level l where l.minExpenditure < :amount order by l.minExpenditure desc ", Level.class);
            query.setParameter("amount", amount);
            return query.list().get(0);
        }
    }
}
