package com.example.smartgarage.repositories;

import com.example.smartgarage.exceptions.EntityNotFoundException;
import com.example.smartgarage.models.Car;
import com.example.smartgarage.models.CarFilterOptions;
import com.example.smartgarage.repositories.contracts.CarRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.*;

@Repository
public class CarRepositoryImpl implements CarRepository {
    private final SessionFactory sessionFactory;

    @Autowired
    public CarRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Car> getCars() {
        try (Session session = sessionFactory.openSession()) {
            Query<Car> query = session.createQuery("from Car", Car.class);
            return query.list();
        }
    }

    @Override
    public List<Car> getCarsByUserId(int userId) {
        try (Session session = sessionFactory.openSession()) {
            Query<Car> query = session.createQuery("from Car where user.userId = :userId", Car.class);
            query.setParameter("userId", userId);
            return query.list();
        }
    }

    @Override
    public List<Car> getCarsByModelId(int modelId) {
        try (Session session = sessionFactory.openSession()) {
            Query<Car> query = session.createQuery("from Car where carModel.modelId = :modelId", Car.class);
            query.setParameter("modelId", modelId);
            return query.list();
        }
    }

    @Override
    public List<Car> getCars(CarFilterOptions carFilterOptions) {
        try (Session session = sessionFactory.openSession()) {
            StringBuilder queryString = new StringBuilder(" from Car c");

            Map<String, Object> queryParams = new HashMap<>();
            ArrayList<String> filter = new ArrayList<>();

            carFilterOptions.getLicensePlate().ifPresent(v -> {
                filter.add(" c.licensePlate like :licensePlate ");
                queryParams.put("licensePlate", "%" + v + "%");
            });

            carFilterOptions.getVin().ifPresent(v -> {
                filter.add(" c.vin like :vin ");
                queryParams.put("vin", "%" + v + "%");
            });

            carFilterOptions.getYear().ifPresent(v -> {
                filter.add(" c.year like :year ");
                queryParams.put("year", v);
            });

            carFilterOptions.getUsername().ifPresent(v -> {
                filter.add(" c.user.username like :username ");
                queryParams.put("username", "%" + v + "%");
            });

            carFilterOptions.getModelName().ifPresent(v -> {
                filter.add(" c.carModel.modelName like :modelName ");
                queryParams.put("modelName", "%" + v + "%");
            });

            carFilterOptions.getMakeName().ifPresent(v -> {
                filter.add(" c.carModel.make.makeName like :makeName ");
                queryParams.put("makeName", "%" + v + "%");
            });

            carFilterOptions.getPhone().ifPresent(v -> {
                filter.add(" c.user.phone like :phone ");
                queryParams.put("phone", "%" + v + "%");
            });

            if (!filter.isEmpty()) {
                queryString
                        .append(" where ")
                        .append(String.join(" and ", filter));
            }
            queryString.append(generateOrderBy(carFilterOptions));

            Query<Car> queryList = session.createQuery(queryString.toString(), Car.class);
            queryList.setProperties(queryParams);
            return queryList.list();
        }
    }

    private String generateOrderBy(CarFilterOptions carFilterOptions) {
        if (carFilterOptions.getOrderBy().isEmpty()) {
            return "";
        }
        String sortBy = "";
        switch (carFilterOptions.getSortBy().get()) {
            case "licensePlate":
                sortBy = "c.licensePlate";
                break;
            case "vin":
                sortBy = "c.vin";
                break;
            case "year":
                sortBy = "c.year";
                break;
            default:
                return "";
        }

        sortBy = String.format(" order by %s", sortBy);
        if (carFilterOptions.getOrderBy().isPresent() &&
                carFilterOptions.getOrderBy().get().equalsIgnoreCase("desc")) {
            sortBy = String.format(" %s desc", sortBy);
        }
        return sortBy;
    }

    @Override
    public Car getCarById(int carId) {
        try (Session session = sessionFactory.openSession()) {
            Car car = session.get(Car.class, carId);
            if (car == null) {
                throw new EntityNotFoundException("Car", carId);
            }
            return car;
        }
    }

    @Override
    public Car getCarByLicensePlate(String licensePlate) {
        try (Session session = sessionFactory.openSession()) {
            Query<Car> query = session.createQuery("from Car where licensePlate = :licensePlate", Car.class);
            query.setParameter("licensePlate", licensePlate);
            if (query.list().size() == 0) {
                throw new EntityNotFoundException("Car", "licensePlate", licensePlate);
            }
            return query.list().get(0);
        }
    }

    @Override
    public Car createCar(Car car) {
        try (Session session = sessionFactory.openSession()) {
            session.save(car);
        }
        return car;
    }

    @Override
    public Car updateCar(Car car) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(car);
            session.getTransaction().commit();
        }
        return car;
    }

    @Override
    public void deleteCar(int carId) {
        Car carToDelete = getCarById(carId);
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(carToDelete);
            session.getTransaction().commit();
        }
    }

    @Override
    public Long getCarCount() {
        try (Session session = sessionFactory.openSession()) {
            Query<Long> query = session.createQuery(
                    "select count(*) from Car", Long.class);
            return query.uniqueResult();
        }
    }
}
