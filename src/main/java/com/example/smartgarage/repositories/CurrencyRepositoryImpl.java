package com.example.smartgarage.repositories;

import com.example.smartgarage.exceptions.EntityNotFoundException;
import com.example.smartgarage.models.VisitCurrency;
import com.example.smartgarage.models.enums.CurrencyCode;
import com.example.smartgarage.repositories.contracts.CurrencyRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CurrencyRepositoryImpl implements CurrencyRepository {
    private final SessionFactory sessionFactory;

    @Autowired
    public CurrencyRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<VisitCurrency> getCurrencies() {
        try(Session session = sessionFactory.openSession()){
            Query<VisitCurrency> query = session
                    .createQuery("from VisitCurrency", VisitCurrency.class);
            return query.list();
        }
    }

    @Override
    public VisitCurrency getCurrencyById(int currencyId) {
        try(Session session = sessionFactory.openSession()){
            VisitCurrency currency = session.get(VisitCurrency.class, currencyId);
            if(currency==null){
                throw new EntityNotFoundException("Visit currency", currencyId);
            }
            return currency;
        }
    }

    @Override
    public VisitCurrency getCurrencyByName(CurrencyCode name) {
        try(Session session = sessionFactory.openSession()){
            Query<VisitCurrency> query = session.createQuery("from VisitCurrency where currencyCode = :name", VisitCurrency.class);
            query.setParameter("name", name);
            List<VisitCurrency> result = query.list();
            if (result.size() == 0) {
                throw new EntityNotFoundException("Currency", "name", name.toString());
            }
            return result.get(0);
        }
    }
}
