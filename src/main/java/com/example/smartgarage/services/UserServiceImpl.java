package com.example.smartgarage.services;

import com.example.smartgarage.exceptions.AuthorizationException;
import com.example.smartgarage.exceptions.EntityDuplicateException;
import com.example.smartgarage.exceptions.EntityNotFoundException;
import com.example.smartgarage.helpers.UsernameGenerator;
import com.example.smartgarage.models.*;
import com.example.smartgarage.repositories.contracts.*;
import com.example.smartgarage.services.contracts.CarService;
import com.example.smartgarage.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;
    private final TokenRepository tokenRepository;
    private final LevelRepository levelRepository;
    private final VisitsHistoryRepository visitsHistoryRepository;
    private final CarRepository carRepository;
    private final CarService carService;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public UserServiceImpl(UserRepository userRepository, TokenRepository tokenRepository, LevelRepository levelRepository, VisitsHistoryRepository visitsHistoryRepository, CarRepository carRepository, CarService carService, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.tokenRepository = tokenRepository;
        this.levelRepository = levelRepository;
        this.visitsHistoryRepository = visitsHistoryRepository;
        this.carRepository = carRepository;
        this.carService = carService;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public List<User> getUsers(UserFilterOptions userFilterOptions) {
        return userRepository.getUsers(userFilterOptions);
    }

    @Override
    public List<User> getUsers(UserFilterOptions userFilterOptions, User authentication) {
        checkEmployeeRole(authentication);
        return userRepository.getUsers(userFilterOptions);
    }

    @Override
    public User getUserById(int userId) {
        return userRepository.getUserById(userId);
    }

    @Override
    public User getUserById(int userId, User authentication) {
        User user = userRepository.getUserById(userId);
        checkAuthorization(user, authentication);
        return userRepository.getUserById(userId);
    }

    @Override
    public User getUserByUsername(String username) {
        return userRepository.getUserByUsername(username);
    }

    @Override
    public User getUserByEmail(String email) {
        return userRepository.getUserByEmail(email);
    }

    @Override
    public User getUserByToken(String token) {
        User user = userRepository.getUserByToken(token);
        Token repositoryToken = tokenRepository.getTokenByUserId(user.getUserId());
        LocalDateTime now = LocalDateTime.now();
        long hours = ChronoUnit.HOURS.between(repositoryToken.getTimeCreated(), now);
        if(hours>1){
            throw new IllegalArgumentException("Invalid token");
        }
        return user;
    }

    @Override
    public User createUser(User user, User authentication) {
        try {
            checkEmployeeRole(authentication);
        } catch (AuthorizationException e) {
            throw new AuthorizationException("Only Employees can create, update or delete profiles.");
        }

        boolean duplicateUsernameExists = true;
        try {
            userRepository.getUserByUsername(user.getUsername());
        } catch (EntityNotFoundException e) {
            duplicateUsernameExists = false;
        }
        while (duplicateUsernameExists) {
            user.setUsername(UsernameGenerator.generate(user.getFirstName()));
            try {
                userRepository.getUserByUsername(user.getUsername());
            } catch (EntityNotFoundException e) {
                duplicateUsernameExists = false;
            }
        }

        boolean duplicateEmailExists = true;
        try {
            userRepository.getUserByEmail(user.getEmail());
        } catch (EntityNotFoundException e) {
            duplicateEmailExists = false;
        }

        if (duplicateEmailExists) {
            throw new EntityDuplicateException("User", "email", user.getEmail());
        }
        userRepository.createUser(user);
        return user;
    }

    @Override
    public void updateUser(User user, User authentication) {
        try {
            checkEmployeeRole(authentication);
        } catch (AuthorizationException e) {
            throw new AuthorizationException("Only Employees can create, update or delete profiles.");
        }

        boolean duplicateUsernameExists = true;
        try {
            User existingUser = userRepository.getUserByUsername(user.getUsername());
            if (existingUser.getUserId() == user.getUserId()) {
                duplicateUsernameExists = false;
            }
        } catch (EntityNotFoundException e) {
            duplicateUsernameExists = false;
        }

        if (duplicateUsernameExists) {
            throw new EntityDuplicateException("User", "username", user.getUsername());
        }

        boolean duplicateEmailExists = true;
        try {
            User existingUser = userRepository.getUserByEmail(user.getEmail());
            if (existingUser.getUserId() == user.getUserId()) {
                duplicateEmailExists = false;
            }
        } catch (EntityNotFoundException e) {
            duplicateEmailExists = false;
        }

        if (duplicateEmailExists) {
            throw new EntityDuplicateException("User", "email", user.getEmail());
        }
        userRepository.updateUser(user);
    }

    @Override
    public void deleteUser(int userId, User authentication) {
        checkEmployeeRole(authentication);
        List<Car> cars = carRepository.getCarsByUserId(userId);
        if (!(cars == null || cars.isEmpty())) {
            for (Car car : cars) {
                carService.deleteCar(car.getCarId(), authentication);
            }
        }
        userRepository.deleteUser(userId);
    }

    @Override
    public Long getUsersCount() {
        return userRepository.getUsersCount();
    }

    @Override
    public Long getEmployeeCount() {
        return userRepository.getEmployeeCount();
    }

    @Override
    public Long getCustomerCount() {
        return userRepository.getCustomerCount();
    }

    @Override
    public User changePassword(int userId, User authentication, String password) {
        User user = userRepository.getUserById(userId);
        if (userId != authentication.getUserId()) {
            throw new AuthorizationException("User can change only his own password!");
        }
        String encryptedPassword = passwordEncoder.encode(password);
        user.setPassword(encryptedPassword);
        userRepository.updateUser(user);
        return user;
    }

    @Override
    public void resetForgottenPassword(int userId, String password) {
        User user = userRepository.getUserById(userId);
        String encryptedPassword = passwordEncoder.encode(password);
        user.setPassword(encryptedPassword);
        userRepository.updateUser(user);
        tokenRepository.deleteToken(userId);
    }

    @Override
    public void updateToken(String token, String email) {
        User user = userRepository.getUserByEmail(email);
        Token newToken = new Token();
        newToken.setToken(token);
        newToken.setUser(user);
        tokenRepository.createToken(newToken);
    }

    @Override
    public void reevaluateLevel(User user) {
        double expenditure = visitsHistoryRepository.getTotalPaidByUserId(user.getUserId());
        Level newLevel;
        if (expenditure == 0.0) {
            newLevel = levelRepository.getLevelById(1);
        } else {
            newLevel = levelRepository.getLevelByExpenditure(expenditure);
        }
        user.setLevel(newLevel);
        userRepository.updateUser(user);
    }

    private void checkEmployeeRole(User user) {
        if (!user.getRole().getRoleName().equalsIgnoreCase("Employee")) {
            throw new AuthorizationException("Only Employees can create, update or delete profiles.");
        }
    }

    private void checkAuthorization(User user, User authentication) {
        if (!(authentication.equals(user) || authentication.getRole().getRoleName().equalsIgnoreCase("Employee"))) {
            throw new AuthorizationException("Only Employees or owners can view users.");
        }
    }
}
