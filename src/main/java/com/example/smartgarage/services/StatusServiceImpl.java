package com.example.smartgarage.services;

import com.example.smartgarage.models.Status;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.smartgarage.services.contracts.StatusService;
import com.example.smartgarage.repositories.contracts.StatusRepository;

import java.util.List;

@Service
public class StatusServiceImpl implements StatusService {
    private final StatusRepository statusRepository;

    @Autowired
    public StatusServiceImpl(StatusRepository statusRepository) {
        this.statusRepository = statusRepository;
    }

    @Override
    public List<Status> getStatuses() {
        return statusRepository.getStatuses();
    }

    @Override
    public Status getStatusById(int statusId) {
        return statusRepository.getStatusById(statusId);
    }
}
