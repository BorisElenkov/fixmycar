package com.example.smartgarage.services;

import com.example.smartgarage.exceptions.AuthorizationException;
import com.example.smartgarage.exceptions.EntityDuplicateException;
import com.example.smartgarage.exceptions.EntityNotFoundException;
import com.example.smartgarage.models.Car;
import com.example.smartgarage.models.CarModel;
import com.example.smartgarage.models.User;
import com.example.smartgarage.repositories.contracts.CarRepository;
import com.example.smartgarage.repositories.contracts.ModelRepository;
import com.example.smartgarage.services.contracts.ModelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ModelServiceImpl implements ModelService {
    private final ModelRepository modelRepository;
    private final CarRepository carRepository;

    @Autowired
    public ModelServiceImpl(ModelRepository modelRepository, CarRepository carRepository) {
        this.modelRepository = modelRepository;
        this.carRepository = carRepository;
    }

    @Override
    public List<CarModel> getModels() {
        return modelRepository.getModels();
    }

    @Override
    public List<CarModel> getAllModelsByMake(String makeName) {
        return modelRepository.getAllModelsByMake(makeName);
    }

    @Override
    public CarModel getModelById(int modelId) {
        return modelRepository.getModelById(modelId);
    }

    @Override
    public CarModel getModelByName(String name) {
        return modelRepository.getModelByName(name);
    }

    @Override
    public CarModel getModelById(int makeId, int modelId) {
        return modelRepository.getModelById(makeId, modelId);
    }

    @Override
    public CarModel createModel(CarModel carModel, User authentication) {
        checkEmployeeRole(authentication);
        boolean duplicateExists = true;
        try {
            modelRepository.getModelByName(carModel.getModelName());
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }

        if (duplicateExists) {
            throw new EntityDuplicateException("Model", "name", carModel.getModelName());
        }
        modelRepository.createModel(carModel);
        return carModel;
    }

    @Override
    public CarModel updateModel(User authentication, CarModel carModel) {
        checkEmployeeRole(authentication);

        boolean duplicateExists = true;
        try {
            CarModel existingModel = modelRepository.getModelByName(carModel.getModelName());
            if (existingModel.getModelId() == carModel.getModelId()) {
                duplicateExists = false;
            }
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }

        if (duplicateExists) {
            throw new EntityDuplicateException("Model", "name", carModel.getModelName());
        }
        modelRepository.updateModel(carModel);
        return carModel;
    }

    @Override
    public void deleteModel(int modelId, User authentication) {
        checkEmployeeRole(authentication);
        List<Car> cars = carRepository.getCarsByModelId(modelId);
        if (!cars.isEmpty()) {
            throw new IllegalArgumentException("This model cannot be deleted. It is in use for some customer cars.");
        }
        modelRepository.deleteModel(modelId);
    }

    private void checkEmployeeRole(User user) {
        if (!user.getRole().getRoleName().equalsIgnoreCase("Employee")) {
            throw new AuthorizationException("Only Employees can update models.");
        }
    }
}
