package com.example.smartgarage.services;

import com.example.smartgarage.exceptions.AuthorizationException;
import com.example.smartgarage.exceptions.EntityDuplicateException;
import com.example.smartgarage.exceptions.EntityNotFoundException;
import com.example.smartgarage.models.CarModel;
import com.example.smartgarage.models.Make;
import com.example.smartgarage.models.User;
import com.example.smartgarage.repositories.contracts.MakeRepository;
import com.example.smartgarage.repositories.contracts.ModelRepository;
import com.example.smartgarage.services.contracts.MakeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MakeServiceImpl implements MakeService {
    private final MakeRepository makeRepository;
    private final ModelRepository modelRepository;

    @Autowired
    public MakeServiceImpl(MakeRepository makeRepository, ModelRepository modelRepository) {
        this.makeRepository = makeRepository;
        this.modelRepository = modelRepository;
    }

    @Override
    public List<Make> getMakes() {
        return makeRepository.getMakes();
    }

    @Override
    public Make getMakeById(int makeId) {
        return makeRepository.getMakeById(makeId);
    }

    @Override
    public Make getMakeByName(String name) {
        return makeRepository.getMakeByName(name);
    }

    @Override
    public Make createMake(Make make, User authentication) {
        checkEmployeeRole(authentication);
        boolean duplicateExists = true;

        try {
            makeRepository.getMakeByName(make.getMakeName());
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }

        if (duplicateExists) {
            throw new EntityDuplicateException("Make", "name", make.getMakeName());
        }
        makeRepository.createMake(make);
        return make;
    }

    @Override
    public Make updateMake(Make make, User authentication) {
        checkEmployeeRole(authentication);

        boolean duplicateExists = true;
        try {
            Make existingMake = makeRepository.getMakeByName(make.getMakeName());
            if (existingMake.getMakeId() == make.getMakeId()) {
                duplicateExists = false;
            }
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }

        if (duplicateExists) {
            throw new EntityDuplicateException("Make", "name", make.getMakeName());
        }
        makeRepository.updateMake(make);
        return make;
    }

    @Override
    public void deleteMake(int makeId, User authentication) {
        checkEmployeeRole(authentication);
        Make make = makeRepository.getMakeById(makeId);
        List<CarModel> models = modelRepository.getAllModelsByMake(make.getMakeName());
        if (!models.isEmpty()) {
            throw new IllegalArgumentException("This make cannot be deleted. It is in use for some car models.");
        }
        makeRepository.deleteMake(makeId);
    }

    private void checkEmployeeRole(User user) {
        if (!user.getRole().getRoleName().equalsIgnoreCase("Employee")) {
            throw new AuthorizationException("Only Employees can update makes.");
        }
    }
}
