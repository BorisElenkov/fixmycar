package com.example.smartgarage.services.contracts;

import com.example.smartgarage.models.Status;

import java.util.List;

public interface StatusService {
    List<Status> getStatuses();

    Status getStatusById(int statusId);
}
