package com.example.smartgarage.services.contracts;

import com.example.smartgarage.models.*;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

public interface VisitService {
    List<Visit> getVisits(VisitFilterOptions visitFilterOptions, User user, Optional<Car> car);

    Visit getVisitById(int visitId);

    Visit getVisitById(int visitId, User user, Optional<Car> car);

    void createVisit(Visit visit, User user);

    void addRepairToVisit(int repairId, int visitId, User user);

    void removeRepairFromVisit(int repairId, int visitId, User user);

    void changeStatus(int visitId, int StatusId, User user);

    void updateVisit(Visit visit, User user);

    void deleteVisit(int visitId, User user);

    Double getTotalPrice(int visitId);

    Double getTotalPriceDiffCurrency(int visitId, String toCurrency) throws IOException;
}
