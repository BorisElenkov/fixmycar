package com.example.smartgarage.services.contracts;

import com.example.smartgarage.models.Level;

import java.util.List;

public interface LevelService {
    List<Level> getLevels();

    Level getLevelById(int levelId);
}
