package com.example.smartgarage.services.contracts;

import com.example.smartgarage.models.Car;
import com.example.smartgarage.models.User;
import com.example.smartgarage.models.VisitsHistory;
import com.example.smartgarage.models.VisitsHistoryFilterOptions;

import java.util.List;
import java.util.Optional;

public interface VisitsHistoryService {
    List<VisitsHistory> getVisitsHistory(VisitsHistoryFilterOptions filterOptions, User user, Optional<Car> car);
}
