package com.example.smartgarage.services.contracts;

import com.example.smartgarage.models.CarModel;
import com.example.smartgarage.models.User;

import java.util.List;

public interface ModelService {
    List<CarModel> getModels();

    List<CarModel> getAllModelsByMake(String makeName);

    CarModel getModelById(int id);

    CarModel getModelByName(String name);

    CarModel getModelById(int makeId, int modelId);

    CarModel createModel(CarModel carModel, User authentication);

    CarModel updateModel(User authentication, CarModel carModel);

    void deleteModel(int id, User authentication);
}
