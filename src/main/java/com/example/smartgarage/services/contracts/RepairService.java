package com.example.smartgarage.services.contracts;

import com.example.smartgarage.models.Repair;
import com.example.smartgarage.models.RepairFilterOptions;
import com.example.smartgarage.models.User;

import java.util.List;

public interface RepairService {
    List<Repair> getRepairs(RepairFilterOptions repairFilterOptions);

    Repair getRepairById(int repairId);

    Repair getRepairById(int repairId, User user);

    Repair getRepairByName(String name);

    void createRepair(Repair repair, User user);

    void updateRepair(Repair repair, User user);

    void deactivateRepair(int repairId, User user);

    void activateRepair(int repairId, User user);
}
