package com.example.smartgarage.services.contracts;

import com.example.smartgarage.models.Car;
import com.example.smartgarage.models.CarFilterOptions;
import com.example.smartgarage.models.User;

import java.util.List;

public interface CarService {

    List<Car> getCarsByUserId(int id);

    List<Car> getCars(CarFilterOptions carFilterOptions, User authentication);

    Car getCarById(int id);

    Car getCarById(int id, User authentication);

    Car getCarByLicensePlate(String licensePlate);

    Car createCar(Car car, User authentication);

    Car updateCar(Car car, User authentication);

    void deleteCar(int carId, User authentication);

    Long getCarCount();
}
