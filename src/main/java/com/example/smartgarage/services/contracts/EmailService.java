package com.example.smartgarage.services.contracts;

import com.example.smartgarage.models.User;

import javax.mail.MessagingException;
import java.io.File;

public interface EmailService {
    void sendLoginInformation(String email, String username, String password);

    void sendResetPasswordEmail(User user, String resetPasswordLink);

    void sendPdfDownloadEmail(User user, File pdf) throws MessagingException;
}
