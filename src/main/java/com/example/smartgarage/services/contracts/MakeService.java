package com.example.smartgarage.services.contracts;

import com.example.smartgarage.models.Make;
import com.example.smartgarage.models.User;

import java.util.List;

public interface MakeService {
    List<Make> getMakes();

    Make getMakeById(int id);

    Make getMakeByName(String name);

    Make createMake(Make make, User authentication);

    Make updateMake(Make make, User authentication);

    void deleteMake(int id, User authentication);
}
