package com.example.smartgarage.services.contracts;

import com.example.smartgarage.models.VisitCurrency;
import com.example.smartgarage.models.enums.CurrencyCode;

import java.util.List;

public interface CurrencyService {
    List<VisitCurrency> getCurrencies();

    VisitCurrency getCurrencyById(int currencyId);

    VisitCurrency getCurrencyByName(CurrencyCode name);
}
