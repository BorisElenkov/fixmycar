package com.example.smartgarage.services.contracts;

import com.example.smartgarage.models.Role;

import java.util.List;

public interface RoleService {
    List<Role> getRoles();

    Role getRoleById(int roleId);

    Role getRoleByName(String name);
}
