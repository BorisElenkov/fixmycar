package com.example.smartgarage.services.contracts;

import com.example.smartgarage.models.User;
import com.example.smartgarage.models.UserFilterOptions;

import java.util.List;

public interface UserService {
    List<User> getUsers(UserFilterOptions userFilterOptions);

    List<User> getUsers(UserFilterOptions userFilterOptions, User authentication);

    User getUserById(int userId);

    User getUserById(int userId, User authentication);

    User getUserByUsername(String username);

    User getUserByEmail(String email);

    User getUserByToken(String token);

    User createUser(User user, User authentication);

    void updateUser(User user, User authentication);

    void deleteUser(int userId, User authentication);

    Long getUsersCount();

    Long getEmployeeCount();

    Long getCustomerCount();

    User changePassword(int userId, User authentication, String password);

    void resetForgottenPassword(int userId, String password);

    void updateToken(String token, String email);

    void reevaluateLevel(User user);
}
