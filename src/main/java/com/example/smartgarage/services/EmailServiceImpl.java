package com.example.smartgarage.services;

import com.example.smartgarage.models.User;
import com.example.smartgarage.services.contracts.EmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;

import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.File;

@Service
public class EmailServiceImpl implements EmailService {

    private final JavaMailSender javaMailSender;

    @Autowired
    public EmailServiceImpl(JavaMailSender javaMailSender) {
        this.javaMailSender = javaMailSender;
    }

    public void sendLoginInformation(String email, String username, String password) {
        String subject = "Login information for your new account";
        String text = "Dear customer,\n\n" +
                "Your account has been created. Please find below your login information:\n\n" +
                "Username: " + username + "\n" +
                "Password: " + password + "\n\n" +
                "Please use this information to access your account.\n" +
                "We highly recommend that you change your password immediately after you log in for the first time!\n\n" +
                "Thank you for using our service!\n\n" +
                "Best regards,\n" +
                "FixMyCar Team";
        SimpleMailMessage message = new SimpleMailMessage();
        message.setTo(email);
        message.setFrom("office.FixMyCar@gmail.com");
        message.setSubject(subject);
        message.setText(text);
        javaMailSender.send(message);
    }

    public void sendResetPasswordEmail(User user, String resetPasswordLink) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom("office.FixMyCar@gmail.com");
        message.setTo(user.getEmail());
        message.setSubject("Reset Your Password");
        message.setText("Dear " + user.getUsername() + ",\n\n" +
                "You have requested to reset your password. To reset your password, please click the following link:\n\n" +
                resetPasswordLink + "\n\n" +
                "If you did not request this password reset, please ignore this message.\n\n" +
                "Best regards,\n" +
                "FixMyCar Team");
        javaMailSender.send(message);
    }

    @Override
    public void sendPdfDownloadEmail(User user, File pdf) throws MessagingException {
        MimeMessage message = javaMailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message, true);
        helper.setFrom("office.FixMyCar@gmail.com");
        helper.setTo(user.getEmail());
        helper.setSubject("Visit report PDF");
        helper.setText("Dear " + user.getUsername() + ",\n\n" +
                "You have requested a PDF of your visit report. Please find it attached.\n\n" +
                "Best regards,\n" +
                "FixMyCar Team");

        FileSystemResource file = new FileSystemResource(pdf);
        helper.addAttachment("Visit Report.pdf", file);

        javaMailSender.send(message);
    }
}
