package com.example.smartgarage.services;

import com.example.smartgarage.models.VisitCurrency;
import com.example.smartgarage.models.enums.CurrencyCode;
import com.example.smartgarage.repositories.contracts.CurrencyRepository;
import com.example.smartgarage.services.contracts.CurrencyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CurrencyServiceImpl implements CurrencyService {
    private final CurrencyRepository currencyRepository;

    @Autowired
    public CurrencyServiceImpl(CurrencyRepository currencyRepository) {
        this.currencyRepository = currencyRepository;
    }

    @Override
    public List<VisitCurrency> getCurrencies() {
        return currencyRepository.getCurrencies();
    }

    @Override
    public VisitCurrency getCurrencyById(int currencyId) {
        return currencyRepository.getCurrencyById(currencyId);
    }

    @Override
    public VisitCurrency getCurrencyByName(CurrencyCode name) {
        return currencyRepository.getCurrencyByName(name);
    }
}
