package com.example.smartgarage.services;

import com.example.smartgarage.exceptions.AuthorizationException;
import com.example.smartgarage.exceptions.EntityDuplicateException;
import com.example.smartgarage.exceptions.EntityNotFoundException;
import com.example.smartgarage.models.Car;
import com.example.smartgarage.models.CarFilterOptions;
import com.example.smartgarage.models.User;
import com.example.smartgarage.repositories.contracts.CarRepository;
import com.example.smartgarage.repositories.contracts.VisitRepository;
import com.example.smartgarage.services.contracts.CarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CarServiceImpl implements CarService {
    private final CarRepository carRepository;
    private final VisitRepository visitRepository;

    @Autowired
    public CarServiceImpl(CarRepository carRepository, VisitRepository visitRepository) {
        this.carRepository = carRepository;
        this.visitRepository = visitRepository;
    }

    @Override
    public List<Car> getCarsByUserId(int userId) {
        return carRepository.getCarsByUserId(userId);
    }

    @Override
    public List<Car> getCars(CarFilterOptions carFilterOptions, User authentication) {
        checkEmployeeRole(authentication);
        return carRepository.getCars(carFilterOptions);
    }

    @Override
    public Car getCarById(int carId) {
        return carRepository.getCarById(carId);
    }

    @Override
    public Car getCarById(int id, User authentication) {
        Car car = carRepository.getCarById(id);
        checkAuthorization(authentication, car);
        return carRepository.getCarById(id);
    }

    @Override
    public Car getCarByLicensePlate(String licensePlate) {
        return carRepository.getCarByLicensePlate(licensePlate);
    }

    @Override
    public Car createCar(Car car, User authentication) {
        checkEmployeeRole(authentication);
        boolean duplicateExists = true;

        try {
            carRepository.getCarByLicensePlate(car.getLicensePlate());
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }

        if (duplicateExists) {
            throw new EntityDuplicateException("Car", "license plate", car.getLicensePlate());
        }

        if (car.getUser().getRole().getRoleName().equalsIgnoreCase("Employee")) {
            throw new IllegalArgumentException("Cars can be added to customers only.");
        }
        carRepository.createCar(car);
        return car;
    }

    @Override
    public Car updateCar(Car car, User authentication) {
        checkEmployeeRole(authentication);
        boolean duplicateExists = true;

        try {
            Car existingCar = carRepository.getCarByLicensePlate(car.getLicensePlate());
            if (existingCar.getCarId() == car.getCarId()) {
                duplicateExists = false;
            }
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }

        if (duplicateExists) {
            throw new EntityDuplicateException("Car", "license plate", car.getLicensePlate());
        }
        carRepository.updateCar(car);
        return car;
    }

    @Override
    public void deleteCar(int carId, User authentication) {
        checkEmployeeRole(authentication);
        visitRepository.deleteVisits(carId);
        carRepository.deleteCar(carId);
    }

    @Override
    public Long getCarCount() {
        return carRepository.getCarCount();
    }

    private void checkEmployeeRole(User user) {
        if (!user.getRole().getRoleName().equalsIgnoreCase("Employee")) {
            throw new AuthorizationException("Only Employees can create, update or delete cars.");
        }
    }

    private void checkAuthorization(User user, Car car) {
        if (!(car.getUser().equals(user) || user.getRole().getRoleName().equalsIgnoreCase("Employee"))) {
            throw new AuthorizationException("Only Employees or owners can view cars.");
        }
    }
}
