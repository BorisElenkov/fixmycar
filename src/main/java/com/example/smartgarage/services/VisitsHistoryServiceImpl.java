package com.example.smartgarage.services;

import com.example.smartgarage.exceptions.AuthorizationException;
import com.example.smartgarage.models.Car;
import com.example.smartgarage.models.User;
import com.example.smartgarage.models.VisitsHistory;
import com.example.smartgarage.models.VisitsHistoryFilterOptions;
import com.example.smartgarage.repositories.contracts.VisitsHistoryRepository;
import com.example.smartgarage.services.contracts.VisitsHistoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class VisitsHistoryServiceImpl implements VisitsHistoryService {

    private final VisitsHistoryRepository visitsHistoryRepository;

    @Autowired
    public VisitsHistoryServiceImpl(VisitsHistoryRepository visitsHistoryRepository) {
        this.visitsHistoryRepository = visitsHistoryRepository;
    }

    @Override
    public List<VisitsHistory> getVisitsHistory(VisitsHistoryFilterOptions filterOptions,
                                                User user, Optional<Car> car) {
        if (car.isPresent()) {
            checkAuthorization(user, car.get());
        } else {
            checkAuthorization(user);
        }
        return visitsHistoryRepository.getVisitsHistory(filterOptions);
    }

    private void checkAuthorization(User user, Car car) {
        if (!(car.getUser().equals(user) ||
                user.getRole().getRoleName().equalsIgnoreCase("Employee"))) {
            throw new AuthorizationException("A customer can view only the repairs history of his own cars. " +
                    "Only employees can view the history of all repairs.");
        }
    }

    private void checkAuthorization(User user) {
        if (!user.getRole().getRoleName().equalsIgnoreCase("Employee")) {
            throw new AuthorizationException("Only employees can view the history of all repairs.");
        }
    }
}
