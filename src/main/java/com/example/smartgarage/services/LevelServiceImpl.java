package com.example.smartgarage.services;

import com.example.smartgarage.models.Level;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.smartgarage.services.contracts.LevelService;
import com.example.smartgarage.repositories.contracts.LevelRepository;

import java.util.List;

@Service
public class LevelServiceImpl implements LevelService {
    private final LevelRepository levelRepository;

    @Autowired
    public LevelServiceImpl(LevelRepository levelRepository) {
        this.levelRepository = levelRepository;
    }

    @Override
    public List<Level> getLevels() {
        return levelRepository.getLevels();
    }

    @Override
    public Level getLevelById(int levelId) {
        return levelRepository.getLevelById(levelId);
    }
}
