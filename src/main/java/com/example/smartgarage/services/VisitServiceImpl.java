package com.example.smartgarage.services;

import com.example.smartgarage.exceptions.AuthorizationException;
import com.example.smartgarage.exceptions.CustomException;
import com.example.smartgarage.exceptions.EntityNotFoundException;
import com.example.smartgarage.models.*;
import com.example.smartgarage.models.enums.StatusName;
import com.example.smartgarage.repositories.contracts.RepairRepository;
import com.example.smartgarage.repositories.contracts.StatusRepository;
import com.example.smartgarage.repositories.contracts.VisitsHistoryRepository;
import com.example.smartgarage.services.contracts.UserService;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.smartgarage.services.contracts.VisitService;

import com.example.smartgarage.repositories.contracts.VisitRepository;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;

@Service
public class VisitServiceImpl implements VisitService {
    private final VisitRepository visitRepository;
    private final VisitsHistoryRepository visitsHistoryRepository;
    private final RepairRepository repairRepository;
    private final StatusRepository statusRepository;
    private final UserService userService;

    @Autowired
    public VisitServiceImpl(VisitRepository visitRepository, VisitsHistoryRepository visitsHistoryRepository,
                            RepairRepository repairRepository, StatusRepository statusRepository,
                            UserService userService) {
        this.visitRepository = visitRepository;
        this.visitsHistoryRepository = visitsHistoryRepository;
        this.repairRepository = repairRepository;
        this.statusRepository = statusRepository;
        this.userService = userService;
    }

    @Override
    public List<Visit> getVisits(VisitFilterOptions visitFilterOptions, User user, Optional<Car> car) {
        if (car.isPresent()) {
            checkAuthorization(user, car.get());
        } else {
            checkAuthorization(user);
        }
        return visitRepository.getVisits(visitFilterOptions);
    }

    @Override
    public Visit getVisitById(int visitId) {
        return visitRepository.getVisitById(visitId);
    }

    @Override
    public Visit getVisitById(int visitId, User user, Optional<Car> car) {
        if (car.isPresent()) {
            checkAuthorization(user, car.get());
        } else {
            checkAuthorization(user);
        }
        return visitRepository.getVisitById(visitId);
    }

    @Override
    public void createVisit(Visit visit, User user) {
        checkAuthorization(user);
        visitRepository.createVisit(visit);
    }

    @Override
    public void addRepairToVisit(int repairId, int visitId, User user) {
        checkAuthorization(user);
        Visit visit = visitRepository.getVisitById(visitId);
        if (visit.getStatus().getName().equals(StatusName.COMPLETED) ||
                visit.getStatus().getName().equals(StatusName.CANCELLED)) {
            throw new IllegalArgumentException("Visits with status 'completed' or 'cancelled' cannot be updated.");
        }
        Repair repair = repairRepository.getRepairById(repairId);
        if (!repair.isActive()) {
            throw new CustomException("Inactive repairs cannot be added to visits.");
        }
        if (visit.getVisitsHistory().stream().anyMatch(h -> h.getVisitId() == visitId
                && h.getRepairId() == repairId)) {
            return;
        }
        User customer = visit.getCar().getUser();
        VisitsHistory history = new VisitsHistory();
        history.setRepairId(repairId);
        history.setRepairName(repair.getName());
        history.setVisitId(visitId);
        int discount = customer.getLevel().getDiscount();
        history.setPrice(repair.getPrice() * (100 - discount) / 100);
        visitsHistoryRepository.create(history);
    }

    @Override
    public void removeRepairFromVisit(int repairId, int visitId, User user) {
        checkAuthorization(user);
        Visit visit = visitRepository.getVisitById(visitId);
        Repair repair = repairRepository.getRepairById(repairId);
        if (visit.getStatus().getName().equals(StatusName.COMPLETED) ||
                visit.getStatus().getName().equals(StatusName.CANCELLED)) {
            throw new IllegalArgumentException("Visits with status 'completed' or 'cancelled' cannot be updated.");
        }
        if (visit.getVisitsHistory().stream().noneMatch(h -> h.getVisitId() == visitId
                && h.getRepairId() == repairId)) {
            throw new EntityNotFoundException("Repair", repairId);
        }
        visitsHistoryRepository.delete(visitId, repairId);
    }

    @Override
    public void changeStatus(int visitId, int statusId, User user) {
        checkAuthorization(user);
        Visit visit = visitRepository.getVisitById(visitId);
        Status newStatus = statusRepository.getStatusById(statusId);
        if (visit.getStatus().getName().equals(StatusName.COMPLETED) ||
                visit.getStatus().getName().equals(StatusName.CANCELLED)) {
            throw new IllegalArgumentException("Visits with status 'completed' or 'cancelled' cannot be modified.");
        }
        visit.setStatus(newStatus);
        visitRepository.updateVisit(visit);
        if (newStatus.getName().equals(StatusName.COMPLETED) || newStatus.getName().equals(StatusName.READY_FOR_PICKUP)) {
            userService.reevaluateLevel(visit.getCar().getUser());
        }
    }

    @Override
    public void updateVisit(Visit visit, User user) {
        checkAuthorization(user);
        if (visit.getStatus().getName().equals(StatusName.COMPLETED) ||
                visit.getStatus().getName().equals(StatusName.CANCELLED)) {
            throw new IllegalArgumentException("Visits with status 'completed' or 'cancelled' cannot be modified.");
        }
        visitRepository.updateVisit(visit);
    }

    @Override
    public void deleteVisit(int visitId, User user) {
        checkAuthorization(user);
        Visit visit = visitRepository.getVisitById(visitId);
        if (visit.getStatus().getName().equals(StatusName.COMPLETED)) {
            throw new IllegalArgumentException("Visits with status 'completed' cannot be modified.");
        }
        Status cancelled = statusRepository.getStatusById(5);
        visit.setStatus(cancelled);
        visitsHistoryRepository.delete(visitId);
        visitRepository.updateVisit(visit);
        userService.reevaluateLevel(visit.getCar().getUser());
    }

    @Override
    public Double getTotalPrice(int visitId) {
        return visitRepository.getTotalPrice(visitId);
    }

    public Double getTotalPriceDiffCurrency(int visitId, String toCurrency) throws IOException {
        Visit visit = visitRepository.getVisitById(visitId);
        Double total = getTotalPrice(visitId);
        return getConvertedTotal("BGN", toCurrency, total, visit.getVisitDate());
    }

    private Double getConvertedTotal(String fromCurrency, String toCurrency,
                                     Double amount, LocalDate visitDate) throws IOException {
        String apiKey = "9gePFdFyCfzsbfcM1bT0wVLBlSDmOumP";
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        String url = "https://api.apilayer.com/exchangerates_data/convert" +
                "?to=" + toCurrency +
                "&from=" + fromCurrency +
                "&amount=" + amount +
                "&date=" + visitDate.format(formatter);

        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .addHeader("apikey", apiKey)
                .method("GET", null)
                .url(url)
                .build();

        Response response = client.newCall(request).execute();
        String responseBody = response.body().string();

        JSONObject jsonObject = new JSONObject(responseBody);
        return jsonObject.getDouble("result");
    }

    private void checkAuthorization(User user, Car car) {
        if (!(car.getUser().equals(user) ||
                user.getRole().getRoleName().equalsIgnoreCase("Employee"))) {
            throw new AuthorizationException("A customer can view only the visits of his own cars. " +
                    "Only employees can view all visits.");
        }
    }

    private void checkAuthorization(User user) {
        if (!user.getRole().getRoleName().equalsIgnoreCase("Employee")) {
            throw new AuthorizationException("Only employees can view all visits and modify visits.");
        }
    }
}
