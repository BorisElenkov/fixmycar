package com.example.smartgarage.services;

import com.example.smartgarage.exceptions.AuthorizationException;
import com.example.smartgarage.exceptions.CustomException;
import com.example.smartgarage.exceptions.EntityDuplicateException;
import com.example.smartgarage.exceptions.EntityNotFoundException;
import com.example.smartgarage.models.Repair;
import com.example.smartgarage.models.RepairFilterOptions;
import com.example.smartgarage.models.User;
import com.example.smartgarage.repositories.contracts.RepairRepository;
import com.example.smartgarage.services.contracts.RepairService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RepairServiceImpl implements RepairService {

    private final RepairRepository repairRepository;

    @Autowired
    public RepairServiceImpl(RepairRepository repairRepository) {
        this.repairRepository = repairRepository;
    }

    @Override
    public List<Repair> getRepairs(RepairFilterOptions repairFilterOptions) {
        return repairRepository.getRepairs(repairFilterOptions);
    }

    @Override
    public Repair getRepairById(int repairId) {
        return repairRepository.getRepairById(repairId);
    }

    @Override
    public Repair getRepairById(int repairId, User user) {
        checkEmployeeRole(user);
        return getRepairById(repairId);
    }

    @Override
    public Repair getRepairByName(String name) {
        return repairRepository.getRepairByName(name);
    }

    @Override
    public void createRepair(Repair repair, User user) {
        checkEmployeeRole(user);
        boolean repairExists = true;
        try {
            repairRepository.getRepairByName(repair.getName());
        } catch (EntityNotFoundException e) {
            repairExists = false;
        }
        if (repairExists) {
            throw new EntityDuplicateException("Repair service", "name", repair.getName());
        }
        repairRepository.createRepair(repair);
    }

    @Override
    public void updateRepair(Repair repair, User user) {
        checkEmployeeRole(user);
        boolean duplicateExists = true;
        try {
            Repair existingRepair = repairRepository.getRepairByName(repair.getName());
            if (existingRepair.getRepairId() == repair.getRepairId()) {
                duplicateExists = false;
            }
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }

        if (duplicateExists) {
            throw new EntityDuplicateException("Repair service", "name", repair.getName());
        }

        if (!repair.isActive()) {
            throw new CustomException("Inactive repairs cannot be updated.");
        }
        repairRepository.updateRepair(repair);
    }

    @Override
    public void deactivateRepair(int repairId, User user) {
        checkEmployeeRole(user);
        Repair repairToDelete = repairRepository.getRepairById(repairId);
        if (repairToDelete.isActive()) {
            repairToDelete.setActive(false);
        }
        repairRepository.updateRepair(repairToDelete);
    }

    @Override
    public void activateRepair(int repairId, User user) {
        checkEmployeeRole(user);
        Repair repairToActivate = repairRepository.getRepairById(repairId);
        repairToActivate.setActive(true);
        repairRepository.updateRepair(repairToActivate);
    }

    private void checkEmployeeRole(User user) {
        if (!user.getRole().getRoleName().equalsIgnoreCase("Employee")) {
            throw new AuthorizationException("Only Employees can modify repairs.");
        }
    }
}
