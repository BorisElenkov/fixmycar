package com.example.smartgarage.validations.constraints;

import com.example.smartgarage.validations.validators.PasswordValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.PARAMETER;

@Documented
@Target({ FIELD, PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = PasswordValidator.class)
public @interface Password {
    String message() default "Password should contain a capital letter, digit, and special symbol (+, -, *, &, ^, …).";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
