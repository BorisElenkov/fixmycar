package com.example.smartgarage.validations.validators;

import com.example.smartgarage.validations.constraints.Password;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class PasswordValidator implements ConstraintValidator<Password, String> {
    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        char[] chars = value.toCharArray();
        boolean isValid = true;
        boolean containsUpprecase = false;
        boolean containsDigit = false;
        boolean containsSpecialChar = false;

        for (char c : chars) {
            if (Character.isUpperCase(c)) {
                containsUpprecase = true;
            } else if (Character.isDigit(c)) {
                containsDigit = true;
            } else if (c >= 33 && c <= 47) {
                containsSpecialChar = true;
            }
        }

        if (!containsUpprecase || !containsDigit || !containsSpecialChar) {
            isValid = false;
        }

        return isValid;
    }

    @Override
    public void initialize(Password constraintAnnotation) {
        ConstraintValidator.super.initialize(constraintAnnotation);
    }
}
