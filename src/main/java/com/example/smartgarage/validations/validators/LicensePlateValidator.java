package com.example.smartgarage.validations.validators;

import com.example.smartgarage.validations.constraints.LicensePlate;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class LicensePlateValidator implements ConstraintValidator<LicensePlate, String> {
    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        return isValidRegularCarNumber(value) ||
                isValidElectricCarNumber(value) ||
                isValidCustomCarNumber(value) ||
                isValidTemporaryCarNumber(value) ||
                isValidMilitaryCarNumber(value) ||
                isValidCivilProtectionCarNumber(value) ||
                isValidForeignerCarNumber(value) ||
                isValidDiplomaticCarNumber(value);
    }

    @Override
    public void initialize(LicensePlate constraintAnnotation) {
        ConstraintValidator.super.initialize(constraintAnnotation);
    }

    private boolean isValidRegularCarNumber(String value) {
        if (value.length() == 7 && value.matches("[EABKMPCTXHY]{1}[0-9]{4}[ABEKMHOPCTYX]{2}")) {
            return true;
        }
        if (value.length() == 8 && value.matches("(BT|BH|BP|EB|TX|KH|OB|PA|PK|EH|PB|PP|CC|CH|CM|CO|CA|CB|CT)[0-9]{4}[ABEKMHOPCTYX]{2}")) {
            return true;
        }
        return false;
    }

    private boolean isValidElectricCarNumber(String value) {
        return value.matches("(EA)[0-9]{4}[ABEKMHOPCTYX]{2}");
    }

    private boolean isValidCustomCarNumber(String value) {
        if (value.length() == 7 && value.matches("[EABKMPCTXHY]{1}[ABEKMHOPCTYX0123456789]{5,6}")) {
            return true;
        }
        if (value.length() == 8 && value.matches("(BT|BH|BP|EA|EB|TX|KH|OB|PA|PK|EH|PB|PP|CC|CH|CM|CO|CA|CB|CT)[ABEKMHOPCTYX0123456789]{5,6}")) {
            return true;
        }
        return false;
    }

    private boolean isValidTemporaryCarNumber(String value) {
        return value.matches("[0-9]{3}[TNMB]{1}[0-9]{3}");
    }

    private boolean isValidMilitaryCarNumber(String value) {
        return value.matches("(BA)[0-9]{6}");
    }

    private boolean isValidCivilProtectionCarNumber(String value) {
        return value.matches("(CP)[0-9]{5}");
    }

    private boolean isValidForeignerCarNumber(String value) {
        return value.matches("(XX|XH)[0-9]{4}");
    }

    private boolean isValidDiplomaticCarNumber(String value) {
        if (value.length() == 5 && value.matches("^(C)(?!06|54|55|93|94|97)([0-9]{2}|(0[1-9]|[1-8][0-9]))[0-9]{2}$")) {
            return true;
        }
        if (value.length() == 6 && value.matches("^(CC|CT)(?!06|54|55|93|94|97)([0-9]{2}|(0[1-9]|[1-8][0-9]))[0-9]{2}$")) {
            return true;
        }
        return false;
    }
}
