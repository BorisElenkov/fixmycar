package com.example.smartgarage.exceptions;

public class EntityNotFoundException extends RuntimeException {
    public EntityNotFoundException(String message) {
        super(message);
    }

    public EntityNotFoundException(String item, int id) {
        this(item, "id", String.valueOf(id));
    }

    public EntityNotFoundException(String item, String attribute, String value) {
        super(String.format("%s with %s %s not found.", item, attribute, value));
    }
}