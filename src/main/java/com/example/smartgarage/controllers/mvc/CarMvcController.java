package com.example.smartgarage.controllers.mvc;

import com.example.smartgarage.exceptions.AuthorizationException;
import com.example.smartgarage.exceptions.CustomException;
import com.example.smartgarage.exceptions.EntityNotFoundException;
import com.example.smartgarage.helpers.*;
import com.example.smartgarage.models.*;
import com.example.smartgarage.models.dtos.*;
import com.example.smartgarage.services.contracts.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/cars")
public class CarMvcController {
    private final CarService carService;
    private final CarMapper carMapper;
    private final AuthenticationHelper authenticationHelper;
    private final VisitMapper visitMapper;
    private final VisitService visitService;
    private final RepairService repairService;
    private final VisitFilterMapper visitFilterMapper;
    private final StatusService statusService;
    private final VisitsHistoryFilterMapper visitsHistoryFilterMapper;
    private final VisitsHistoryService visitsHistoryService;
    private final VisitsHistoryMapper visitsHistoryMapper;

    @Autowired
    public CarMvcController(CarService carService, CarMapper carMapper, AuthenticationHelper authenticationHelper,
                            VisitMapper visitMapper, VisitService visitService, RepairService repairService,
                            VisitFilterMapper visitFilterMapper, StatusService statusService,
                            VisitsHistoryFilterMapper visitsHistoryFilterMapper,
                            VisitsHistoryService visitsHistoryService,
                            VisitsHistoryMapper visitsHistoryMapper) {
        this.carService = carService;
        this.carMapper = carMapper;
        this.authenticationHelper = authenticationHelper;
        this.visitMapper = visitMapper;
        this.visitService = visitService;
        this.repairService = repairService;
        this.visitFilterMapper = visitFilterMapper;
        this.statusService = statusService;
        this.visitsHistoryFilterMapper = visitsHistoryFilterMapper;
        this.visitsHistoryService = visitsHistoryService;
        this.visitsHistoryMapper = visitsHistoryMapper;
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @ModelAttribute("currentUserId")
    public Integer populateCurrentUserId(HttpSession session) {
        return (Integer) session.getAttribute("currentUserId");
    }

    @ModelAttribute("isEmployee")
    public boolean populateIsEmployee(HttpSession session) {
        if (session.getAttribute("role") != null) {
            return session.getAttribute("role").equals("Employee");
        }
        return false;
    }

    @ModelAttribute("statuses")
    public List<Status> populateStatuses() {
        return statusService.getStatuses();
    }

    @GetMapping
    public String showAllCars(@ModelAttribute("filter") CarFilterDto carFilterDto,
                              HttpSession session, Model model) {
        User authentication;
        try {
            authentication = authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "redirect:/authentication/login";
        }
        try {
            CarFilterOptions carFilterOptions = new CarFilterOptions(
                    carFilterDto.getLicensePlate(),
                    carFilterDto.getVin(),
                    carFilterDto.getYear(),
                    carFilterDto.getUsername(),
                    carFilterDto.getModelName(),
                    carFilterDto.getMakeName(),
                    carFilterDto.getPhone(),
                    carFilterDto.getSortBy(),
                    carFilterDto.getOrderBy());
            model.addAttribute(carService.getCars(carFilterOptions, authentication));
            model.addAttribute("filter", carFilterDto);
            return "CarsView";
        } catch (AuthorizationException e) {
            model.addAttribute("error", e.getMessage());
            return "AccessDeniedView";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
    }

    @GetMapping("/{carId}/visits")
    public String showVisits(@PathVariable int carId,
                             @ModelAttribute("filter") VisitFilterDto filterDto,
                             HttpSession session, Model model) {
        User user;
        try {
            user = authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "redirect:/authentication/login";
        }
        try {
            Car car = carService.getCarById(carId);
            model.addAttribute("car", car);
            VisitFilterOptions filterOptions = visitFilterMapper.dtoToObject(filterDto);
            filterOptions.setCarId(Optional.of(carId));
            List<Visit> visits = visitService.getVisits(filterOptions, user, Optional.of(car));
            List<VisitDtoOut> visitDtoOuts = visitMapper.getVisitDtoOutListFromVisit(visits);
            model.addAttribute("filter", filterDto);
            model.addAttribute("visits", visitDtoOuts);
            return "VisitsForSingleCarView";
        } catch (AuthorizationException e) {
            model.addAttribute("error", e.getMessage());
            return "AccessDeniedView";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
    }

    @GetMapping("/{carId}/visits/new")
    public String showNewVisitPage(@PathVariable int carId,
                                   HttpSession session, Model model) {
        try {
            authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "redirect:/authentication/login";
        }
        Car car = carService.getCarById(carId);
        model.addAttribute("car", car);
        return "VisitCreateView";
    }

    @PostMapping("/{carId}/visits/new")
    public String createVisit(@PathVariable int carId,
                              HttpSession session, Model model) {
        User user;
        try {
            user = authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "redirect:/authentication/login";
        }
        try {
            Car car = carService.getCarById(carId);
            model.addAttribute("car", car);
            VisitDtoIn visitDtoIn = new VisitDtoIn(carId);
            Visit visit = visitMapper.dtoToObject(visitDtoIn);
            visitService.createVisit(visit, user);
            model.addAttribute("visit", visit);
            model.addAttribute("carId", carId);
            model.addAttribute("visitId", visit.getVisitId());
            return "redirect:/cars/" + car.getCarId() + "/visits/" + visit.getVisitId() + "/repairs/new";
        } catch (AuthorizationException e) {
            model.addAttribute("error", e.getMessage());
            return "AccessDeniedView";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
    }

    @GetMapping("/{carId}/visits/{visitId}/repairs/new")
    public String showAddRepairsToVisitPage(@PathVariable int carId, @PathVariable int visitId,
                                            @ModelAttribute("filter") RepairFilterDto filterDto,
                                            HttpSession session, Model model) {
        User user;
        try {
            user = authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "redirect:/authentication/login";
        }
        try {
            Car car = carService.getCarById(carId);
            model.addAttribute("car", car);
            Visit visit = visitService.getVisitById(visitId, user, Optional.of(car));
            model.addAttribute("visit", visit);
            RepairFilterOptions filterOptions = new RepairFilterOptions(
                    filterDto.getName(), filterDto.getMinPrice(), filterDto.getMaxPrice(),
                    filterDto.getActive(), filterDto.getSortBy(), filterDto.getSortOrder());
            model.addAttribute("filter", filterDto);
            model.addAttribute("repairs", repairService.getRepairs(filterOptions));
            model.addAttribute("addedRepairs", new HashSet<Integer>());
            return "VisitAddRepairsView";
        } catch (AuthorizationException e) {
            model.addAttribute("error", e.getMessage());
            return "AccessDeniedView";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
    }

    @PostMapping("/{carId}/visits/{visitId}/repairs/new")
    public String addRepairsToVisit(@PathVariable int carId, @PathVariable int visitId,
                                    @RequestParam("selectedRepairs") List<String> selectedRepairs,
                                    HttpSession session, Model model) {
        User user;
        try {
            user = authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "redirect:/authentication/login";
        }
        try {
            Visit visit = visitService.getVisitById(visitId);
            for (String repairName : selectedRepairs) {
                Repair repair = repairService.getRepairByName(repairName);
                visitService.addRepairToVisit(repair.getRepairId(), visitId, user);
            }
            return "redirect:/visits/" + visitId;
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        } catch (AuthorizationException | IllegalArgumentException e) {
            model.addAttribute("error", e.getMessage());
            return "AccessDeniedView";
        } catch (CustomException e) {
            model.addAttribute("error", e.getMessage());
            return "VisitActivateRepairsMessageView";
        }
    }

    @GetMapping("/{carId}/visits-history")
    public String showVisitsHistory(@PathVariable int carId,
                                    @ModelAttribute("filter") VisitsHistoryFilterDto filterDto,
                                    HttpSession session, Model model) {
        User user;
        try {
            user = authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "redirect:/authentication/login";
        }
        try {
            Car car = carService.getCarById(carId);
            model.addAttribute("car", car);
            VisitsHistoryFilterOptions filterOptions = visitsHistoryFilterMapper.dtoToObject(filterDto);
            filterOptions.setCarId(Optional.of(carId));
            List<VisitsHistory> history = visitsHistoryService.getVisitsHistory(filterOptions, user, Optional.of(car));
            List<VisitsHistoryDtoOut> result = visitsHistoryMapper.getDtoOutListFromVisitsHistory(history);
            model.addAttribute("history", result);
            model.addAttribute("filter", filterDto);
            return "VisitsHistoryForSingleCarView";
        } catch (
                AuthorizationException e) {
            model.addAttribute("error", e.getMessage());
            return "AccessDeniedView";
        } catch (
                EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
    }
}
