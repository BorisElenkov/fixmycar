package com.example.smartgarage.controllers.mvc;

import com.example.smartgarage.exceptions.EntityNotFoundException;
import com.example.smartgarage.models.User;
import com.example.smartgarage.models.dtos.PasswordDto;
import com.example.smartgarage.services.contracts.EmailService;
import com.example.smartgarage.services.contracts.UserService;
import net.bytebuddy.utility.RandomString;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@Controller
@RequestMapping("/")
public class ForgotPasswordMvcController {
    private final JavaMailSender javaMailSender;
    private final UserService userService;
    private final EmailService emailService;

    @Autowired
    public ForgotPasswordMvcController(JavaMailSender javaMailSender, UserService userService, EmailService emailService) {
        this.javaMailSender = javaMailSender;
        this.userService = userService;
        this.emailService = emailService;
    }

    @GetMapping("/forgot-password")
    public String showForgotPasswordForm() {
        return "PasswordForgotView";
    }

    @PostMapping("/forgot-password")
    public String processForgotPassword(@RequestParam("email") String email, Model model) {
        String token = RandomString.make(30);

        try {
            userService.updateToken(token, email);
            String resetPasswordLink = "http://localhost:8080/reset-password?token=" + token;
            User user = userService.getUserByEmail(email);
            emailService.sendResetPasswordEmail(user, resetPasswordLink);
            model.addAttribute("message", "We have sent a reset password link to your email. Please check.");

        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
        return "PasswordForgotView";
    }

    @GetMapping("/reset-password")
    public String showResetPasswordForm(@Param(value = "token") String token, Model model) {
        try {
            User user = userService.getUserByToken(token);
            model.addAttribute("token", token);
            model.addAttribute("passwordDto", new PasswordDto());
            return "PasswordResetView";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        } catch (IllegalArgumentException e){
            model.addAttribute("error", e.getMessage());
            return "AccessDeniedView";
        }
    }

    @PostMapping("/reset-password")
    public String processResetPassword(@Valid PasswordDto passwordDto,
                                       BindingResult bindingResult,
                                       HttpServletRequest request,
                                       Model model) {
        String token = request.getParameter("token");
        User user;
        try {
            user = userService.getUserByToken(token);
        } catch (EntityNotFoundException e) {
            model.addAttribute("message", "Invalid token");
            return "PasswordResetView";
        } catch (IllegalArgumentException e){
            model.addAttribute("error", e.getMessage());
            return "AccessDeniedView";
        }

        if (!passwordDto.getPassword().equals(passwordDto.getConfirmPassword())) {
            bindingResult.rejectValue("confirmPassword", "not_same_password",
                    "Password confirm should match password!");
            model.addAttribute("token", token);
            return "PasswordResetView";
        }
        if (bindingResult.hasErrors()) {
            model.addAttribute("token", token);
            return "PasswordResetView";
        }
        userService.resetForgottenPassword(user.getUserId(), passwordDto.getPassword());
        model.addAttribute("message", "You have successfully changed your password");
        return "SuccessView";
    }
}
