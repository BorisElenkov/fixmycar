package com.example.smartgarage.controllers.mvc;

import com.example.smartgarage.exceptions.AuthorizationException;
import com.example.smartgarage.exceptions.CustomException;
import com.example.smartgarage.exceptions.EntityNotFoundException;
import com.example.smartgarage.helpers.AuthenticationHelper;
import com.example.smartgarage.helpers.PdfCreator;
import com.example.smartgarage.helpers.VisitFilterMapper;
import com.example.smartgarage.helpers.VisitMapper;
import com.example.smartgarage.models.*;
import com.example.smartgarage.models.dtos.CurrencyDto;
import com.example.smartgarage.models.dtos.RepairFilterDto;
import com.example.smartgarage.models.dtos.VisitDtoOut;
import com.example.smartgarage.models.dtos.VisitFilterDto;
import com.example.smartgarage.models.enums.CurrencyCode;
import com.example.smartgarage.services.contracts.*;
import com.itextpdf.text.DocumentException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.mail.MessagingException;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.io.File;
import java.io.IOException;
import java.util.*;

@Controller
@RequestMapping("/visits")
public class VisitMvcController {

    private final VisitFilterMapper visitFilterMapper;
    private final AuthenticationHelper authenticationHelper;
    private final VisitService visitService;
    private final VisitMapper visitMapper;
    private final RepairService repairService;
    private final StatusService statusService;
    private final CurrencyService currencyService;
    private final PdfCreator pdfCreator;
    private final EmailService emailService;

    @Autowired
    public VisitMvcController(VisitFilterMapper visitFilterMapper,
                              AuthenticationHelper authenticationHelper,
                              VisitService visitService, VisitMapper visitMapper,
                              RepairService repairService, StatusService statusService,
                              CurrencyService currencyService, PdfCreator pdfCreator, EmailService emailService) {
        this.visitFilterMapper = visitFilterMapper;
        this.authenticationHelper = authenticationHelper;
        this.visitService = visitService;
        this.visitMapper = visitMapper;
        this.repairService = repairService;
        this.statusService = statusService;
        this.currencyService = currencyService;
        this.pdfCreator = pdfCreator;
        this.emailService = emailService;
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @ModelAttribute("currentUserId")
    public Integer populateCurrentUserId(HttpSession session) {
        return (Integer) session.getAttribute("currentUserId");
    }

    @ModelAttribute("isEmployee")
    public boolean populateIsEmployee(HttpSession session) {
        if (session.getAttribute("role") != null) {
            return session.getAttribute("role").equals("Employee");
        }
        return false;
    }

    @GetMapping
    public String getVisits(@ModelAttribute("filter") VisitFilterDto filterDto,
                            HttpSession session, Model model) {
        User user;
        try {
            user = authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "redirect:/authentication/login";
        }
        try {
            VisitFilterOptions filterOptions = visitFilterMapper.dtoToObject(filterDto);
            List<Visit> visits = visitService.getVisits(filterOptions, user, Optional.empty());
            List<VisitDtoOut> result = visitMapper.getVisitDtoOutListFromVisit(visits);
            model.addAttribute("user", user);
            model.addAttribute("statuses", statusService.getStatuses());
            model.addAttribute("visits", result);
            model.addAttribute("filter", filterDto);
            return "VisitsView";
        } catch (AuthorizationException e) {
            model.addAttribute("error", e.getMessage());
            return "AccessDeniedView";
        }
    }

    @GetMapping("/{visitId}")
    public String getSingleVisit(@PathVariable int visitId,
                                 RedirectAttributes attributes,
                                 @RequestParam(name = "total", required = false) Double total,
                                 @RequestParam(name = "currency", required = false) String currency,
                                 HttpSession session, Model model) {
        User user;
        try {
            user = authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "redirect:/authentication/login";
        }
        try {
            Visit v = visitService.getVisitById(visitId);
            Car car = v.getCar();
            Visit visit = visitService.getVisitById(visitId, user, Optional.of(car));
            VisitDtoOut visitDtoOut = visitMapper.objectToDto(visit);
            if (total != null) {
                visitDtoOut.setTotalPrice(total);
            }
            if (currency != null) {
                visitDtoOut.setCurrency(currency);
            }
            model.addAttribute("user", user);
            model.addAttribute("customer", visit.getCar().getUser());
            model.addAttribute("car", car);
            model.addAttribute("visitDtoOut", visitDtoOut);
            model.addAttribute("visitId", visitDtoOut.getVisitId());
            attributes.addFlashAttribute("currency", visitDtoOut.getCurrency());
            return "VisitReportView";
        } catch (AuthorizationException e) {
            model.addAttribute("error", e.getMessage());
            return "AccessDeniedView";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
    }

    @GetMapping("/{visitId}/pdf")
    public String getVisitPdf(@PathVariable int visitId,
                              @RequestParam(name = "currency", required = false) String currency,
                              HttpSession session, Model model) {
        User user;
        try {
            user = authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "redirect:/authentication/login";
        }
        try {
            Visit v = visitService.getVisitById(visitId);
            Car car = v.getCar();
            Visit visit = visitService.getVisitById(visitId, user, Optional.of(car));
            VisitCurrency curr;
            if (currency != null) {
                curr = currencyService.getCurrencyByName(CurrencyCode.valueOf(currency));
            } else {
                curr = currencyService.getCurrencyById(16);
            }

            File pdf = pdfCreator.createPdf(visit, curr);

            emailService.sendPdfDownloadEmail(visit.getCar().getUser(), pdf);
            model.addAttribute("message", "We have sent you an e-mail with the Visit report in a PDF file. Please check your e-mail.");

            return "SuccessView";
        } catch (AuthorizationException e) {
            model.addAttribute("error", e.getMessage());
            return "AccessDeniedView";
        } catch (EntityNotFoundException | DocumentException | IOException | MessagingException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
    }


    @GetMapping("/{visitId}/currency")
    public String showChooseCurrencyPage(@PathVariable int visitId,
                                         HttpSession session, Model model) {
        try {
            User user = authenticationHelper.tryGetCurrentUser(session);
            Visit visit = visitService.getVisitById(visitId);
            model.addAttribute("user", user);
            model.addAttribute("currencies", currencyService.getCurrencies());
            model.addAttribute("currencyDto", new CurrencyDto());
            return "CurrencyChoiceView";
        } catch (AuthorizationException e) {
            return "redirect:/authentication/login";
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping("/{visitId}/currency")
    public String getSingleVisitDiffCurrency(@PathVariable int visitId,
                                             @Valid CurrencyDto currencyDto,
                                             BindingResult bindingResult,
                                             RedirectAttributes attributes,
                                             HttpSession session, Model model) {
        User user;
        try {
            user = authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "redirect:/authentication/login";
        }
        try {
            Visit v = visitService.getVisitById(visitId);
            Car car = v.getCar();
            Visit visit = visitService.getVisitById(visitId, user, Optional.of(car));
            VisitDtoOut visitDtoOut = visitMapper.objectToDto(visit);
            VisitCurrency currency = currencyService.getCurrencyById(currencyDto.getCurrencyId());
            Double totalPrice = visitService
                    .getTotalPriceDiffCurrency(visitId, currency.getCurrencyCode().toString());
            visitDtoOut.setTotalPrice(Math.round(totalPrice * 100.0) / 100.0);
            visitDtoOut.setCurrency(currency.getCurrencyCode().toString());
            model.addAttribute("user", user);
            model.addAttribute("customer", visit.getCar().getUser());
            model.addAttribute("car", visit.getCar());
            model.addAttribute("visitDtoOut", visitDtoOut);
            attributes.addFlashAttribute("total", visitDtoOut.getTotalPrice());
            attributes.addFlashAttribute("currency", visitDtoOut.getCurrency());
            return "redirect:/visits/" + visitId + "?total=" + visitDtoOut.getTotalPrice() + "&currency=" + visitDtoOut.getCurrency();
        } catch (AuthorizationException e) {
            model.addAttribute("error", e.getMessage());
            return "AccessDeniedView";
        } catch (EntityNotFoundException | IOException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
    }

    @GetMapping("/{visitId}/update")
    public String showUpdateVisitPage(@PathVariable int visitId,
                                      @ModelAttribute("filter") RepairFilterDto filterDto,
                                      HttpSession session, Model model) {
        User user;
        try {
            user = authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "redirect:/authentication/login";
        }
        try {
            Visit visit = visitService.getVisitById(visitId, user, Optional.empty());
            Car car = visit.getCar();
            model.addAttribute("car", car);
            model.addAttribute("visit", visit);
            RepairFilterOptions filterOptions = new RepairFilterOptions(
                    filterDto.getName(), filterDto.getMinPrice(), filterDto.getMaxPrice(),
                    filterDto.getActive(), filterDto.getSortBy(), filterDto.getSortOrder());
            model.addAttribute("filter", filterDto);
            List<Repair> repairs = repairService.getRepairs(filterOptions);
            model.addAttribute("repairs", repairs);
            Map<Integer, Boolean> addedRepairs = new HashMap<>();
            for (Repair repair : repairs) {
                if (visit.getVisitsHistory().stream().anyMatch(vh -> vh.getRepairId() == repair.getRepairId())) {
                    addedRepairs.put(repair.getRepairId(), true);
                } else {
                    addedRepairs.put(repair.getRepairId(), false);
                }
            }
            model.addAttribute("addedRepairs", addedRepairs);
            return "VisitUpdateView";
        } catch (AuthorizationException e) {
            model.addAttribute("error", e.getMessage());
            return "AccessDeniedView";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
    }

    @PostMapping("/{visitId}/update")
    public String updateVisit(@PathVariable int visitId,
                              @RequestParam("selectedRepairs") List<String> selectedRepairs,
                              HttpSession session, Model model) {
        User user;
        try {
            user = authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "redirect:/authentication/login";
        }
        try {
            Visit visit = visitService.getVisitById(visitId, user, Optional.empty());
            for (VisitsHistory vh : visit.getVisitsHistory()) {
                visitService.removeRepairFromVisit(vh.getRepairId(), visitId, user);
            }
            for (String repairName : selectedRepairs) {
                Repair repair = repairService.getRepairByName(repairName);
                visitService.addRepairToVisit(repair.getRepairId(), visitId, user);
            }
            return "redirect:/visits/{visitId}";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        } catch (AuthorizationException | IllegalArgumentException e) {
            model.addAttribute("error", e.getMessage());
            return "AccessDeniedView";
        } catch (CustomException e) {
            return "VisitActivateRepairsMessageView";
        }
    }

    @GetMapping("/{visitId}/status")
    public String showChangeStatusPage(@PathVariable int visitId,
                                       HttpSession session, Model model) {
        User user;
        try {
            user = authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "redirect:/authentication/login";
        }
        try {
            Visit visit = visitService.getVisitById(visitId, user, Optional.empty());

            model.addAttribute("statuses", statusService.getStatuses());
            model.addAttribute("visit", visit);
            return "VisitChangeStatusView";
        } catch (AuthorizationException e) {
            model.addAttribute("error", e.getMessage());
            return "AccessDeniedView";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
    }

    @PostMapping("/{visitId}/status")
    public String updateVisit(@PathVariable int visitId,
                              @RequestParam("statusId") int statusId,
                              HttpSession session, Model model) {
        User user;
        try {
            user = authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "redirect:/authentication/login";
        }
        try {
            visitService.changeStatus(visitId, statusId, user);
            return "redirect:/visits/{visitId}";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        } catch (AuthorizationException | IllegalArgumentException e) {
            model.addAttribute("error", e.getMessage());
            return "AccessDeniedView";
        }
    }

    @GetMapping("/{visitId}/delete")
    public String showDeleteVisitPage(@PathVariable int visitId,
                                      HttpSession session, Model model) {
        User user;
        try {
            user = authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "redirect:/authentication/login";
        }
        try {
            Visit visit = visitService.getVisitById(visitId, user, Optional.empty());

            model.addAttribute("visit", visit);
            return "VisitDeleteView";
        } catch (AuthorizationException e) {
            model.addAttribute("error", e.getMessage());
            return "AccessDeniedView";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
    }

    @PostMapping("/{visitId}/delete")
    public String deleteVisit(@PathVariable int visitId,
                              HttpSession session, Model model) {
        User user;
        try {
            user = authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "redirect:/authentication/login";
        }
        try {
            visitService.deleteVisit(visitId, user);
            return "redirect:/visits/{visitId}";
        } catch (
                EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        } catch (AuthorizationException |
                 IllegalArgumentException e) {
            model.addAttribute("error", e.getMessage());
            return "AccessDeniedView";
        }
    }
}
