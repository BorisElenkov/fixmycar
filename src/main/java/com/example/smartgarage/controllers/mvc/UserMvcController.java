package com.example.smartgarage.controllers.mvc;

import com.example.smartgarage.exceptions.AuthorizationException;
import com.example.smartgarage.exceptions.EntityDuplicateException;
import com.example.smartgarage.exceptions.EntityNotFoundException;
import com.example.smartgarage.helpers.*;
import com.example.smartgarage.models.Car;
import com.example.smartgarage.models.User;
import com.example.smartgarage.models.UserFilterOptions;
import com.example.smartgarage.models.dtos.*;
import com.example.smartgarage.services.contracts.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

@Controller
@RequestMapping("/users")
public class UserMvcController {

    private final UserService userService;
    private final UserMapper userMapper;
    private final CarService carService;
    private final CarMapper carMapper;
    private final RoleService roleService;
    private final LevelService levelService;
    private final ModelService modelService;
    private final MakeService makeService;
    private final AuthenticationHelper authenticationHelper;
    private final UserFilterOptionsMapper userFilterOptionsMapper;
    private final MakeModelsMapper makeModelsMapper;
    private final EmailService emailService;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public UserMvcController(UserService userService, UserMapper userMapper, CarService carService,
                             CarMapper carMapper, RoleService roleService, LevelService levelService,
                             ModelService modelService, MakeService makeService,
                             AuthenticationHelper authenticationHelper,
                             UserFilterOptionsMapper userFilterOptionsMapper, MakeModelsMapper makeModelsMapper, EmailService emailService, PasswordEncoder passwordEncoder) {
        this.userService = userService;
        this.userMapper = userMapper;
        this.carService = carService;
        this.carMapper = carMapper;
        this.roleService = roleService;
        this.levelService = levelService;
        this.modelService = modelService;
        this.makeService = makeService;
        this.authenticationHelper = authenticationHelper;
        this.userFilterOptionsMapper = userFilterOptionsMapper;
        this.makeModelsMapper = makeModelsMapper;
        this.emailService = emailService;
        this.passwordEncoder = passwordEncoder;
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @ModelAttribute("currentUserId")
    public Integer populateCurrentUserId(HttpSession session) {
        return (Integer) session.getAttribute("currentUserId");
    }

    @ModelAttribute("isEmployee")
    public boolean populateIsEmployee(HttpSession session) {
        if (session.getAttribute("role") != null) {
            return session.getAttribute("role").equals("Employee");
        }
        return false;
    }

    @GetMapping
    public String showAllUsers(@ModelAttribute("filter") UserFilterDto userFilterDto,
                               HttpSession session, Model model) {
        User authorization;
        try {
            authorization = authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "redirect:/authentication/login";
        }
        try {
            UserFilterOptions filterOptions = userFilterOptionsMapper.dtoToObject(userFilterDto);
            model.addAttribute(userService.getUsers(filterOptions, authorization));
            model.addAttribute("filter", userFilterDto);
            model.addAttribute("roles", roleService.getRoles());
            return "UsersView";
        } catch (AuthorizationException e) {
            model.addAttribute("error", e.getMessage());
            return "AccessDeniedView";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
    }

    @GetMapping("/{userId}")
    public String showSingleUser(@PathVariable int userId, HttpSession session, Model model) {
        User authorization;
        try {
            authorization = authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "redirect:/authentication/login";
        }
        try {
            model.addAttribute("user", userService.getUserById(userId, authorization));
            model.addAttribute("userId", userId);
            model.addAttribute("car", carService.getCarsByUserId(userId));
            return "UserView";
        } catch (AuthorizationException e) {
            model.addAttribute("error", e.getMessage());
            return "AccessDeniedView";
        } catch (EntityNotFoundException e) {
            return "NotFoundView";
        }
    }

    @GetMapping("/new")
    public String showCreateUserPage(HttpSession session, Model model) {
        try {
            authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "redirect:/authentication/login";
        }
        model.addAttribute("user", new UserDtoIn());
        model.addAttribute("roles", roleService.getRoles());
        model.addAttribute("levels", levelService.getLevels());
        return "UserCreateView";
    }

    @PostMapping("/new")
    public String createUser(@Valid @ModelAttribute("user") UserDtoIn userDtoIn,
                             BindingResult bindingResult, HttpSession session, Model model) {
        try {
            authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "redirect:/authentication/login";
        }
        if (bindingResult.hasErrors()) {

            model.addAttribute("roles", roleService.getRoles());
            model.addAttribute("levels", levelService.getLevels());
            return "UserCreateView";
        }
        try {
            User authentication = authenticationHelper.tryGetCurrentUser(session);
            User user = userMapper.dtoToObject(userDtoIn);
            emailService.sendLoginInformation(user.getEmail(), user.getUsername(), user.getPassword()); // send login information to user's email
            String encryptedPassword = passwordEncoder.encode(user.getPassword());
            user.setPassword(encryptedPassword);
            userService.createUser(user, authentication);
            return "redirect:/users";
        } catch (AuthorizationException e) {
            model.addAttribute("error", e.getMessage());
            return "AccessDeniedView";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        } catch (EntityDuplicateException e) {
            model.addAttribute("error", e.getMessage());
            return "DuplicateExistView";
        }
    }

    @GetMapping("/{userId}/update")
    public String showEditUserPage(@PathVariable int userId, HttpSession session, Model model) {
        try {
            authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "redirect:/authentication/login";
        }
        try {
            UserDtoIn userDtoIn = userMapper.objectToDtoUpdate(userService.getUserById(userId));
            model.addAttribute("userId", userId);
            model.addAttribute("user", userDtoIn);
            model.addAttribute("roles", roleService.getRoles());
            model.addAttribute("levels", levelService.getLevels());
            return "UserUpdateView";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        } catch (AuthorizationException e) {
            model.addAttribute("error", e.getMessage());
            return "AccessDeniedView";
        }
    }

    @PostMapping("/{userId}/update")
    public String updateUser(@PathVariable int userId,
                             @Valid @ModelAttribute("user") UserDtoIn userDtoIn,
                             BindingResult bindingResult,
                             HttpSession session,
                             Model model) {
        try {
            authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "redirect:/authentication/login";
        }

        if (bindingResult.hasErrors()) {
            model.addAttribute("roles", roleService.getRoles());
            model.addAttribute("levels", levelService.getLevels());
            return "UserUpdateView";
        }
        try {
            User authentication = authenticationHelper.tryGetCurrentUser(session);
            User updatedUser = userService.getUserById(userId);
            User user = userMapper.dtoToObjectUpdate(updatedUser, userDtoIn);
            userService.updateUser(user, authentication);
            return "redirect:/users/{userId}";
        } catch (AuthorizationException e) {
            model.addAttribute("error", e.getMessage());
            return "AccessDeniedView";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        } catch (EntityDuplicateException e) {
            model.addAttribute("error", e.getMessage());
            return "DuplicateExistView";
        }
    }

    @GetMapping("/{userId}/delete")
    public String showDeleteUserPage(@PathVariable int userId, HttpSession session, Model model) {
        try {
            authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "redirect:/authentication/login";
        }
        try {
            model.addAttribute("userId", userId);
            return "UserDeleteView";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
    }

    @PostMapping("/{userId}/delete")
    public String deleteUser(@PathVariable int userId, HttpSession session, Model model) {
        try {
            authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "redirect:/authentication/login";
        }
        try {
            User authentication = authenticationHelper.tryGetCurrentUser(session);
            userService.deleteUser(userId, authentication);
            return "redirect:/users";
        } catch (AuthorizationException e) {
            model.addAttribute("error", e.getMessage());
            return "AccessDeniedView";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
    }

    @GetMapping("/{userId}/cars/{carId}")
    public String showSingleCar(@PathVariable int userId, @PathVariable int carId,
                                HttpSession session, Model model) {
        User authentication;
        try {
            authentication = authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "redirect:/authentication/login";
        }
        try {
            model.addAttribute("user", userService.getUserById(userId));
            model.addAttribute("userId", userId);
            model.addAttribute("carId", carId);
            model.addAttribute("car", carService.getCarById(carId, authentication));
            return "CarView";
        } catch (AuthorizationException e) {
            model.addAttribute("error", e.getMessage());
            return "AccessDeniedView";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
    }

    @GetMapping("/{userId}/cars/new")
    public String showCreateCarPage(@PathVariable int userId, HttpSession session, Model model) {
        try {
            authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "redirect:/authentication/login";
        }
        model.addAttribute("userId", userId);
        model.addAttribute("car", new CarDtoIn());
        model.addAttribute("makes", makeModelsMapper.getMakesDto());
        return "CarCreateView";
    }

    @PostMapping("/{userId}/cars/new")
    public String createCar(@Valid @ModelAttribute("car") CarDtoIn carDtoIn,
                            BindingResult bindingResult,
                            @PathVariable int userId,
                            HttpSession session,
                            Model model) {
        try {
            authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "redirect:/authentication/login";
        }

        if (bindingResult.hasErrors()) {
            model.addAttribute("user", userService.getUserById(userId));
            model.addAttribute("makes", makeModelsMapper.getMakesDto());
            return "CarCreateView";
        }
        try {
            User authentication = authenticationHelper.tryGetCurrentUser(session);
            Car car = carMapper.dtoToObjectCreate(carDtoIn, userId);
            carService.createCar(car, authentication);
            return "redirect:/users/{userId}";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        } catch (AuthorizationException e) {
            model.addAttribute("error", e.getMessage());
            return "AccessDeniedView";
        } catch (EntityDuplicateException e) {
            model.addAttribute("error", e.getMessage());
            return "DuplicateExistView";
        }
    }

    @GetMapping("/{userId}/cars/{carId}/update")
    public String showEditCarPage(@PathVariable int userId, @PathVariable int carId,
                                  HttpSession session, Model model) {
        try {
            authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "redirect:/authentication/login";
        }
        try {
            CarDtoIn carDtoIn = carMapper.carDtoInUpdate(carService.getCarById(carId));
            model.addAttribute("userId", userId);
            model.addAttribute("carId", carId);
            model.addAttribute("carToUpdate", carService.getCarById(carId));
            model.addAttribute("car", carDtoIn);
            model.addAttribute("makes", makeModelsMapper.getMakesDto());
            return "CarUpdateView";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
    }

    @PostMapping("/{userId}/cars/{carId}/update")
    public String updateCar(@PathVariable int userId, @PathVariable int carId,
                            @Valid @ModelAttribute("car") CarDtoIn carDtoIn,
                            BindingResult bindingResult,
                            HttpSession session,
                            Model model) {
        try {
            authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "redirect:/authentication/login";
        }
        if (bindingResult.hasErrors()) {
            model.addAttribute("makes", makeModelsMapper.getMakesDto());
            return "CarUpdateView";
        }
        try {
            User authentication = authenticationHelper.tryGetCurrentUser(session);
            Car carToUpdate = carService.getCarById(carId);
            Car car = carMapper.dtoToObjectUpdate(carDtoIn, userId, carToUpdate);
            carService.updateCar(car, authentication);
            return "redirect:/users/{userId}/cars/{carId}";
        } catch (AuthorizationException e) {
            model.addAttribute("error", e.getMessage());
            return "AccessDeniedView";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        } catch (EntityDuplicateException e) {
            model.addAttribute("error", e.getMessage());
            return "DuplicateExistView";
        }
    }

    @GetMapping("/{userId}/cars/{carId}/delete")
    public String showDeleteCarPage(@PathVariable int userId, @PathVariable int carId,
                                    HttpSession session, Model model) {
        try {
            authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "redirect:/authentication/login";
        }
        try {
            model.addAttribute("userId", userId);
            model.addAttribute("carId", carId);
            return "CarDeleteView";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
    }

    @PostMapping("/{userId}/cars/{carId}/delete")
    public String deleteCar(@PathVariable int userId, @PathVariable int carId,
                            HttpSession session, Model model) {
        try {
            authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "redirect:/authentication/login";
        }
        try {
            User authentication = authenticationHelper.tryGetCurrentUser(session);
            carService.deleteCar(carId, authentication);
            model.addAttribute("userId", userId);
            return "redirect:/users/{userId}";
        } catch (AuthorizationException e) {
            model.addAttribute("error", e.getMessage());
            return "AccessDeniedView";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
    }

    @GetMapping("/{userId}/password")
    public String showChangePasswordPage(@PathVariable int userId,
                                         HttpSession session, Model model) {
        try {
            authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "redirect:/authentication/login";
        }
        try {
            model.addAttribute("passwordDto", new PasswordDto());
            User authentication = authenticationHelper.tryGetCurrentUser(session);
            model.addAttribute("userId", userId);
            model.addAttribute("user", userService.getUserById(userId));
            model.addAttribute("authentication", authentication);
            return "PasswordChangeView";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        } catch (AuthorizationException e) {
            model.addAttribute("error", e.getMessage());
            return "AccessDeniedView";
        }
    }

    @PostMapping("/{userId}/password")
    public String updatePassword(@PathVariable int userId,
                                 @Valid PasswordDto passwordDto,
                                 BindingResult bindingResult,
                                 HttpSession session,
                                 Model model) {
        try {
            authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "redirect:/authentication/login";
        }

        if (!passwordDto.getPassword().equals(passwordDto.getConfirmPassword())) {
            bindingResult.rejectValue("confirmPassword", "not_same_password",
                    "Password confirm should match password!");
            return "PasswordChangeView";
        }
        if (bindingResult.hasErrors()) {
            return "PasswordChangeView";
        }
        try {
            User authentication = authenticationHelper.tryGetCurrentUser(session);
            User updatedUser = userService.getUserById(userId);
            userService.changePassword(userId, authentication, passwordDto.getPassword());
            model.addAttribute("message", "You have successfully changed your password.");
            return "SuccessView";
        } catch (AuthorizationException e) {
            model.addAttribute("error", e.getMessage());
            return "AccessDeniedView";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
    }
}
