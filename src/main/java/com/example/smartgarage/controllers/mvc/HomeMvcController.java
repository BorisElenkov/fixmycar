package com.example.smartgarage.controllers.mvc;

import com.example.smartgarage.helpers.AuthenticationHelper;
import com.example.smartgarage.services.contracts.CarService;
import com.example.smartgarage.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("/")
public class HomeMvcController {

    private final UserService userService;
    private final CarService carService;
    private final AuthenticationHelper authenticationHelper;


    @Autowired
    public HomeMvcController(UserService userService, CarService carService, AuthenticationHelper authenticationHelper) {
        this.userService = userService;
        this.carService = carService;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping
    public String showHomePage(Model model) {
        model.addAttribute("userCount", userService.getUsersCount());
        model.addAttribute("employeeCount", userService.getEmployeeCount());
        model.addAttribute("customerCount", userService.getCustomerCount());
        model.addAttribute("carCount", carService.getCarCount());
        return "index";
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @ModelAttribute("currentUserId")
    public Integer populateCurrentUserId(HttpSession session) {
        return (Integer) session.getAttribute("currentUserId");
    }

    @ModelAttribute("isEmployee")
    public boolean populateIsEmployee(HttpSession session) {
        if (session.getAttribute("role") != null) {
            return session.getAttribute("role").equals("Employee");
        }
        return false;
    }

    @GetMapping("/about")
    public String handleAbout() {
        return "AboutView";
    }
}
