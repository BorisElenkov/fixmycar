package com.example.smartgarage.controllers.mvc;

import com.example.smartgarage.exceptions.AuthorizationException;
import com.example.smartgarage.exceptions.CustomException;
import com.example.smartgarage.exceptions.EntityDuplicateException;
import com.example.smartgarage.exceptions.EntityNotFoundException;
import com.example.smartgarage.helpers.AuthenticationHelper;
import com.example.smartgarage.helpers.RepairMapper;
import com.example.smartgarage.models.Repair;
import com.example.smartgarage.models.RepairFilterOptions;
import com.example.smartgarage.models.User;
import com.example.smartgarage.models.dtos.RepairDtoIn;
import com.example.smartgarage.models.dtos.RepairFilterDto;
import com.example.smartgarage.services.contracts.RepairService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/repairs")
public class RepairMvcController {

    private final AuthenticationHelper authenticationHelper;
    private final RepairService repairService;
    private final RepairMapper repairMapper;

    @Autowired
    public RepairMvcController(AuthenticationHelper authenticationHelper, RepairService repairService,
                               RepairMapper repairMapper) {
        this.authenticationHelper = authenticationHelper;
        this.repairService = repairService;
        this.repairMapper = repairMapper;
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @ModelAttribute("currentUserId")
    public Integer populateCurrentUserId(HttpSession session) {
        return (Integer) session.getAttribute("currentUserId");
    }

    @ModelAttribute("isEmployee")
    public boolean populateIsEmployee(HttpSession session) {
        if (session.getAttribute("role") != null) {
            return session.getAttribute("role").equals("Employee");
        }
        return false;
    }

    @GetMapping
    public String getRepairs(@ModelAttribute("repairFilterDto") RepairFilterDto filterDto,
                             HttpSession session, Model model) {
        User user;
        try {
            user = authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            RepairFilterOptions filterOptions = new RepairFilterOptions(null, null,
                    null, null, null, null);
            model.addAttribute("repairs", repairService.getRepairs(filterOptions));
            return "RepairsInfoView";
        }
        try {
            RepairFilterOptions filterOptions = new RepairFilterOptions(
                    filterDto.getName(),
                    filterDto.getMinPrice(),
                    filterDto.getMaxPrice(),
                    filterDto.getActive(),
                    filterDto.getSortBy(),
                    filterDto.getSortOrder()
            );
            List<Repair> repairs = repairService.getRepairs(filterOptions);
            model.addAttribute("user", user);
            model.addAttribute("repairs", repairs);
            model.addAttribute("repairFilterDto", filterDto);
            return "RepairsView";
        } catch (AuthorizationException e) {
            model.addAttribute("error", e.getMessage());
            return "AccessDeniedView";
        }
    }

    @GetMapping("/new")
    public String showNewRepairPage(HttpSession session, Model model) {
        try {
            authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "redirect:/authentication/login";
        }
        model.addAttribute("repair", new RepairDtoIn());
        return "RepairCreateView";
    }

    @PostMapping("/new")
    public String createRepair(@Valid @ModelAttribute("repair") RepairDtoIn repairDto,
                               BindingResult bindingResult, HttpSession session, Model model) {
        User user;
        try {
            user = authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "redirect:/authentication/login";
        }
        if (bindingResult.hasErrors()) {
            return "RepairCreateView";
        }

        try {
            Repair repair = repairMapper.dtoToObject(repairDto);
            repairService.createRepair(repair, user);
            return "redirect:/repairs/";
        } catch (AuthorizationException e) {
            model.addAttribute("error", e.getMessage());
            return "AccessDeniedView";
        } catch (EntityDuplicateException e) {
            bindingResult.rejectValue("name", "duplicate_name", e.getMessage());
            return "RepairCreateView";
        }
    }

    @GetMapping("/{repairId}/update")
    public String showUpdateRepairPage(@PathVariable int repairId, HttpSession session, Model model) {
        try {
            authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "redirect:/authentication/login";
        }
        try {
            Repair repair = repairService.getRepairById(repairId);
            RepairDtoIn repairDtoIn = repairMapper.objectToDto(repair);
            model.addAttribute("repairId", repairId);
            model.addAttribute("repair", repairDtoIn);
            return "RepairUpdateView";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
    }

    @PostMapping("{repairId}/update")
    public String updateRepair(@PathVariable int repairId,
                               @Valid @ModelAttribute("repair") RepairDtoIn repairDtoIn,
                               BindingResult bindingResult,
                               HttpSession session,
                               Model model) {
        User user;
        try {
            user = authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "redirect:/authentication/login";
        }
        if (bindingResult.hasErrors()) {
            return "RepairUpdateView";
        }
        try {
            Repair existingRepair = repairService.getRepairById(repairId);
            Repair updatedRepair = repairMapper.dtoToObject(existingRepair, repairDtoIn);
            repairService.updateRepair(updatedRepair, user);
            return "redirect:/repairs/";
        } catch (AuthorizationException e) {
            model.addAttribute("error", e.getMessage());
            return "AccessDeniedView";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        } catch (EntityDuplicateException e) {
            bindingResult.rejectValue("name", "duplicate_name", e.getMessage());
            return "RepairUpdateView";
        } catch (CustomException e) {
            bindingResult.rejectValue("name", "inactive_repair", e.getMessage());
            return "redirect:/repairs/" + repairId;
        }
    }


    @GetMapping("/{repairId}/deactivate")
    public String showDeactivateRepairPage(@PathVariable int repairId, HttpSession session, Model model) {
        try {
            authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "redirect:/authentication/login";
        }
        try {
            model.addAttribute("repairId", repairId);
            return "RepairDeactivateView";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
    }

    @PostMapping("/{repairId}/deactivate")
    public String deactivateRepair(@PathVariable int repairId, HttpSession session, Model model) {
        User user;
        try {
            user = authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "redirect:/authentication/login";
        }
        try {
            repairService.deactivateRepair(repairId, user);
            return "redirect:/repairs";
        } catch (AuthorizationException e) {
            model.addAttribute("error", e.getMessage());
            return "AccessDeniedView";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
    }

    @GetMapping("/{repairId}/activate")
    public String showActivateRepairPage(@PathVariable int repairId, HttpSession session, Model model) {
        try {
            authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "redirect:/authentication/login";
        }
        try {
            model.addAttribute("repairId", repairId);
            return "RepairActivateView";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
    }

    @PostMapping("/{repairId}/activate")
    public String activateRepair(@PathVariable int repairId, HttpSession session, Model model) {
        User user;
        try {
            user = authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "redirect:/authentication/login";
        }
        try {
            repairService.activateRepair(repairId, user);
            return "redirect:/repairs";
        } catch (AuthorizationException e) {
            model.addAttribute("error", e.getMessage());
            return "AccessDeniedView";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
    }
}
