package com.example.smartgarage.controllers.mvc;

import com.example.smartgarage.exceptions.AuthorizationException;
import com.example.smartgarage.exceptions.EntityDuplicateException;
import com.example.smartgarage.exceptions.EntityNotFoundException;
import com.example.smartgarage.helpers.AuthenticationHelper;
import com.example.smartgarage.helpers.MakeMapper;
import com.example.smartgarage.helpers.ModelMapper;
import com.example.smartgarage.models.CarModel;
import com.example.smartgarage.models.Make;
import com.example.smartgarage.models.User;
import com.example.smartgarage.models.dtos.MakeDtoIn;
import com.example.smartgarage.models.dtos.ModelDtoIn;
import com.example.smartgarage.services.contracts.MakeService;
import com.example.smartgarage.services.contracts.ModelService;
import com.example.smartgarage.services.contracts.RoleService;
import com.example.smartgarage.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

@Controller
@RequestMapping("/attributes")
public class AttributeMvcController {
    private final RoleService roleService;
    private final UserService userService;
    private final MakeService makeService;
    private final MakeMapper makeMapper;
    private final ModelService modelService;
    private final ModelMapper modelMapper;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public AttributeMvcController(RoleService roleService, UserService userService,
                                  MakeService makeService,
                                  MakeMapper makeMapper, ModelService modelService,
                                  ModelMapper modelMapper, AuthenticationHelper authenticationHelper) {
        this.roleService = roleService;
        this.userService = userService;
        this.makeService = makeService;
        this.makeMapper = makeMapper;
        this.modelService = modelService;
        this.modelMapper = modelMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @ModelAttribute("currentUserId")
    public Integer populateCurrentUserId(HttpSession session) {
        return (Integer) session.getAttribute("currentUserId");
    }

    @ModelAttribute("isEmployee")
    public boolean populateIsEmployee(HttpSession session) {
        if (session.getAttribute("role") != null) {
            return session.getAttribute("role").equals("Employee");
        }
        return false;
    }

    @GetMapping
    public String showAllRoles(HttpSession session, Model model) {
        try {
            authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "redirect:/authentication/login";
        }
        try {
            model.addAttribute("roles", roleService.getRoles());
            model.addAttribute("makes", makeService.getMakes());
            model.addAttribute("models", modelService.getModels());
            return "AttributesView";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
    }

    @GetMapping("/models/new")
    public String showCreateModelPage(HttpSession session, Model model) {
        try {
            authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "redirect:/authentication/login";
        }
        model.addAttribute("model", new ModelDtoIn());
        model.addAttribute("makes", makeService.getMakes());
        return "ModelCreateView";
    }

    @PostMapping("/models/new")
    public String createModel(@Valid @ModelAttribute("model") ModelDtoIn modelDtoIn,
                              BindingResult bindingResult, HttpSession session, Model model) {
        try {
            authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "redirect:/authentication/login";
        }
        if (bindingResult.hasErrors()) {
            model.addAttribute("model", new ModelDtoIn());
            model.addAttribute("makes", makeService.getMakes());
            return "ModelCreateView";
        }
        try {
            CarModel modelToCreate = modelMapper.dtoToObject(modelDtoIn);
            User authentication = authenticationHelper.tryGetCurrentUser(session);
            modelService.createModel(modelToCreate, authentication);
            return "redirect:/attributes";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        } catch (EntityDuplicateException e) {
            model.addAttribute("error", e.getMessage());
            return "DuplicateExistView";
        }
    }

    @GetMapping("/models/{modelId}/update")
    public String showEditModelPage(@PathVariable int modelId, HttpSession session, Model model) {
        try {
            authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "redirect:/authentication/login";
        }
        try {
            ModelDtoIn modelDtoIn = modelMapper.objectToDtoUpdate(modelService.getModelById(modelId));
            model.addAttribute("modelId", modelId);
            model.addAttribute("model", modelDtoIn);
            model.addAttribute("makes", makeService.getMakes());
            return "ModelUpdateView";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
    }


    @PostMapping("/models/{modelId}/update")
    public String updateModel(@PathVariable int modelId,
                              @Valid @ModelAttribute("model") ModelDtoIn modelDtoIn,
                              BindingResult bindingResult,
                              HttpSession session,
                              Model model) {
        try {
            authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "redirect:/authentication/login";
        }
        if (bindingResult.hasErrors()) {
            model.addAttribute("makes", makeService.getMakes());
            return "ModelUpdateView";
        }
        try {
            User authentication = authenticationHelper.tryGetCurrentUser(session);
            CarModel carModel = modelMapper.dtoToObjectUpdate(modelId, modelDtoIn);
            modelService.updateModel(authentication, carModel);
            return "redirect:/attributes";
        } catch (AuthorizationException e) {
            model.addAttribute("error", e.getMessage());
            return "AccessDeniedView";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        } catch (EntityDuplicateException e) {
            model.addAttribute("error", e.getMessage());
            return "DuplicateExistView";
        }
    }

    @GetMapping("/models/{modelId}/delete")
    public String showDeleteModelPage(@PathVariable int modelId, HttpSession session, Model model) {
        try {
            authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "redirect:/authentication/login";
        }
        try {
            model.addAttribute("modelId", modelId);
            return "ModelDeleteView";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
    }

    @PostMapping("/models/{modelId}/delete")
    public String deleteModel(@PathVariable int modelId, HttpSession session, Model model) {
        try {
            authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "redirect:/authentication/login";
        }
        try {
            User authentication = authenticationHelper.tryGetCurrentUser(session);
            modelService.deleteModel(modelId, authentication);
            model.addAttribute("modelId", modelId);
            return "redirect:/attributes";
        } catch (AuthorizationException | IllegalArgumentException e) {
            model.addAttribute("error", e.getMessage());
            return "AccessDeniedView";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
    }

    @GetMapping("/makes/new")
    public String showCreateMakePage(HttpSession session, Model model) {
        try {
            authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "redirect:/authentication/login";
        }
        model.addAttribute("make", new MakeDtoIn());
        return "MakeCreateView";
    }

    @PostMapping("/makes/new")
    public String createMake(@Valid @ModelAttribute("make") MakeDtoIn makeDtoIn,
                             BindingResult bindingResult, HttpSession session, Model model) {
        try {
            authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "redirect:/authentication/login";
        }
        if (bindingResult.hasErrors()) {
            model.addAttribute("make", new MakeDtoIn());
            return "MakeCreateView";
        }
        try {
            Make makeToCreate = makeMapper.dtoToObject(makeDtoIn);
            User authentication = authenticationHelper.tryGetCurrentUser(session);
            makeService.createMake(makeToCreate, authentication);
            return "redirect:/attributes";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        } catch (EntityDuplicateException e) {
            model.addAttribute("error", e.getMessage());
            return "DuplicateExistView";
        }
    }

    @GetMapping("/makes/{makeId}/update")
    public String showEditMakePage(@PathVariable int makeId, HttpSession session, Model model) {
        try {
            authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "redirect:/authentication/login";
        }
        try {
            model.addAttribute("makeId", makeId);
            model.addAttribute("make", makeService.getMakeById(makeId));
            return "MakeUpdateView";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
    }

    @PostMapping("/makes/{makeId}/update")
    public String updateMake(@PathVariable int makeId,
                             @Valid @ModelAttribute("make") MakeDtoIn makeDtoIn,
                             BindingResult bindingResult,
                             HttpSession session,
                             Model model) {
        try {
            authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "redirect:/authentication/login";
        }
        if (bindingResult.hasErrors()) {
            return "MakeUpdateView";
        }
        try {
            User authentication = authenticationHelper.tryGetCurrentUser(session);
            Make make = makeMapper.dtoToObject(makeId, makeDtoIn);
            makeService.updateMake(make, authentication);
            return "redirect:/attributes";
        } catch (AuthorizationException e) {
            model.addAttribute("error", e.getMessage());
            return "AccessDeniedView";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        } catch (EntityDuplicateException e) {
            model.addAttribute("error", e.getMessage());
            return "DuplicateExistView";
        }
    }

    @GetMapping("/makes/{makeId}/delete")
    public String showDeleteMakePage(@PathVariable int makeId, HttpSession session, Model model) {
        try {
            authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "redirect:/authentication/login";
        }
        try {
            model.addAttribute("makeId", makeId);
            return "MakeDeleteView";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
    }

    @PostMapping("/makes/{makeId}/delete")
    public String deleteMake(@PathVariable int makeId, HttpSession session, Model model) {
        try {
            authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "redirect:/authentication/login";
        }
        try {
            User authentication = authenticationHelper.tryGetCurrentUser(session);
            makeService.deleteMake(makeId, authentication);
            return "redirect:/attributes";
        } catch (AuthorizationException e) {
            model.addAttribute("error", e.getMessage());
            return "AccessDeniedView";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
    }
}
