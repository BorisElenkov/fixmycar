package com.example.smartgarage.controllers.mvc;

import com.example.smartgarage.exceptions.AuthorizationException;
import com.example.smartgarage.helpers.AuthenticationHelper;
import com.example.smartgarage.helpers.VisitsHistoryFilterMapper;
import com.example.smartgarage.helpers.VisitsHistoryMapper;
import com.example.smartgarage.models.*;
import com.example.smartgarage.models.dtos.VisitsHistoryDtoOut;
import com.example.smartgarage.models.dtos.VisitsHistoryFilterDto;
import com.example.smartgarage.services.contracts.VisitsHistoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/visits-history")
public class VisitsHistoryMvcController {
    private final AuthenticationHelper authenticationHelper;
    private final VisitsHistoryService visitsHistoryService;
    private final VisitsHistoryFilterMapper visitsHistoryFilterMapper;
    private final VisitsHistoryMapper visitsHistoryMapper;

    @Autowired
    public VisitsHistoryMvcController(AuthenticationHelper authenticationHelper, VisitsHistoryService visitsHistoryService, VisitsHistoryFilterMapper visitsHistoryFilterMapper, VisitsHistoryMapper visitsHistoryMapper) {
        this.authenticationHelper = authenticationHelper;
        this.visitsHistoryService = visitsHistoryService;
        this.visitsHistoryFilterMapper = visitsHistoryFilterMapper;
        this.visitsHistoryMapper = visitsHistoryMapper;
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @ModelAttribute("currentUserId")
    public Integer populateCurrentUserId(HttpSession session) {
        return (Integer) session.getAttribute("currentUserId");
    }

    @ModelAttribute("isEmployee")
    public boolean populateIsEmployee(HttpSession session) {
        if (session.getAttribute("role") != null) {
            return session.getAttribute("role").equals("Employee");
        }
        return false;
    }

    @GetMapping
    public String getVisitsHistory(@ModelAttribute("filter") VisitsHistoryFilterDto filterDto,
                                   HttpSession session, Model model) {
        try {
            VisitsHistoryFilterOptions filterOptions = visitsHistoryFilterMapper.dtoToObject(filterDto);
            User user = authenticationHelper.tryGetCurrentUser(session);
            List<VisitsHistory> history = visitsHistoryService.getVisitsHistory(filterOptions, user, Optional.empty());
            model.addAttribute("user", user);
            List<VisitsHistoryDtoOut> result = visitsHistoryMapper.getDtoOutListFromVisitsHistory(history);
            model.addAttribute("history", result);
            model.addAttribute("filter", filterDto);
            return "VisitsHistoryView";
        } catch (AuthorizationException e) {
            return "redirect:/authentication/login";
        }
    }
}
