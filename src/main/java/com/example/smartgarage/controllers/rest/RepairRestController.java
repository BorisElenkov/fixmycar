package com.example.smartgarage.controllers.rest;

import com.example.smartgarage.exceptions.AuthorizationException;
import com.example.smartgarage.exceptions.CustomException;
import com.example.smartgarage.exceptions.EntityDuplicateException;
import com.example.smartgarage.exceptions.EntityNotFoundException;
import com.example.smartgarage.helpers.AuthenticationHelper;
import com.example.smartgarage.helpers.RepairMapper;
import com.example.smartgarage.models.Repair;
import com.example.smartgarage.models.RepairFilterOptions;
import com.example.smartgarage.models.User;
import com.example.smartgarage.models.dtos.RepairDtoIn;
import com.example.smartgarage.services.contracts.RepairService;
import com.example.smartgarage.services.contracts.VisitService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/repairs")
public class RepairRestController {
    private final AuthenticationHelper authenticationHelper;
    private final RepairService repairService;
    private final RepairMapper repairMapper;
    private final VisitService visitService;

    @Autowired
    public RepairRestController(AuthenticationHelper authenticationHelper, RepairService repairService,
                                RepairMapper repairMapper, VisitService visitService) {
        this.authenticationHelper = authenticationHelper;
        this.repairService = repairService;
        this.repairMapper = repairMapper;
        this.visitService = visitService;
    }

    @ApiResponse(responseCode = "200", description = "Successful operation")
    @ApiResponse(responseCode = "401", description = "Unauthorized operation")
    @Operation(summary = "Get all repairs with filter")
    @GetMapping
    public List<Repair> getRepairs(@RequestHeader HttpHeaders headers,
                                   @RequestParam(required = false) String name,
                                   @RequestParam(required = false) Double minPrice,
                                   @RequestParam(required = false) Double maxPrice,
                                   @RequestParam(required = false) Boolean isActive,
                                   @RequestParam(required = false) String sortBy,
                                   @RequestParam(required = false) String sortOrder) {
        try {
            RepairFilterOptions repairFilterOptions = new RepairFilterOptions
                    (name, minPrice, maxPrice, isActive, sortBy, sortOrder);
            List<Repair> repairs = repairService.getRepairs(repairFilterOptions);
            return repairs;
        } catch (AuthorizationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @ApiResponse(responseCode = "200", description = "Successful operation")
    @ApiResponse(responseCode = "401", description = "Unauthorized operation")
    @ApiResponse(responseCode = "404", description = "Resource not found")
    @Operation(summary = "Get one repair")
    @GetMapping("/{repairId}")
    public Repair getRepairById(@RequestHeader HttpHeaders headers,
                                @PathVariable int repairId) {
        try {
            authenticationHelper.tryGetUser(headers);
            return repairService.getRepairById(repairId);
        } catch (AuthorizationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @ApiResponse(responseCode = "200", description = "Successful operation")
    @ApiResponse(responseCode = "401", description = "Unauthorized operation")
    @ApiResponse(responseCode = "409", description = "Duplicate entry")
    @Operation(summary = "Create repair")
    @PostMapping
    public Repair createRepair(@RequestHeader HttpHeaders headers,
                               @Valid @RequestBody RepairDtoIn repairDtoIn) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            Repair repair = repairMapper.dtoToObject(repairDtoIn);
            repairService.createRepair(repair, user);
            return repair;
        } catch (AuthorizationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (EntityDuplicateException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @ApiResponse(responseCode = "200", description = "Successful operation")
    @ApiResponse(responseCode = "401", description = "Unauthorized operation")
    @ApiResponse(responseCode = "404", description = "Resource not found")
    @ApiResponse(responseCode = "409", description = "Duplicate entry")
    @Operation(summary = "Update repair")
    @PutMapping("/{repairId}")
    public Repair updateRepair(@RequestHeader HttpHeaders headers,
                               @PathVariable int repairId,
                               @Valid @RequestBody RepairDtoIn repairDtoIn) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            Repair existingRepair = repairService.getRepairById(repairId);
            Repair repair = repairMapper.dtoToObject(existingRepair, repairDtoIn);
            repairService.updateRepair(repair, user);
            return repair;
        } catch (AuthorizationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (EntityDuplicateException | CustomException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @ApiResponse(responseCode = "200", description = "Successful operation")
    @ApiResponse(responseCode = "401", description = "Unauthorized operation")
    @ApiResponse(responseCode = "404", description = "Resource not found")
    @Operation(summary = "Delete repair")
    @DeleteMapping("/{repairId}")
    public void deleteRepair(@RequestHeader HttpHeaders headers,
                             @PathVariable int repairId) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            repairService.deactivateRepair(repairId, user);
        } catch (AuthorizationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @ApiResponse(responseCode = "200", description = "Successful operation")
    @ApiResponse(responseCode = "401", description = "Unauthorized operation")
    @ApiResponse(responseCode = "404", description = "Resource not found")
    @Operation(summary = "Activate repair")
    @PutMapping("/{repairId}/activate")
    public void activateRepair(@RequestHeader HttpHeaders headers,
                               @PathVariable int repairId) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            repairService.activateRepair(repairId, user);
        } catch (AuthorizationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}
