package com.example.smartgarage.controllers.rest;

import com.example.smartgarage.exceptions.AuthorizationException;
import com.example.smartgarage.exceptions.EntityDuplicateException;
import com.example.smartgarage.exceptions.EntityNotFoundException;
import com.example.smartgarage.helpers.AuthenticationHelper;
import com.example.smartgarage.helpers.MakeMapper;
import com.example.smartgarage.models.Make;
import com.example.smartgarage.models.User;
import com.example.smartgarage.models.dtos.MakeDtoIn;
import com.example.smartgarage.services.contracts.MakeService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/makes")
public class MakeRestController {
    private final AuthenticationHelper authenticationHelper;
    private final MakeService makeService;
    private final MakeMapper makeMapper;

    @Autowired
    public MakeRestController(AuthenticationHelper authenticationHelper, MakeService makeService, MakeMapper makeMapper) {
        this.authenticationHelper = authenticationHelper;
        this.makeService = makeService;
        this.makeMapper = makeMapper;
    }

    @ApiResponse(responseCode = "200", description = "Successful operation")
    @ApiResponse(responseCode = "401", description = "Unauthorized operation")
    @Operation(summary = "Get all makes")
    @GetMapping
    public List<Make> getMakes(@RequestHeader HttpHeaders headers) {
        try {
            authenticationHelper.tryGetUser(headers);
        } catch (AuthorizationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
        return makeService.getMakes();
    }

    @ApiResponse(responseCode = "200", description = "Successful operation")
    @ApiResponse(responseCode = "401", description = "Unauthorized operation")
    @ApiResponse(responseCode = "404", description = "Resource not found")
    @Operation(summary = "Get one make")
    @GetMapping("/{makeId}")
    public Make getMakeById(@RequestHeader HttpHeaders headers, @PathVariable int makeId) {
        try {
            authenticationHelper.tryGetUser(headers);
            return makeService.getMakeById(makeId);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (AuthorizationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @ApiResponse(responseCode = "200", description = "Successful operation")
    @ApiResponse(responseCode = "401", description = "Unauthorized operation")
    @ApiResponse(responseCode = "409", description = "Duplicate entry")
    @Operation(summary = "Create make")
    @PostMapping
    public Make createMake(@RequestHeader HttpHeaders headers, @Valid @RequestBody MakeDtoIn makeDtoIn) {
        try {
            User authentication = authenticationHelper.tryGetUser(headers);
            Make make = makeMapper.dtoToObject(makeDtoIn);
            makeService.createMake(make, authentication);
            return make;
        } catch (EntityDuplicateException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (AuthorizationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @ApiResponse(responseCode = "200", description = "Successful operation")
    @ApiResponse(responseCode = "401", description = "Unauthorized operation")
    @ApiResponse(responseCode = "404", description = "Resource not found")
    @ApiResponse(responseCode = "409", description = "Duplicate entry")
    @Operation(summary = "Update make")
    @PutMapping("/{makeId}")
    public Make updateMake(@RequestHeader HttpHeaders headers,
                           @PathVariable int makeId,
                           @Valid @RequestBody MakeDtoIn makeDtoIn) {
        try {
            User authentication = authenticationHelper.tryGetUser(headers);
            Make make = makeMapper.dtoToObject(makeId, makeDtoIn);
            makeService.updateMake(make, authentication);
            return make;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (EntityDuplicateException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (AuthorizationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @ApiResponse(responseCode = "200", description = "Successful operation")
    @ApiResponse(responseCode = "401", description = "Unauthorized operation")
    @ApiResponse(responseCode = "404", description = "Resource not found")
    @Operation(summary = "Delete make")
    @DeleteMapping("/{makeId}")
    public void deleteMake(@RequestHeader HttpHeaders headers, @PathVariable int makeId) {
        try {
            User authentication = authenticationHelper.tryGetUser(headers);
            makeService.deleteMake(makeId, authentication);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (AuthorizationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }
}
