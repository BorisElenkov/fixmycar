package com.example.smartgarage.controllers.rest;

import com.example.smartgarage.exceptions.AuthorizationException;
import com.example.smartgarage.exceptions.EntityDuplicateException;
import com.example.smartgarage.exceptions.EntityNotFoundException;
import com.example.smartgarage.helpers.AuthenticationHelper;
import com.example.smartgarage.helpers.ModelMapper;
import com.example.smartgarage.models.CarModel;
import com.example.smartgarage.models.User;
import com.example.smartgarage.models.dtos.ModelDtoIn;
import com.example.smartgarage.services.contracts.MakeService;
import com.example.smartgarage.services.contracts.ModelService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/makes")
public class ModelRestController {
    private final AuthenticationHelper authenticationHelper;
    private final ModelService modelService;
    private final MakeService makeService;
    private final ModelMapper modelMapper;

    public ModelRestController(AuthenticationHelper authenticationHelper, ModelService modelService,
                               MakeService makeService, ModelMapper modelMapper) {
        this.authenticationHelper = authenticationHelper;
        this.modelService = modelService;
        this.makeService = makeService;
        this.modelMapper = modelMapper;
    }

    @ApiResponse(responseCode = "200", description = "Successful operation")
    @ApiResponse(responseCode = "401", description = "Unauthorized operation")
    @Operation(summary = "Get all car models")
    @GetMapping("/{makeId}/models")
    public List<CarModel> getAllModelsForMake(@RequestHeader HttpHeaders headers, @PathVariable int makeId) {
        try {
            authenticationHelper.tryGetUser(headers);
        } catch (AuthorizationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
        return modelService.getAllModelsByMake(makeService.getMakeById(makeId).getMakeName());
    }

    @ApiResponse(responseCode = "200", description = "Successful operation")
    @ApiResponse(responseCode = "401", description = "Unauthorized operation")
    @ApiResponse(responseCode = "404", description = "Resource not found")
    @Operation(summary = "Get one car model")
    @GetMapping("/{makeId}/models/{modelId}")
    public CarModel getModelById(@RequestHeader HttpHeaders headers, @PathVariable int makeId, @PathVariable int modelId) {
        try {
            authenticationHelper.tryGetUser(headers);
            return modelService.getModelById(makeId, modelId);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (AuthorizationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @ApiResponse(responseCode = "200", description = "Successful operation")
    @ApiResponse(responseCode = "401", description = "Unauthorized operation")
    @ApiResponse(responseCode = "404", description = "Resource not found")
    @ApiResponse(responseCode = "409", description = "Duplicate entry")
    @Operation(summary = "Create car model")
    @PostMapping("/{makeId}/models")
    public CarModel createModel(@RequestHeader HttpHeaders headers, @PathVariable int makeId,
                                @Valid @RequestBody ModelDtoIn modelDtoIn) {
        try {
            User authentication = authenticationHelper.tryGetUser(headers);
            CarModel carModel = modelMapper.dtoToObject(makeId, modelDtoIn);
            modelService.createModel(carModel, authentication);
            return carModel;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (EntityDuplicateException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (AuthorizationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @ApiResponse(responseCode = "200", description = "Successful operation")
    @ApiResponse(responseCode = "401", description = "Unauthorized operation")
    @ApiResponse(responseCode = "404", description = "Resource not found")
    @ApiResponse(responseCode = "409", description = "Duplicate entry")
    @Operation(summary = "Update car model")
    @PutMapping("/{makeId}/models/{modelId}")
    public CarModel updateModel(@RequestHeader HttpHeaders headers,
                                @PathVariable int makeId,
                                @PathVariable int modelId,
                                @Valid @RequestBody ModelDtoIn modelDtoIn) {
        try {
            User authentication = authenticationHelper.tryGetUser(headers);
            CarModel carModel = modelMapper.dtoToObject(makeId, modelId, modelDtoIn);
            modelService.updateModel(authentication, carModel);
            return carModel;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (EntityDuplicateException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (AuthorizationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @ApiResponse(responseCode = "200", description = "Successful operation")
    @ApiResponse(responseCode = "401", description = "Unauthorized operation")
    @ApiResponse(responseCode = "404", description = "Resource not found")
    @Operation(summary = "Delete car model")
    @DeleteMapping("/{makeId}/models/{modelId}")
    public void deleteModel(@RequestHeader HttpHeaders headers, @PathVariable int makeId, @PathVariable int modelId) {
        try {
            User authentication = authenticationHelper.tryGetUser(headers);
            modelService.deleteModel(modelId, authentication);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (AuthorizationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }
}
