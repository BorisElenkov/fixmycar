package com.example.smartgarage.controllers.rest;

import com.example.smartgarage.exceptions.AuthorizationException;
import com.example.smartgarage.exceptions.EntityDuplicateException;
import com.example.smartgarage.exceptions.EntityNotFoundException;
import com.example.smartgarage.helpers.AuthenticationHelper;
import com.example.smartgarage.helpers.CarMapper;
import com.example.smartgarage.helpers.VisitMapper;
import com.example.smartgarage.helpers.VisitsHistoryMapper;
import com.example.smartgarage.models.*;
import com.example.smartgarage.models.dtos.CarDtoIn;
import com.example.smartgarage.models.dtos.VisitDtoOut;
import com.example.smartgarage.models.dtos.VisitsHistoryDtoOut;
import com.example.smartgarage.services.contracts.CarService;
import com.example.smartgarage.services.contracts.VisitService;
import com.example.smartgarage.services.contracts.VisitsHistoryService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/cars")
public class CarRestController {
    private final AuthenticationHelper authenticationHelper;
    private final CarService carService;
    private final CarMapper carMapper;
    private final VisitService visitService;
    private final VisitMapper visitMapper;
    private final VisitsHistoryService visitsHistoryService;
    private final VisitsHistoryMapper visitsHistoryMapper;

    @Autowired
    public CarRestController(AuthenticationHelper authenticationHelper, CarService carService,
                             CarMapper carMapper, VisitService visitService,
                             VisitMapper visitMapper, VisitsHistoryService visitsHistoryService, VisitsHistoryMapper visitsHistoryMapper) {
        this.authenticationHelper = authenticationHelper;
        this.carService = carService;
        this.carMapper = carMapper;
        this.visitService = visitService;
        this.visitMapper = visitMapper;
        this.visitsHistoryService = visitsHistoryService;
        this.visitsHistoryMapper = visitsHistoryMapper;
    }

    @ApiResponse(responseCode = "200", description = "Successful operation")
    @ApiResponse(responseCode = "401", description = "Unauthorized operation")
    @Operation(summary = "Get all cars with filter")
    @GetMapping
    public List<Car> getCars(@RequestHeader HttpHeaders headers,
                             @RequestParam(required = false) String licensePlate,
                             @RequestParam(required = false) String vin,
                             @RequestParam(required = false) Integer year,
                             @RequestParam(required = false) String username,
                             @RequestParam(required = false) String modelName,
                             @RequestParam(required = false) String makeName,
                             @RequestParam(required = false) String phone,
                             @RequestParam(required = false) String sortBy,
                             @RequestParam(required = false) String sortOrder) {
        try {
            User authentication = authenticationHelper.tryGetUser(headers);
            CarFilterOptions carFilterOptions =
                    new CarFilterOptions(licensePlate, vin, year, username, modelName, makeName, phone, sortBy, sortOrder);
            List<Car> carList = carService.getCars(carFilterOptions, authentication);
            return carList;
        } catch (AuthorizationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @ApiResponse(responseCode = "200", description = "Successful operation")
    @ApiResponse(responseCode = "404", description = "Resource not found")
    @Operation(summary = "Get one car")
    @GetMapping("/{carId}")
    public Car getCarById(@RequestHeader HttpHeaders headers, @PathVariable int carId) {
        try {
            User authentication = authenticationHelper.tryGetUser(headers);
            return carService.getCarById(carId, authentication);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @ApiResponse(responseCode = "200", description = "Successful operation")
    @ApiResponse(responseCode = "401", description = "Unauthorized operation")
    @ApiResponse(responseCode = "404", description = "Resource not found")
    @ApiResponse(responseCode = "409", description = "Duplicate entry")
    @Operation(summary = "Create car")
    @PostMapping
    public Car createCar(@RequestHeader HttpHeaders headers, @Valid @RequestBody CarDtoIn carDtoIn) {
        try {
            User authentication = authenticationHelper.tryGetUser(headers);
            Car car = carMapper.dtoToObject(carDtoIn);
            carService.createCar(car, authentication);
            return car;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (EntityDuplicateException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (AuthorizationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @ApiResponse(responseCode = "200", description = "Successful operation")
    @ApiResponse(responseCode = "401", description = "Unauthorized operation")
    @ApiResponse(responseCode = "404", description = "Resource not found")
    @ApiResponse(responseCode = "409", description = "Duplicate entry")
    @Operation(summary = "Update car")
    @PutMapping("/{carId}")
    public Car updateCar(@RequestHeader HttpHeaders headers,
                         @PathVariable int carId,
                         @Valid @RequestBody CarDtoIn carDtoIn) {
        try {
            User authentication = authenticationHelper.tryGetUser(headers);
            Car car = carMapper.dtoToObject(carId, carDtoIn);
            carService.updateCar(car, authentication);
            return car;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (EntityDuplicateException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (AuthorizationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @ApiResponse(responseCode = "200", description = "Successful operation")
    @ApiResponse(responseCode = "401", description = "Unauthorized operation")
    @ApiResponse(responseCode = "404", description = "Resource not found")
    @Operation(summary = "Delete car")
    @DeleteMapping("/{carId}")
    public void deleteCar(@RequestHeader HttpHeaders headers, @PathVariable int carId) {
        try {
            User authentication = authenticationHelper.tryGetUser(headers);
            carService.deleteCar(carId, authentication);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (AuthorizationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @ApiResponse(responseCode = "200", description = "Successful operation")
    @ApiResponse(responseCode = "401", description = "Unauthorized operation")
    @ApiResponse(responseCode = "404", description = "Resource not found")
    @Operation(summary = "All visits for one car")
    @GetMapping("/{carId}/visits")
    public List<VisitDtoOut> getVisitsByCarId(@RequestHeader HttpHeaders headers,
                                              @PathVariable int carId,
                                              @RequestParam(required = false) Integer statusId,
                                              @RequestParam(required = false) String sortBy,
                                              @RequestParam(required = false) String sortOrder) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            Car car = carService.getCarById(carId);
            VisitFilterOptions visitFilterOptions = new VisitFilterOptions(carId, car.getLicensePlate(),
                    statusId, car.getUser().getUserId(), car.getUser().getUsername(), sortBy, sortOrder);
            List<Visit> visits = visitService.getVisits(visitFilterOptions, user, Optional.of(car));
            return visitMapper.getVisitDtoOutListFromVisit(visits);
        } catch (AuthorizationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
    @ApiResponse(responseCode = "200", description = "Successful operation")
    @ApiResponse(responseCode = "401", description = "Unauthorized operation")
    @ApiResponse(responseCode = "404", description = "Resource not found")
    @Operation(summary = "Visit history for one car")
    @GetMapping("/{carId}/visits-history")
    public List<VisitsHistoryDtoOut> getVisitsHistoryByCarId(@RequestHeader HttpHeaders headers,
                                                             @PathVariable(required = false) Integer carId,
                                                             @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) @RequestParam(required = false) LocalDate visitDate,
                                                             @RequestParam(required = false) String repairName,
                                                             @RequestParam(required = false) String sortBy,
                                                             @RequestParam(required = false) String sortOrder) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            Car car = carService.getCarById(carId);
            VisitsHistoryFilterOptions filterOptions = new VisitsHistoryFilterOptions(carId, car.getLicensePlate(),
                    visitDate, repairName, car.getUser().getUserId(), car.getUser().getUsername(), sortBy, sortOrder);
            List<VisitsHistory> visitsHistory = visitsHistoryService.getVisitsHistory(filterOptions, user, Optional.of(car));
            return visitsHistoryMapper.getDtoOutListFromVisitsHistory(visitsHistory);
        } catch (AuthorizationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}
