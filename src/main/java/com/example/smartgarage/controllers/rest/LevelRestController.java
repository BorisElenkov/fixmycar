package com.example.smartgarage.controllers.rest;

import com.example.smartgarage.models.Level;
import com.example.smartgarage.services.contracts.LevelService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/levels")
public class LevelRestController {
    private final LevelService levelService;

    @Autowired
    public LevelRestController(LevelService levelService) {
        this.levelService = levelService;
    }

    @ApiResponse(responseCode = "200", description = "Successful operation")
    @Operation(summary = "Get all levels")
    @GetMapping
    public List<Level> getLevels(){
        return levelService.getLevels();
    }
}
