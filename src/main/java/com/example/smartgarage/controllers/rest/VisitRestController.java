package com.example.smartgarage.controllers.rest;

import com.example.smartgarage.exceptions.AuthorizationException;
import com.example.smartgarage.exceptions.CustomException;
import com.example.smartgarage.exceptions.EntityNotFoundException;
import com.example.smartgarage.helpers.AuthenticationHelper;
import com.example.smartgarage.helpers.VisitMapper;
import com.example.smartgarage.models.*;
import com.example.smartgarage.models.dtos.VisitDtoIn;
import com.example.smartgarage.models.dtos.VisitDtoOut;
import com.example.smartgarage.services.contracts.RepairService;
import com.example.smartgarage.services.contracts.StatusService;
import com.example.smartgarage.services.contracts.VisitService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import org.springframework.http.HttpHeaders;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/visits")
public class VisitRestController {

    private final AuthenticationHelper authenticationHelper;
    private final VisitService visitService;
    private final VisitMapper visitMapper;
    private final StatusService statusService;
    private final RepairService repairService;


    @Autowired
    public VisitRestController(AuthenticationHelper authenticationHelper, VisitService visitService, VisitMapper visitMapper, StatusService statusService, RepairService repairService) {
        this.authenticationHelper = authenticationHelper;
        this.visitService = visitService;
        this.visitMapper = visitMapper;
        this.statusService = statusService;
        this.repairService = repairService;
    }

    @ApiResponse(responseCode = "200", description = "Successful operation")
    @ApiResponse(responseCode = "401", description = "Unauthorized operation")
    @Operation(summary = "Get all visits with filter")
    @GetMapping
    public List<VisitDtoOut> getVisits(@RequestHeader HttpHeaders headers,
                                       @RequestParam(required = false) Integer carId,
                                       @RequestParam(required = false) String licensePlate,
                                       @RequestParam(required = false) Integer statusId,
                                       @RequestParam(required = false) Integer customerId,
                                       @RequestParam(required = false) String username,
                                       @RequestParam(required = false) String sortBy,
                                       @RequestParam(required = false) String sortOrder) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            VisitFilterOptions visitFilterOptions = new VisitFilterOptions(carId, licensePlate,
                    statusId, customerId, username, sortBy, sortOrder);
            List<Visit> visits = visitService.getVisits(visitFilterOptions, user, Optional.empty());
            return visitMapper.getVisitDtoOutListFromVisit(visits);
        } catch (AuthorizationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @ApiResponse(responseCode = "200", description = "Successful operation")
    @ApiResponse(responseCode = "401", description = "Unauthorized operation")
    @ApiResponse(responseCode = "404", description = "Resource not found")
    @Operation(summary = "Get one visit")
    @GetMapping("/{visitId}")
    public VisitDtoOut getVisitById(@RequestHeader HttpHeaders headers, @PathVariable int visitId) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            Visit v = visitService.getVisitById(visitId);
            Car car = v.getCar();
            Visit visit = visitService.getVisitById(visitId, user, Optional.of(car));
            return visitMapper.objectToDto(visit);
        } catch (AuthorizationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @ApiResponse(responseCode = "200", description = "Successful operation")
    @ApiResponse(responseCode = "401", description = "Unauthorized operation")
    @Operation(summary = "Create visit")
    @PostMapping
    public VisitDtoOut createVisit(@RequestHeader HttpHeaders headers, @RequestBody VisitDtoIn visitDtoIn) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            Visit visit = visitMapper.dtoToObject(visitDtoIn);
            visitService.createVisit(visit, user);
            return visitMapper.objectToDto(visit);
        } catch (AuthorizationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @ApiResponse(responseCode = "200", description = "Successful operation")
    @ApiResponse(responseCode = "401", description = "Unauthorized operation")
    @ApiResponse(responseCode = "404", description = "Resource not found")
    @Operation(summary = "Update visit")
    @PutMapping("{visitId}")
    public VisitDtoOut updateVisit(@RequestHeader HttpHeaders headers,
                                   @RequestBody VisitDtoIn visitDtoIn,
                                   @PathVariable int visitId) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            Visit visit = visitMapper.dtoToObject(visitId, visitDtoIn);
            visitService.updateVisit(visit, user);
            return visitMapper.objectToDto(visit);
        } catch (AuthorizationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @ApiResponse(responseCode = "200", description = "Successful operation")
    @ApiResponse(responseCode = "401", description = "Unauthorized operation")
    @ApiResponse(responseCode = "404", description = "Resource not found")
    @ApiResponse(responseCode = "409", description = "Duplicate entry")
    @Operation(summary = "Visit status")
    @PutMapping("{visitId}/status")
    public void changeStatus(@RequestHeader HttpHeaders headers,
                             @PathVariable int visitId,
                             @RequestParam(required = true) int statusId) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            visitService.changeStatus(visitId, statusId, user);
        } catch (AuthorizationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @ApiResponse(responseCode = "200", description = "Successful operation")
    @ApiResponse(responseCode = "401", description = "Unauthorized operation")
    @ApiResponse(responseCode = "404", description = "Resource not found")
    @ApiResponse(responseCode = "409", description = "Duplicate entry")
    @Operation(summary = "Add repair for visit")
    @PutMapping("{visitId}/repairs/{repairId}")
    public void addRepair(@RequestHeader HttpHeaders headers,
                          @PathVariable int visitId,
                          @PathVariable int repairId) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            Visit visit = visitService.getVisitById(visitId);
            Repair repair = repairService.getRepairById(repairId);
            visitService.addRepairToVisit(repairId, visitId, user);
        } catch (AuthorizationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (IllegalArgumentException | CustomException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @ApiResponse(responseCode = "200", description = "Successful operation")
    @ApiResponse(responseCode = "401", description = "Unauthorized operation")
    @ApiResponse(responseCode = "404", description = "Resource not found")
    @ApiResponse(responseCode = "409", description = "Duplicate entry")
    @Operation(summary = "Remove repair from visit")
    @DeleteMapping("{visitId}/services/{repairId}")
    public void removeRepair(@RequestHeader HttpHeaders headers,
                             @PathVariable int visitId,
                             @PathVariable int repairId) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            Visit visit = visitService.getVisitById(visitId);
            Repair repair = repairService.getRepairById(repairId);
            visitService.removeRepairFromVisit(repairId, visitId, user);
        } catch (AuthorizationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @ApiResponse(responseCode = "200", description = "Successful operation")
    @ApiResponse(responseCode = "401", description = "Unauthorized operation")
    @ApiResponse(responseCode = "404", description = "Resource not found")
    @Operation(summary = "Delete visit")
    @DeleteMapping("/{visitId}")
    public void deleteVisit(@RequestHeader HttpHeaders headers,
                            @PathVariable int visitId) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            visitService.deleteVisit(visitId, user);
        } catch (AuthorizationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}

