package com.example.smartgarage.controllers.rest;

import com.example.smartgarage.models.Status;
import com.example.smartgarage.services.contracts.StatusService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/statuses")
public class StatusRestController {
    private final StatusService statusService;

    @Autowired
    public StatusRestController(StatusService statusService) {
        this.statusService = statusService;
    }

    @ApiResponse(responseCode = "200", description = "Successful operation")
    @Operation(summary = "Get all statuses")
    @GetMapping
    public List<Status> getStatuses(){
        return statusService.getStatuses();
    }
}
