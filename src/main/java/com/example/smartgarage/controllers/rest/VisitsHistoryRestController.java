package com.example.smartgarage.controllers.rest;

import com.example.smartgarage.exceptions.AuthorizationException;
import com.example.smartgarage.helpers.AuthenticationHelper;
import com.example.smartgarage.helpers.VisitsHistoryMapper;
import com.example.smartgarage.models.User;
import com.example.smartgarage.models.VisitsHistory;
import com.example.smartgarage.models.dtos.VisitsHistoryDtoOut;
import com.example.smartgarage.models.VisitsHistoryFilterOptions;
import com.example.smartgarage.services.contracts.VisitsHistoryService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/history")
public class VisitsHistoryRestController {

    private final AuthenticationHelper authenticationHelper;
    private final VisitsHistoryService visitsHistoryService;
    private final VisitsHistoryMapper visitsHistoryMapper;

    @Autowired
    public VisitsHistoryRestController(AuthenticationHelper authenticationHelper, VisitsHistoryService visitsHistoryService, VisitsHistoryMapper visitsHistoryMapper) {
        this.authenticationHelper = authenticationHelper;
        this.visitsHistoryService = visitsHistoryService;
        this.visitsHistoryMapper = visitsHistoryMapper;
    }

    @ApiResponse(responseCode = "200", description = "Successful operation")
    @ApiResponse(responseCode = "401", description = "Unauthorized operation")
    @Operation(summary = "Get visit history with filter")
    @GetMapping
    public List<VisitsHistoryDtoOut> getVisitsHistory(@RequestHeader HttpHeaders headers,
                                                      @RequestParam(required = false) Integer carId,
                                                      @RequestParam(required = false) String licensePlate,
                                                      @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) @RequestParam(required = false) LocalDate visitDate,
                                                      @RequestParam(required = false) String repairName,
                                                      @RequestParam(required = false) Integer customerId,
                                                      @RequestParam(required = false) String username,
                                                      @RequestParam(required = false) String sortBy,
                                                      @RequestParam(required = false) String sortOrder) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            VisitsHistoryFilterOptions filterOptions = new VisitsHistoryFilterOptions(carId, licensePlate,
                    visitDate, repairName, customerId, username, sortBy, sortOrder);
            List<VisitsHistory> visitsHistory = visitsHistoryService.getVisitsHistory(filterOptions, user, Optional.empty());
            return getDtoOutListFromVisitsHistory(visitsHistory);
        } catch (AuthorizationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    private List<VisitsHistoryDtoOut> getDtoOutListFromVisitsHistory(List<VisitsHistory> visitsHistory) {
        List<VisitsHistoryDtoOut> result = new ArrayList<>();
        if (!visitsHistory.isEmpty()) {
            for (VisitsHistory history : visitsHistory) {
                result.add(visitsHistoryMapper.objectToDto(history));
            }
        }
        return result;
    }
}
