package com.example.smartgarage.helpers;

import com.example.smartgarage.models.VisitsHistoryFilterOptions;
import com.example.smartgarage.models.dtos.VisitsHistoryFilterDto;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class VisitsHistoryFilterMapper {

    public VisitsHistoryFilterOptions dtoToObject(VisitsHistoryFilterDto filterDto){
        VisitsHistoryFilterOptions filterOptions = new VisitsHistoryFilterOptions();

        filterOptions.setCarId(Optional.ofNullable(filterDto.getCarId()));

        if (filterDto.getLicensePlate()!=null && filterDto.getLicensePlate().isEmpty()) {
            filterOptions.setLicensePlate(Optional.empty());
        }
        else{filterOptions.setLicensePlate(Optional.ofNullable(filterDto.getLicensePlate()));}

        filterOptions.setVisitDate(Optional.ofNullable(filterDto.getVisitDate()));

        if (filterDto.getRepairName()!=null && filterDto.getRepairName().isEmpty()) {
            filterOptions.setRepairName(Optional.empty());
        }
        else{filterOptions.setRepairName(Optional.ofNullable(filterDto.getRepairName()));}

        filterOptions.setCustomerId(Optional.ofNullable(filterDto.getCustomerId()));

        if (filterDto.getUsername()!=null && filterDto.getUsername().isEmpty()) {
            filterOptions.setUsername(Optional.empty());
        }
        else{filterOptions.setUsername(Optional.ofNullable(filterDto.getUsername()));}

        filterOptions.setSortBy(Optional.ofNullable(filterDto.getSortBy()));
        filterOptions.setSortOrder(Optional.ofNullable(filterDto.getSortOrder()));
        return filterOptions;
    }
}
