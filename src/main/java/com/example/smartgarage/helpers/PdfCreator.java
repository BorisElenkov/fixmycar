package com.example.smartgarage.helpers;


import java.io.*;

import com.example.smartgarage.models.Visit;
import com.example.smartgarage.models.VisitCurrency;
import com.example.smartgarage.models.VisitsHistory;
import com.example.smartgarage.services.contracts.VisitService;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class PdfCreator {

    private static Font catFont = new Font(Font.FontFamily.TIMES_ROMAN, 18, Font.BOLD);
    private static Font redFont = new Font(Font.FontFamily.TIMES_ROMAN, 18, Font.NORMAL, BaseColor.RED);
    private static Font subFont = new Font(Font.FontFamily.TIMES_ROMAN, 16, Font.BOLD);
    private static Font smallRed = new Font(Font.FontFamily.TIMES_ROMAN, 14, Font.NORMAL, BaseColor.RED);
    private static Font tableFont = new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.NORMAL, BaseColor.BLACK);
    private static Font tableHeading = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD, BaseColor.RED);
    private final VisitService visitService;

    @Autowired
    public PdfCreator(VisitService visitService) {
        this.visitService = visitService;
    }

    public File createPdf(Visit visit, VisitCurrency currency) throws IOException, DocumentException {

        Document document = new Document();
        String fileName = String.format("Visit-report-%s-%d.pdf"
                , visit.getCar().getUser().getUsername(), visit.getVisitId());
        PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(fileName));

        document.open();
        Paragraph preface = new Paragraph();
        preface.add(Chunk.NEWLINE);

        preface.add(new Paragraph("FixMyCar", redFont));
        preface.add(new Paragraph("Fixing is all we know!", smallRed));

        preface.add(Chunk.NEWLINE);
        preface.add(new Paragraph("Visit report", catFont));

        preface.add(Chunk.NEWLINE);
        preface.add(new Paragraph("Customer information", subFont));
        preface.add(Chunk.NEWLINE);
        PdfPTable userTable = generateUserTable(visit);
        preface.add(userTable);

        preface.add(Chunk.NEWLINE);
        preface.add(new Paragraph("Car information", subFont));
        preface.add(Chunk.NEWLINE);
        PdfPTable carTable = generateCarTable(visit);
        preface.add(carTable);

        preface.add(Chunk.NEWLINE);
        preface.add(new Paragraph("Repairs", subFont));
        preface.add(Chunk.NEWLINE);
        PdfPTable repairsTable = generateRepairsTable(visit);
        preface.add(repairsTable);

        preface.add(Chunk.NEWLINE);
        preface.add(new Paragraph("Total", subFont));
        preface.add(Chunk.NEWLINE);
        PdfPTable totalTable = generateTotalTable(visit, currency);
        preface.add(totalTable);

        preface.add(Chunk.NEWLINE);
        preface.add(new Paragraph(String.format("Status: %s", visit.getStatus().getName().toString()), subFont));

        document.add(preface);
        document.close();
        return new File(fileName);
    }

    private PdfPTable generateUserTable(Visit visit) {
        PdfPTable table = new PdfPTable(6);

        PdfPCell c1 = new PdfPCell(new Phrase("First name", tableHeading));
        c1.setHorizontalAlignment(Element.ALIGN_LEFT);
        table.addCell(c1);

        c1 = new PdfPCell(new Phrase("Last name", tableHeading));
        c1.setHorizontalAlignment(Element.ALIGN_LEFT);
        table.addCell(c1);

        c1 = new PdfPCell(new Phrase("Phone", tableHeading));
        c1.setHorizontalAlignment(Element.ALIGN_LEFT);
        table.addCell(c1);

        c1 = new PdfPCell(new Phrase("Email", tableHeading));
        c1.setHorizontalAlignment(Element.ALIGN_LEFT);
        table.addCell(c1);

        c1 = new PdfPCell(new Phrase("Username", tableHeading));
        c1.setHorizontalAlignment(Element.ALIGN_LEFT);
        table.addCell(c1);

        c1 = new PdfPCell(new Phrase("Level", tableHeading));
        c1.setHorizontalAlignment(Element.ALIGN_LEFT);
        table.addCell(c1);

        table.setHeaderRows(1);
        table.addCell(new Phrase(visit.getCar().getUser().getFirstName(), tableFont));
        table.addCell(new Phrase(visit.getCar().getUser().getLastName(), tableFont));
        table.addCell(new Phrase(visit.getCar().getUser().getPhone(), tableFont));
        table.addCell(new Phrase(visit.getCar().getUser().getEmail(), tableFont));
        table.addCell(new Phrase(visit.getCar().getUser().getUsername(), tableFont));
        table.addCell(new Phrase(visit.getCar().getUser().getLevel().getName().toString(), tableFont));
        return table;
    }

    private PdfPTable generateCarTable(Visit visit) {
        PdfPTable table = new PdfPTable(5);

        PdfPCell c1 = new PdfPCell(new Phrase("License plate", tableHeading));
        c1.setHorizontalAlignment(Element.ALIGN_LEFT);
        table.addCell(c1);

        c1 = new PdfPCell(new Phrase("VIN", tableHeading));
        c1.setHorizontalAlignment(Element.ALIGN_LEFT);
        table.addCell(c1);

        c1 = new PdfPCell(new Phrase("Year", tableHeading));
        c1.setHorizontalAlignment(Element.ALIGN_LEFT);
        table.addCell(c1);

        c1 = new PdfPCell(new Phrase("Make", tableHeading));
        c1.setHorizontalAlignment(Element.ALIGN_LEFT);
        table.addCell(c1);

        c1 = new PdfPCell(new Phrase("Model", tableHeading));
        c1.setHorizontalAlignment(Element.ALIGN_LEFT);
        table.addCell(c1);

        table.setHeaderRows(1);
        table.addCell(new Phrase(visit.getCar().getLicensePlate(), tableFont));
        table.addCell(new Phrase(visit.getCar().getVin(), tableFont));
        table.addCell(new Phrase(String.valueOf(visit.getCar().getYear()), tableFont));
        table.addCell(new Phrase(visit.getCar().getCarModel().getMake().getMakeName(), tableFont));
        table.addCell(new Phrase(visit.getCar().getCarModel().getModelName(), tableFont));
        return table;
    }

    private PdfPTable generateRepairsTable(Visit visit) {
        PdfPTable table = new PdfPTable(2);


        PdfPCell c1 = new PdfPCell(new Phrase("Repair name", tableHeading));
        c1.setHorizontalAlignment(Element.ALIGN_LEFT);
        table.addCell(c1);

        c1 = new PdfPCell(new Phrase("Price", tableHeading));
        c1.setHorizontalAlignment(Element.ALIGN_LEFT);
        table.addCell(c1);
        table.setHeaderRows(1);

        for (VisitsHistory vh : visit.getVisitsHistory()) {
            table.addCell(new Phrase(vh.getRepairName(), tableFont));
            table.addCell(new Phrase(String.valueOf(vh.getPrice()), tableFont));
        }

        return table;
    }

    private PdfPTable generateTotalTable(Visit visit, VisitCurrency currency) throws IOException {
        PdfPTable table = new PdfPTable(2);


        PdfPCell c1 = new PdfPCell(new Phrase("Total price", tableHeading));
        c1.setHorizontalAlignment(Element.ALIGN_LEFT);
        table.addCell(c1);

        c1 = new PdfPCell(new Phrase("Currency", tableHeading));
        c1.setHorizontalAlignment(Element.ALIGN_LEFT);
        table.addCell(c1);
        table.setHeaderRows(1);

        Double totalPrice = visitService.getTotalPriceDiffCurrency(visit.getVisitId(), currency.getCurrencyCode().toString());
        Double total = Math.round(totalPrice * 100.0) / 100.0;
        table.addCell(new Phrase(String.valueOf(total), tableFont));
        table.addCell(new Phrase(currency.getCurrencyCode().toString(), tableFont));

        return table;
    }
}
