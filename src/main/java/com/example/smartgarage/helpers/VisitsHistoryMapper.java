package com.example.smartgarage.helpers;

import com.example.smartgarage.models.Visit;
import com.example.smartgarage.models.VisitsHistory;
import com.example.smartgarage.models.dtos.VisitsHistoryDtoOut;
import com.example.smartgarage.services.contracts.VisitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class VisitsHistoryMapper {
    private final VisitService visitService;

    @Autowired
    public VisitsHistoryMapper(VisitService visitService) {
        this.visitService = visitService;
    }

    public VisitsHistoryDtoOut objectToDto(VisitsHistory visitsHistory){
        VisitsHistoryDtoOut dto = new VisitsHistoryDtoOut();
        Visit visit = visitService.getVisitById(visitsHistory.getVisitId());
        dto.setVisitsHistoryId(visitsHistory.getVisitsHistoryId());
        dto.setUsername(visit.getCar().getUser().getUsername());
        dto.setCarLicensePlate(visit.getCar().getLicensePlate());
        dto.setVisitId(visitsHistory.getVisitId());
        dto.setRepair(visitsHistory.getRepairName());
        dto.setDate(visit.getVisitDate());
        return dto;
    }

    public List<VisitsHistoryDtoOut> getDtoOutListFromVisitsHistory(List<VisitsHistory> history) {
        List<VisitsHistoryDtoOut> result = new ArrayList<>();
        if (!history.isEmpty()) {
            for (VisitsHistory vh : history) {
                result.add(objectToDto(vh));
            }
        }
        return result;
    }
}
