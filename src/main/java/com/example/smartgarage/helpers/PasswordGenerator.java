package com.example.smartgarage.helpers;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class PasswordGenerator {
    private static final String CAPITAL_LETTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    private static final String SMALL_LETTERS = "abcdefghijklmnopqrstuvwxyz";
    private static final String DIGITS = "0123456789";
    private static final String SPECIAL_CHARACTERS = "+-*&^%$#@!~";
    private static final int LENGTH = 8;

    public static String generate() {
        StringBuilder builder = new StringBuilder();
        List<Character> charList = new ArrayList<>();

        // Add at least one capital letter
        charList.add(CAPITAL_LETTERS.charAt(new Random().nextInt(CAPITAL_LETTERS.length())));
        // Add at least one small letter
        charList.add(SMALL_LETTERS.charAt(new Random().nextInt(CAPITAL_LETTERS.length())));
        // Add at least one digit
        charList.add(DIGITS.charAt(new Random().nextInt(DIGITS.length())));
        // Add at least one special character
        charList.add(SPECIAL_CHARACTERS.charAt(new Random().nextInt(SPECIAL_CHARACTERS.length())));

        // Add random characters to fill the remaining length
        SecureRandom secureRandom = new SecureRandom();
        int remainingLength = LENGTH - 4;
        for (int i = 0; i < remainingLength; i++) {
            int charType = secureRandom.nextInt(3); // 0 for small letters, 1 for capital letters, 2 for special characters
            if (charType == 0) {
                charList.add(SMALL_LETTERS.charAt(secureRandom.nextInt(SMALL_LETTERS.length())));
            } else if (charType == 1) {
                charList.add(CAPITAL_LETTERS.charAt(secureRandom.nextInt(CAPITAL_LETTERS.length())));
            } else {
                charList.add(SPECIAL_CHARACTERS.charAt(secureRandom.nextInt(SPECIAL_CHARACTERS.length())));
            }
        }

        for (Character c : charList) {
            builder.append(c);
        }
        return builder.toString();
    }
}
