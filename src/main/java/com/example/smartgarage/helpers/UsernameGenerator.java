package com.example.smartgarage.helpers;

import java.util.Random;
public class UsernameGenerator {

    private static final String DIGITS = "0123456789";

    private static final int LENGTH = 4;

    public static String generate(String firstName){
        StringBuilder builder = new StringBuilder();
        builder.append(firstName);
        for (int i = 0; i < LENGTH; i++) {
            builder.append(DIGITS.charAt(new Random().nextInt(DIGITS.length())));
        }
        return builder.toString();
    }
}
