package com.example.smartgarage.helpers;

import com.example.smartgarage.models.User;
import com.example.smartgarage.models.dtos.UserDtoIn;
import com.example.smartgarage.services.contracts.LevelService;
import com.example.smartgarage.services.contracts.RoleService;
import com.example.smartgarage.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class UserMapper {
    private final UserService userService;
    private final LevelService levelService;
    private final RoleService roleService;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public UserMapper(UserService userService, LevelService levelService, RoleService roleService, PasswordEncoder passwordEncoder) {
        this.userService = userService;
        this.levelService = levelService;
        this.roleService = roleService;
        this.passwordEncoder = passwordEncoder;
    }

    public User dtoToObject(UserDtoIn userDtoIn) {
        User user = new User();
        user.setFirstName(userDtoIn.getFirstName());
        user.setLastName(userDtoIn.getLastName());
        user.setPhone(userDtoIn.getPhone());
        user.setEmail(userDtoIn.getEmail());
        String password = PasswordGenerator.generate();
        user.setPassword(password);
        String username = UsernameGenerator.generate(userDtoIn.getFirstName());
        user.setUsername(username);
        user.setLevel(levelService.getLevelById(userDtoIn.getLevelId()));
        user.setRole(roleService.getRoleById(userDtoIn.getRoleId()));
        return user;
    }

    public User dtoToObject(int userId, UserDtoIn userDtoIn) {
        User user = dtoToObject(userDtoIn);
        user.setUserId(userId);
        return user;
    }

    public User dtoToObjectUpdate(User updatedUser, UserDtoIn userDtoIn) {
        User user = new User();
        user.setUserId(updatedUser.getUserId());
        user.setFirstName(userDtoIn.getFirstName());
        user.setLastName(userDtoIn.getLastName());
        user.setPhone(userDtoIn.getPhone());
        user.setEmail(userDtoIn.getEmail());
        user.setUsername(updatedUser.getUsername());
        user.setPassword(updatedUser.getPassword());
        user.setLevel(levelService.getLevelById(userDtoIn.getLevelId()));
        user.setRole(roleService.getRoleById(userDtoIn.getRoleId()));
        return user;
    }

    public UserDtoIn objectToDtoUpdate(User user) {
        UserDtoIn userDtoIn = new UserDtoIn();
        userDtoIn.setFirstName(user.getFirstName());
        userDtoIn.setLastName(user.getLastName());
        userDtoIn.setPhone(user.getPhone());
        userDtoIn.setEmail(user.getEmail());
        userDtoIn.setRoleId(user.getRole().getRoleId());
        userDtoIn.setLevelId(user.getLevel().getLevelId());
        return userDtoIn;
    }
}
