package com.example.smartgarage.helpers;

import com.example.smartgarage.models.Visit;
import com.example.smartgarage.models.VisitsHistory;
import com.example.smartgarage.models.dtos.VisitDtoIn;
import com.example.smartgarage.models.dtos.VisitDtoOut;
import com.example.smartgarage.services.contracts.StatusService;
import com.example.smartgarage.services.contracts.CarService;
import com.example.smartgarage.services.contracts.VisitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Component
public class VisitMapper {
    private final StatusService statusService;
    private final CarService carService;
    private final VisitService visitService;

    @Autowired
    public VisitMapper(StatusService statusService, CarService carService, VisitService visitService) {
        this.statusService = statusService;
        this.carService = carService;
        this.visitService = visitService;
    }

    public Visit dtoToObject(VisitDtoIn visitDtoIn) {
        Visit visit = new Visit();
        visit.setVisitDate(LocalDate.now());
        visit.setStatus(statusService.getStatusById(1));
        visit.setCar(carService.getCarById(visitDtoIn.getCarId()));
        return visit;
    }

    public Visit dtoToObject(int visitId, VisitDtoIn visitDtoIn) {
        Visit existingVisit = visitService.getVisitById(visitId);
        existingVisit.setCar(carService.getCarById(visitDtoIn.getCarId()));
        return existingVisit;
    }

    public VisitDtoOut objectToDto(Visit visit) {
        VisitDtoOut dto = new VisitDtoOut();
        dto.setVisitId(visit.getVisitId());
        dto.setCustomerId(visit.getCar().getUser().getUserId());
        dto.setUsername(visit.getCar().getUser().getUsername());
        dto.setCarId(visit.getCar().getCarId());
        dto.setLicensePlate(visit.getCar().getLicensePlate());
        dto.setStatusId(visit.getStatus().getStatusId());
        dto.setStatusName(visit.getStatus().getName().toString());
        dto.setVisitDate(visit.getVisitDate());
        if (!visit.getVisitsHistory().isEmpty()) {
            for (VisitsHistory history : visit.getVisitsHistory()) {
                dto.getRepairs().put(history.getRepairName(), history.getPrice());
            }
        }
        if (visitService.getTotalPrice(visit.getVisitId()) == null) {
            dto.setTotalPrice(0);
        } else {
            Double totalPrice = visitService.getTotalPrice(visit.getVisitId());
            dto.setTotalPrice(Math.round(totalPrice * 100.0) / 100.0);
        }
        dto.setCurrency("BGN");
        return dto;
    }

    public List<VisitDtoOut> getVisitDtoOutListFromVisit(List<Visit> visits) {
        List<VisitDtoOut> result = new ArrayList<>();
        if (!visits.isEmpty()) {
            for (Visit visit : visits) {
                result.add(objectToDto(visit));
            }
        }
        return result;
    }
}
