package com.example.smartgarage.helpers;

import com.example.smartgarage.models.Car;
import com.example.smartgarage.models.dtos.CarDtoIn;
import com.example.smartgarage.services.contracts.CarService;
import com.example.smartgarage.services.contracts.ModelService;
import com.example.smartgarage.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CarMapper {

    private final CarService carService;
    private final UserService userService;
    private final ModelService modelService;

    @Autowired
    public CarMapper(CarService carService, UserService userService, ModelService modelService) {
        this.carService = carService;
        this.userService = userService;
        this.modelService = modelService;
    }

    public Car dtoToObject(CarDtoIn carDtoIn) {
        Car car = new Car();
        car.setCarModel(modelService.getModelById(carDtoIn.getModelId()));
        car.setLicensePlate(carDtoIn.getLicensePlate());
        car.setVin(carDtoIn.getVin());
        car.setYear(carDtoIn.getYear());
        car.setUser(userService.getUserById(carDtoIn.getUserId()));
        return car;
    }

    public Car dtoToObject(int carId, CarDtoIn carDtoIn) {
        Car car = dtoToObject(carDtoIn);
        car.setCarId(carId);
        return car;
    }

    public Car dtoToObjectCreate(CarDtoIn carDtoIn, int userId) {
        Car car = new Car();
        car.setLicensePlate(carDtoIn.getLicensePlate());
        car.setVin(carDtoIn.getVin());
        car.setYear(carDtoIn.getYear());
        car.setCarModel(modelService.getModelById(carDtoIn.getModelId()));
        car.setUser(userService.getUserById(userId));
        return car;
    }

    public CarDtoIn carDtoInUpdate(Car car) {
        CarDtoIn carDtoIn = new CarDtoIn();
        carDtoIn.setLicensePlate(car.getLicensePlate());
        carDtoIn.setVin(car.getVin());
        carDtoIn.setYear(car.getYear());
        carDtoIn.setMakeId(car.getCarModel().getMake().getMakeId());
        carDtoIn.setModelId(car.getCarModel().getModelId());
        return carDtoIn;
    }

    public Car dtoToObjectUpdate(CarDtoIn carDtoIn, int userId, Car carToUpdate) {
        Car car = carToUpdate;
        car.setCarModel(modelService.getModelById(carDtoIn.getModelId()));
        car.setLicensePlate(carDtoIn.getLicensePlate());
        car.setVin(carDtoIn.getVin());
        car.setYear(carDtoIn.getYear());
        car.setUser(userService.getUserById(userId));
        return car;
    }
}
