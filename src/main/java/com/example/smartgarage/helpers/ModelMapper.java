package com.example.smartgarage.helpers;

import com.example.smartgarage.models.CarModel;
import com.example.smartgarage.models.dtos.ModelDtoIn;
import com.example.smartgarage.services.contracts.MakeService;
import com.example.smartgarage.services.contracts.ModelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ModelMapper {
    private final ModelService modelService;
    private final MakeService makeService;

    @Autowired
    public ModelMapper(ModelService modelService, MakeService makeService) {
        this.modelService = modelService;
        this.makeService = makeService;
    }

    public CarModel dtoToObject(ModelDtoIn modelDtoIn) {
        CarModel carModel = new CarModel();
        carModel.setModelName(modelDtoIn.getModelName());
        carModel.setMake(makeService.getMakeById(modelDtoIn.getMakeId()));
        return carModel;
    }

    public CarModel dtoToObject(int makeId, ModelDtoIn modelDtoIn) {
        CarModel carModel = dtoToObject(modelDtoIn);
        carModel.setMake(makeService.getMakeById(makeId));
        return carModel;
    }

    public CarModel dtoToObject(int makeId, int modelId, ModelDtoIn modelDtoIn) {
        CarModel carModel = dtoToObject(modelDtoIn);
        carModel.setMake(makeService.getMakeById(makeId));
        carModel.setModelId(modelId);
        return carModel;
    }
    
    public CarModel dtoToObjectUpdate(int modelId, ModelDtoIn modelDtoIn) {
        CarModel carModel = new CarModel();
        carModel.setModelId(modelId);
        carModel.setModelName(modelDtoIn.getModelName());
        carModel.setMake(makeService.getMakeById(modelDtoIn.getMakeId()));
        return carModel;
    }

    public ModelDtoIn objectToDtoUpdate(CarModel carModel) {
        ModelDtoIn modelDtoIn = new ModelDtoIn();
        modelDtoIn.setModelName(carModel.getModelName());
        modelDtoIn.setMakeId(carModel.getMake().getMakeId());
        return modelDtoIn;
    }
}
