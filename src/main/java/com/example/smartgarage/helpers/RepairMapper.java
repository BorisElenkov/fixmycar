package com.example.smartgarage.helpers;

import com.example.smartgarage.models.Repair;
import com.example.smartgarage.models.dtos.RepairDtoIn;
import com.example.smartgarage.repositories.contracts.VisitRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class RepairMapper {

    private final VisitRepository visitRepository;

    @Autowired
    public RepairMapper(VisitRepository visitRepository) {
        this.visitRepository = visitRepository;
    }

    public Repair dtoToObject(RepairDtoIn repairDtoIn) {
        Repair repair = new Repair();
        repair.setName(repairDtoIn.getName());
        repair.setPrice(repairDtoIn.getPrice());
        return repair;
    }

    public Repair dtoToObject(Repair repair, RepairDtoIn repairDtoIn) {
        repair.setName(repairDtoIn.getName());
        repair.setPrice(repairDtoIn.getPrice());
        return repair;
    }

    public RepairDtoIn objectToDto(Repair repair) {
        RepairDtoIn dto = new RepairDtoIn();
        dto.setName(repair.getName());
        dto.setPrice(repair.getPrice());
        return dto;
    }
}
