package com.example.smartgarage.helpers;

import com.example.smartgarage.models.VisitFilterOptions;
import com.example.smartgarage.models.dtos.VisitFilterDto;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class VisitFilterMapper {

    public VisitFilterOptions dtoToObject(VisitFilterDto filterDto){
        VisitFilterOptions filterOptions = new VisitFilterOptions();

        filterOptions.setCarId(Optional.ofNullable(filterDto.getCarId()));

        if (filterDto.getLicensePlate()!=null && filterDto.getLicensePlate().isEmpty()) {
            filterOptions.setLicensePlate(Optional.empty());
        }
        else{filterOptions.setLicensePlate(Optional.ofNullable(filterDto.getLicensePlate()));}

        filterOptions.setStatusId(Optional.ofNullable(filterDto.getStatusId()));

        filterOptions.setCustomerId(Optional.ofNullable(filterDto.getCustomerId()));

        if (filterDto.getUsername()!=null && filterDto.getUsername().isEmpty()) {
            filterOptions.setUsername(Optional.empty());
        }
        else{filterOptions.setUsername(Optional.ofNullable(filterDto.getUsername()));}

        filterOptions.setSortBy(Optional.ofNullable(filterDto.getSortBy()));
        filterOptions.setSortOrder(Optional.ofNullable(filterDto.getSortOrder()));
        return filterOptions;
    }
}
