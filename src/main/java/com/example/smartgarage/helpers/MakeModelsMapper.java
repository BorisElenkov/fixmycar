package com.example.smartgarage.helpers;

import com.example.smartgarage.models.CarModel;
import com.example.smartgarage.models.Make;
import com.example.smartgarage.models.dtos.CarFormMakeDto;
import com.example.smartgarage.models.dtos.CarFormModelDto;
import com.example.smartgarage.services.contracts.MakeService;
import com.example.smartgarage.services.contracts.ModelService;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class MakeModelsMapper {
    private final MakeService makeService;
    private final ModelService modelService;

    public MakeModelsMapper(MakeService makeService, ModelService modelService) {
        this.makeService = makeService;
        this.modelService = modelService;
    }

    public CarFormMakeDto getMakeModels(int makeId){
        Make make = makeService.getMakeById(makeId);
        List<CarModel> models = modelService.getAllModelsByMake(make.getMakeName());

        CarFormMakeDto dto = new CarFormMakeDto();
        dto.setMakeId(make.getMakeId());
        dto.setMakeName(make.getMakeName());
        for(CarModel model : models) {
            CarFormModelDto modelDto = new CarFormModelDto();
            modelDto.setModelId(model.getModelId());
            modelDto.setModelName(model.getModelName());
            dto.getModels().add(modelDto);
        }
        return dto;
    }

    public List<CarFormMakeDto> getMakesDto(){
        List<CarFormMakeDto> makesDto = new ArrayList<>();
        List<Make> makes = makeService.getMakes();
        for(Make make : makes){
            makesDto.add(getMakeModels(make.getMakeId()));
        }
        return makesDto;
    }
}
