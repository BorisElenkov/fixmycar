package com.example.smartgarage.helpers;

import com.example.smartgarage.models.UserFilterOptions;
import com.example.smartgarage.models.dtos.UserFilterDto;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class UserFilterOptionsMapper {

    public UserFilterOptions dtoToObject(UserFilterDto filterDto) {
        UserFilterOptions filterOptions = new UserFilterOptions();

        if (filterDto.getFirstName() != null && filterDto.getFirstName().isEmpty()) {
            filterOptions.setFirstName(Optional.empty());
        } else {
            filterOptions.setFirstName(Optional.ofNullable(filterDto.getFirstName()));
        }

        if (filterDto.getLastName() != null && filterDto.getLastName().isEmpty()) {
            filterOptions.setLastName(Optional.empty());
        } else {
            filterOptions.setLastName(Optional.ofNullable(filterDto.getLastName()));
        }

        if (filterDto.getPhone() != null && filterDto.getPhone().isEmpty()) {
            filterOptions.setPhone(Optional.empty());
        } else {
            filterOptions.setPhone(Optional.ofNullable(filterDto.getPhone()));
        }

        if (filterDto.getEmail() != null && filterDto.getEmail().isEmpty()) {
            filterOptions.setEmail(Optional.empty());
        } else {
            filterOptions.setEmail(Optional.ofNullable(filterDto.getEmail()));
        }

        if (filterDto.getUsername() != null && filterDto.getUsername().isEmpty()) {
            filterOptions.setUsername(Optional.empty());
        } else {
            filterOptions.setUsername(Optional.ofNullable(filterDto.getUsername()));
        }

        if (filterDto.getLicensePlate() != null && filterDto.getLicensePlate().isEmpty()) {
            filterOptions.setLicensePlate(Optional.empty());
        } else {
            filterOptions.setLicensePlate(Optional.ofNullable(filterDto.getLicensePlate()));
        }

        if (filterDto.getVin() != null && filterDto.getVin().isEmpty()) {
            filterOptions.setVin(Optional.empty());
        } else {
            filterOptions.setVin(Optional.ofNullable(filterDto.getVin()));
        }

        if (filterDto.getModelName() != null && filterDto.getModelName().isEmpty()) {
            filterOptions.setModelName(Optional.empty());
        } else {
            filterOptions.setModelName(Optional.ofNullable(filterDto.getModelName()));
        }

        if (filterDto.getMakeName() != null && filterDto.getMakeName().isEmpty()) {
            filterOptions.setMakeName(Optional.empty());
        } else {
            filterOptions.setMakeName(Optional.ofNullable(filterDto.getMakeName()));
        }

        filterOptions.setRoleId(Optional.ofNullable(filterDto.getRoleId()));
        filterOptions.setStartDate(Optional.ofNullable(filterDto.getStartDate()));
        filterOptions.setEndDate(Optional.ofNullable(filterDto.getEndDate()));

        filterOptions.setSortBy(Optional.ofNullable(filterDto.getSortBy()));
        filterOptions.setOrderBy(Optional.ofNullable(filterDto.getOrderBy()));
        return filterOptions;
    }
}
