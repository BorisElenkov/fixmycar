package com.example.smartgarage.helpers;

import com.example.smartgarage.models.Make;
import com.example.smartgarage.models.dtos.MakeDtoIn;
import com.example.smartgarage.services.contracts.MakeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class MakeMapper {

    private final MakeService makeService;

    @Autowired
    public MakeMapper(MakeService makeService) {
        this.makeService = makeService;
    }

    public Make dtoToObject(MakeDtoIn makeDtoIn) {
        Make make = new Make();
        make.setMakeName(makeDtoIn.getMakeName());
        return make;
    }

    public Make dtoToObject(int makeId, MakeDtoIn makeDtoIn) {
        Make make = dtoToObject(makeDtoIn);
        make.setMakeId(makeId);
        return make;
    }
}
