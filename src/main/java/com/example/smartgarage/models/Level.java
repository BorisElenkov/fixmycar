package com.example.smartgarage.models;

import com.example.smartgarage.models.enums.LevelName;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "levels")
public class Level {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "level_id")
    private int levelId;

    @Column(name = "level_name")
    @Enumerated(EnumType.STRING)
    private LevelName name;

    @Column(name = "discount")
    private int discount;

    @Column(name = "min_expenditure")
    double minExpenditure;

    public int getLevelId() {
        return levelId;
    }

    public void setLevelId(int levelId) {
        this.levelId = levelId;
    }

    public LevelName getName() {
        return name;
    }

    public void setName(LevelName name) {
        this.name = name;
    }

    public int getDiscount() {
        return discount;
    }

    public void setDiscount(int discount) {
        this.discount = discount;
    }

    public double getMinExpenditure() {
        return minExpenditure;
    }

    public void setMinExpenditure(double minExpenditure) {
        this.minExpenditure = minExpenditure;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Level level = (Level) o;
        return levelId == level.levelId && discount == level.discount && Double.compare(level.minExpenditure, minExpenditure) == 0 && name == level.name;
    }

    @Override
    public int hashCode() {
        return Objects.hash(levelId, name, discount, minExpenditure);
    }
}
