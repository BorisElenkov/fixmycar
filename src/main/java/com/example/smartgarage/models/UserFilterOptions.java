package com.example.smartgarage.models;

import org.apache.tomcat.jni.Local;

import java.time.LocalDate;
import java.util.Optional;

public class UserFilterOptions {
    private Optional<String> firstName;
    private Optional<String> lastName;
    private Optional<String> phone;
    private Optional<String> email;
    private Optional<String> username;
    private Optional<String> licensePlate;
    private Optional<String> vin;
    private Optional<String> modelName;
    private Optional<String> makeName;
    private Optional<Integer> roleId;
    private Optional<LocalDate> startDate;
    private Optional<LocalDate> endDate;
    private Optional<String> sortBy;
    private Optional<String> orderBy;

    public UserFilterOptions() {
        this(null, null, null, null, null, null,
                null, null, null, null, null, null, null, null);
    }

    public UserFilterOptions(String firstName, String lastName, String phone, String email,
                             String username, String licensePlate, String vin,
                             String modelName, String makeName, Integer roleId, LocalDate startDate,
                             LocalDate endDate, String sortBy, String orderBy) {
        this.firstName = Optional.ofNullable(firstName);
        this.lastName = Optional.ofNullable(lastName);
        this.phone = Optional.ofNullable(phone);
        this.email = Optional.ofNullable(email);
        this.username = Optional.ofNullable(username);
        this.licensePlate = Optional.ofNullable(licensePlate);
        this.vin = Optional.ofNullable(vin);
        this.modelName = Optional.ofNullable(modelName);
        this.makeName = Optional.ofNullable(makeName);
        this.roleId = Optional.ofNullable(roleId);
        this.startDate = Optional.ofNullable(startDate);
        this.endDate = Optional.ofNullable(endDate);
        this.sortBy = Optional.ofNullable(sortBy);
        this.orderBy = Optional.ofNullable(orderBy);
    }

    public Optional<String> getFirstName() {
        return firstName;
    }

    public void setFirstName(Optional<String> firstName) {
        this.firstName = firstName;
    }

    public Optional<String> getLastName() {
        return lastName;
    }

    public void setLastName(Optional<String> lastName) {
        this.lastName = lastName;
    }

    public Optional<String> getPhone() {
        return phone;
    }

    public void setPhone(Optional<String> phone) {
        this.phone = phone;
    }

    public Optional<String> getEmail() {
        return email;
    }

    public void setEmail(Optional<String> email) {
        this.email = email;
    }

    public Optional<String> getUsername() {
        return username;
    }

    public void setUsername(Optional<String> username) {
        this.username = username;
    }

    public Optional<String> getLicensePlate() {
        return licensePlate;
    }

    public void setLicensePlate(Optional<String> licensePlate) {
        this.licensePlate = licensePlate;
    }

    public Optional<String> getVin() {
        return vin;
    }

    public void setVin(Optional<String> vin) {
        this.vin = vin;
    }

    public Optional<String> getModelName() {
        return modelName;
    }

    public void setModelName(Optional<String> modelName) {
        this.modelName = modelName;
    }

    public Optional<String> getMakeName() {
        return makeName;
    }

    public void setMakeName(Optional<String> makeName) {
        this.makeName = makeName;
    }

    public Optional<Integer> getRoleId() {
        return roleId;
    }

    public void setRoleId(Optional<Integer> roleId) {
        this.roleId = roleId;
    }

    public Optional<LocalDate> getStartDate() {
        return startDate;
    }

    public void setStartDate(Optional<LocalDate> startDate) {
        this.startDate = startDate;
    }

    public Optional<LocalDate> getEndDate() {
        return endDate;
    }

    public void setEndDate(Optional<LocalDate> endDate) {
        this.endDate = endDate;
    }

    public Optional<String> getSortBy() {
        return sortBy;
    }

    public void setSortBy(Optional<String> sortBy) {
        this.sortBy = sortBy;
    }

    public Optional<String> getOrderBy() {
        return orderBy;
    }

    public void setOrderBy(Optional<String> orderBy) {
        this.orderBy = orderBy;
    }
}
