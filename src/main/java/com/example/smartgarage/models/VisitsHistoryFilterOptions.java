package com.example.smartgarage.models;

import java.time.LocalDate;
import java.util.Optional;

public class VisitsHistoryFilterOptions {
    private Optional<Integer> carId;
    private Optional<String> licensePlate;
    private Optional<LocalDate> visitDate;
    private Optional<String> repairName;
    private Optional<Integer> customerId;
    private Optional<String> username;
    private Optional<String> sortBy;
    private Optional<String> sortOrder;

    public VisitsHistoryFilterOptions() {
        this(null, null, null, null, null, null, null, null);
    }

    public VisitsHistoryFilterOptions(Integer carId, String licensePlate, LocalDate visitDate,
                                      String repairName, Integer customerId, String username,
                                      String sortBy, String sortOrder) {
        this.carId = Optional.ofNullable(carId);
        this.licensePlate = Optional.ofNullable(licensePlate);
        this.visitDate = Optional.ofNullable(visitDate);
        this.repairName = Optional.ofNullable(repairName);
        this.customerId = Optional.ofNullable(customerId);
        this.username = Optional.ofNullable(username);
        this.sortBy = Optional.ofNullable(sortBy);
        this.sortOrder = Optional.ofNullable(sortOrder);
    }

    public Optional<Integer> getCarId() {
        return carId;
    }

    public void setCarId(Optional<Integer> carId) {
        this.carId = carId;
    }

    public Optional<LocalDate> getVisitDate() {
        return visitDate;
    }

    public void setVisitDate(Optional<LocalDate> visitDate) {
        this.visitDate = visitDate;
    }

    public Optional<String> getRepairName() {
        return repairName;
    }

    public void setRepairName(Optional<String> repairName) {
        this.repairName = repairName;
    }

    public Optional<Integer> getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Optional<Integer> customerId) {
        this.customerId = customerId;
    }

    public Optional<String> getSortBy() {
        return sortBy;
    }

    public void setSortBy(Optional<String> sortBy) {
        this.sortBy = sortBy;
    }

    public Optional<String> getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(Optional<String> sortOrder) {
        this.sortOrder = sortOrder;
    }

    public Optional<String> getLicensePlate() {
        return licensePlate;
    }

    public void setLicensePlate(Optional<String> licensePlate) {
        this.licensePlate = licensePlate;
    }

    public Optional<String> getUsername() {
        return username;
    }

    public void setUsername(Optional<String> username) {
        this.username = username;
    }
}
