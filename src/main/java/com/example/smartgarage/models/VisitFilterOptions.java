package com.example.smartgarage.models;

import java.util.Optional;

public class VisitFilterOptions {
    private Optional<Integer> carId;
    private Optional<String> licensePlate;
    private Optional<Integer> statusId;
    private Optional<Integer> customerId;
    private Optional<String> username;
    private Optional<String> sortBy;
    private Optional<String> sortOrder;

    public VisitFilterOptions() {
        this(null, null, null, null, null, null, null);
    }

    public VisitFilterOptions(Integer carId, String licensePlate, Integer statusId,
                              Integer customerId, String username,
                              String sortBy, String sortOrder) {
        this.carId = Optional.ofNullable(carId);
        this.licensePlate = Optional.ofNullable(licensePlate);
        this.statusId = Optional.ofNullable(statusId);
        this.customerId = Optional.ofNullable(customerId);
        this.username = Optional.ofNullable(username);
        this.sortBy = Optional.ofNullable(sortBy);
        this.sortOrder = Optional.ofNullable(sortOrder);
    }

    public Optional<Integer> getCarId() {
        return carId;
    }

    public void setCarId(Optional<Integer> carId) {
        this.carId = carId;
    }

    public Optional<Integer> getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Optional<Integer> customerId) {
        this.customerId = customerId;
    }

    public Optional<Integer> getStatusId() {
        return statusId;
    }

    public void setStatusId(Optional<Integer> statusId) {
        this.statusId = statusId;
    }

    public Optional<String> getSortBy() {
        return sortBy;
    }

    public void setSortBy(Optional<String> sortBy) {
        this.sortBy = sortBy;
    }

    public Optional<String> getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(Optional<String> sortOrder) {
        this.sortOrder = sortOrder;
    }

    public Optional<String> getLicensePlate() {
        return licensePlate;
    }

    public void setLicensePlate(Optional<String> licensePlate) {
        this.licensePlate = licensePlate;
    }

    public Optional<String> getUsername() {
        return username;
    }

    public void setUsername(Optional<String> username) {
        this.username = username;
    }
}
