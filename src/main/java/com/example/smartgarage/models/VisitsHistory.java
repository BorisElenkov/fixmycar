package com.example.smartgarage.models;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "visits_history")
public class VisitsHistory {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "visits_history_id")
    private int visitsHistoryId;

    @Column(name = "visit_id")
    private int visitId;

    @Column(name = "repair_name")
    private String repairName;

    @Column(name = "price")
    private double price;

    @Column(name = "repair_id")
    private int repairId;

    public VisitsHistory() {
    }

    public VisitsHistory(int visitsHistoryId, int visitId, String repairName, double price, int repairId) {
        this.visitsHistoryId = visitsHistoryId;
        this.visitId = visitId;
        this.repairName = repairName;
        this.price = price;
        this.repairId = repairId;
    }

    public int getVisitsHistoryId() {
        return visitsHistoryId;
    }

    public void setVisitsHistoryId(int visitsHistoryId) {
        this.visitsHistoryId = visitsHistoryId;
    }

    public int getVisitId() {
        return visitId;
    }

    public void setVisitId(int visitId) {
        this.visitId = visitId;
    }

    public String getRepairName() {
        return repairName;
    }

    public int getRepairId() {
        return repairId;
    }

    public void setRepairId(int repairId) {
        this.repairId = repairId;
    }

    public void setRepairName(String repairName) {
        this.repairName = repairName;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        VisitsHistory that = (VisitsHistory) o;
        return visitId == that.visitId && Double.compare(that.price, price) == 0 && Objects.equals(repairName, that.repairName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(visitId, repairName, price);
    }
}
