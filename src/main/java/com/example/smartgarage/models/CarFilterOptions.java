package com.example.smartgarage.models;

import java.util.Optional;

public class CarFilterOptions {
    private Optional<String> licensePlate;
    private Optional<String> vin;
    private Optional<Integer> year;
    private Optional<String> username;
    private Optional<String> modelName;
    private Optional<String> makeName;
    private Optional<String> phone;
    private Optional<String> sortBy;
    private Optional<String> orderBy;

    public CarFilterOptions() {
        this(null, null, null, null, null,
                null, null, null, null);
    }

    public CarFilterOptions(String licensePlate, String vin, Integer year, String username,
                            String modelName, String makeName, String phone, String sortBy, String orderBy) {
        this.licensePlate = Optional.ofNullable(licensePlate);
        this.vin = Optional.ofNullable(vin);
        this.year = Optional.ofNullable(year);
        this.username = Optional.ofNullable(username);
        this.modelName = Optional.ofNullable(modelName);
        this.makeName = Optional.ofNullable(makeName);
        this.phone = Optional.ofNullable(phone);
        this.sortBy = Optional.ofNullable(sortBy);
        this.orderBy = Optional.ofNullable(orderBy);
    }

    public Optional<String> getLicensePlate() {
        return licensePlate;
    }

    public Optional<String> getVin() {
        return vin;
    }

    public Optional<Integer> getYear() {
        return year;
    }

    public Optional<String> getUsername() {
        return username;
    }

    public Optional<String> getModelName() {
        return modelName;
    }

    public Optional<String> getMakeName() {
        return makeName;
    }

    public Optional<String> getPhone() {
        return phone;
    }

    public Optional<String> getSortBy() {
        return sortBy;
    }

    public Optional<String> getOrderBy() {
        return orderBy;
    }
}
