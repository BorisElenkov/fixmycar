package com.example.smartgarage.models;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "visits")
public class Visit {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "visit_id")
    private int visitId;

    @ManyToOne
    @JoinColumn(name = "car_id")
    private Car car;

    @ManyToOne
    @JoinColumn(name = "status_id")
    private Status status;

    @OneToMany(fetch = FetchType.EAGER)
    @JoinColumn(name = "visit_id")
    private Set<VisitsHistory> visitsHistory;

    @Column(name = "visit_date")
    private LocalDate visitDate;

    public Visit() {
        this.visitDate = LocalDate.now();
        this.visitsHistory = new HashSet<>();
    }

    public int getVisitId() {
        return visitId;
    }

    public void setVisitId(int visitId) {
        this.visitId = visitId;
    }

    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Set<VisitsHistory> getVisitsHistory() {
        return visitsHistory;
    }

    public void setVisitsHistory(Set<VisitsHistory> visitsHistory) {
        this.visitsHistory = visitsHistory;
    }

    public LocalDate getVisitDate() {
        return visitDate;
    }

    public void setVisitDate(LocalDate visitDate) {
        this.visitDate = visitDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Visit visit = (Visit) o;
        return visitId == visit.visitId && car.equals(visit.car) && status.equals(visit.status) && visitsHistory.equals(visit.visitsHistory) && visitDate.equals(visit.visitDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(visitId, car, status, visitsHistory, visitDate);
    }
}
