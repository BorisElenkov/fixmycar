package com.example.smartgarage.models;

import com.example.smartgarage.models.enums.CurrencyCode;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "currencies")
public class VisitCurrency {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "currency_id")
    private int currencyId;

    @Column(name = "code")
    @Enumerated(EnumType.STRING)
    private CurrencyCode currencyCode;

    public int getCurrencyId() {
        return currencyId;
    }

    public void setCurrencyId(int currencyId) {
        this.currencyId = currencyId;
    }

    public CurrencyCode getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(CurrencyCode currencyCode) {
        this.currencyCode = currencyCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        VisitCurrency currency = (VisitCurrency) o;
        return currencyId == currency.currencyId && currencyCode == currency.currencyCode;
    }

    @Override
    public int hashCode() {
        return Objects.hash(currencyId, currencyCode);
    }
}
