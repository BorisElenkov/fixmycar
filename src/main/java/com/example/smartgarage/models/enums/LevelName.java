package com.example.smartgarage.models.enums;

public enum LevelName {
    START,
    BRONZE,
    SILVER,
    GOLD;

    @Override
    public String toString() {
        switch (this) {
            case START:
                return "Start";
            case BRONZE:
                return "Bronze";
            case SILVER:
                return "Silver";
            case GOLD:
                return "Gold";
            default:
                return "Unknown level";
        }
    }
}
