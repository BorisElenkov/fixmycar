package com.example.smartgarage.models.enums;

public enum StatusName {
    NOT_STARTED,
    IN_PROGRESS,
    READY_FOR_PICKUP,
    COMPLETED,
    CANCELLED;

    @Override
    public String toString() {
        switch (this) {
            case NOT_STARTED:
                return "Not started";
            case IN_PROGRESS:
                return "In progress";
            case READY_FOR_PICKUP:
                return "Ready for pickup";
            case COMPLETED:
                return "Completed";
            case CANCELLED:
                return "Cancelled";
            default:
                return "Unknown status";
        }
    }
}
