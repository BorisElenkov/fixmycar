package com.example.smartgarage.models.enums;

public enum CurrencyCode {
    EUR,USD,JPY,GBP,AUD,CAD,CHF,CNY,SEK,MXN,NZD,SGD,HKD,NOK,KRW,BGN
}
