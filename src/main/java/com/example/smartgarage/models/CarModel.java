package com.example.smartgarage.models;

import javax.persistence.*;

@Entity
@Table(name = "models")
public class CarModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "model_id")
    private int modelId;
    @Column(name = "name")
    private String modelName;
    @ManyToOne
    @JoinColumn(name = "make_id")
    private Make make;

    public CarModel() {
    }

    public CarModel(int modelId, String modelName, Make make) {
        this.modelId = modelId;
        this.modelName = modelName;
        this.make = make;
    }

    public int getModelId() {
        return modelId;
    }

    public void setModelId(int modelId) {
        this.modelId = modelId;
    }

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public Make getMake() {
        return make;
    }

    public void setMake(Make make) {
        this.make = make;
    }
}
