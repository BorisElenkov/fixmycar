package com.example.smartgarage.models.dtos;

import java.util.ArrayList;
import java.util.List;

public class CarFormMakeDto {

    private int makeId;
    private String makeName;
    private List<CarFormModelDto> models = new ArrayList<>();

    public int getMakeId() {
        return makeId;
    }

    public void setMakeId(int makeId) {
        this.makeId = makeId;
    }

    public String getMakeName() {
        return makeName;
    }

    public void setMakeName(String makeName) {
        this.makeName = makeName;
    }

    public List<CarFormModelDto> getModels() {
        return models;
    }

    public void setModels(List<CarFormModelDto> models) {
        this.models = models;
    }
}
