package com.example.smartgarage.models.dtos;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class ModelDtoIn {
    public static final int MODEL_NAME_MIN_LENGTH = 2;
    public static final int MODEL_NAME_MAX_LENGTH = 32;
    @NotNull(message = "Model name can not be empty!")
    @Size(min = MODEL_NAME_MIN_LENGTH, max = MODEL_NAME_MAX_LENGTH,
            message = "Model name should be between 2 and 32 symbols")
    private String modelName;

    @NotNull
    private int makeId;

    public ModelDtoIn() {
    }

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public int getMakeId() {
        return makeId;
    }

    public void setMakeId(int makeId) {
        this.makeId = makeId;
    }
}
