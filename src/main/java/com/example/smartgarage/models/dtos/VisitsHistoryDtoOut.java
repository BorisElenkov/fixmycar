package com.example.smartgarage.models.dtos;

import java.time.LocalDate;

public class VisitsHistoryDtoOut {
    private int visitsHistoryId;
    private int customerId;
    private String username;
    private int carId;
    private String carLicensePlate;
    private int visitId;
    private String repair;
    private LocalDate date;

    public int getVisitsHistoryId() {
        return visitsHistoryId;
    }

    public void setVisitsHistoryId(int visitsHistoryId) {
        this.visitsHistoryId = visitsHistoryId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getCarLicensePlate() {
        return carLicensePlate;
    }

    public void setCarLicensePlate(String carLicensePlate) {
        this.carLicensePlate = carLicensePlate;
    }

    public int getVisitId() {
        return visitId;
    }

    public void setVisitId(int visitId) {
        this.visitId = visitId;
    }

    public String getRepair() {
        return repair;
    }

    public void setRepair(String repair) {
        this.repair = repair;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }
}
