package com.example.smartgarage.models.dtos;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

public class VisitDtoIn {

    @Positive
    @NotNull
    private int carId;

    public VisitDtoIn(){}

    public VisitDtoIn(int carId){
        this.carId = carId;
    }

    public int getCarId() {
        return carId;
    }

    public void setCarId(int carId) {
        this.carId = carId;
    }
}
