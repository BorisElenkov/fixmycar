package com.example.smartgarage.models.dtos;

import com.example.smartgarage.validations.constraints.Password;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class PasswordDto {
    public static final int PASSWORD_MIN_LENGTH = 8;
    public static final int PASSWORD_MAX_LENGTH = 32;
    @NotNull(message = "Password cannot be empty.")
    @Size(min = PASSWORD_MIN_LENGTH, max = PASSWORD_MAX_LENGTH,
            message = "Password should be between 8 and 32 symbols")
    @Password
    private String password;

    @NotNull(message = "Password cannot be empty.")
    @Size(min = PASSWORD_MIN_LENGTH, max = PASSWORD_MAX_LENGTH,
            message = "Password should be between 8 and 32 symbols")
    @Password
    private String confirmPassword;

    public PasswordDto() {
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }
}
