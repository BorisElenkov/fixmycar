package com.example.smartgarage.models.dtos;

import com.example.smartgarage.models.CarModel;

import java.util.ArrayList;
import java.util.List;

public class MakeDtoOut {
    private int makeId;
    private String makeName;
    private List<CarModel> models;

    public MakeDtoOut(){
        models = new ArrayList<>();
    }

    public int getMakeId() {
        return makeId;
    }

    public void setMakeId(int makeId) {
        this.makeId = makeId;
    }

    public String getMakeName() {
        return makeName;
    }

    public void setMakeName(String makeName) {
        this.makeName = makeName;
    }

    public List<CarModel> getModels() {
        return models;
    }

    public void setModels(List<CarModel> models) {
        this.models = models;
    }
}
