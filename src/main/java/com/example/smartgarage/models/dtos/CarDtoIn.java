package com.example.smartgarage.models.dtos;

import com.example.smartgarage.validations.constraints.LicensePlate;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class CarDtoIn {
    public static final int VIN_MIN_LENGTH = 17;
    public static final int VIN_MAX_LENGTH = 17;
    @NotNull
    private int modelId;
    @NotNull
    private int makeId;
    @NotNull(message = "License plate can not be empty!")
    @LicensePlate
    private String licensePlate;
    @NotNull(message = "VIN can not be empty!")
    @Size(min = VIN_MIN_LENGTH, max = VIN_MAX_LENGTH,
            message = "VIN should contain 17 symbols")
    private String vin;
    @NotNull(message = "Year can not be empty!")
    @Min(value = 1886, message = "Year should be after 1886")
    @Max(value = 2030, message = "Year should be before 2030")
    private int year;
    @NotNull
    private int userId;

    public CarDtoIn() {
    }

    public int getModelId() {
        return modelId;
    }

    public void setModelId(int modelId) {
        this.modelId = modelId;
    }

    public int getMakeId() {
        return makeId;
    }

    public void setMakeId(int makeId) {
        this.makeId = makeId;
    }

    public String getLicensePlate() {
        return licensePlate;
    }

    public void setLicensePlate(String licensePlate) {
        this.licensePlate = licensePlate;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }
}
