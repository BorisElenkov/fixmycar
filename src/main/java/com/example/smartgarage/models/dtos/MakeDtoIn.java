package com.example.smartgarage.models.dtos;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class MakeDtoIn {
    public static final int МАКЕ_NAME_MIN_LENGTH = 2;
    public static final int МАКЕ_NAME_MAX_LENGTH = 32;
    @NotNull(message = "Маке name can not be empty!")
    @Size(min = МАКЕ_NAME_MIN_LENGTH, max = МАКЕ_NAME_MAX_LENGTH,
            message = "Make name should be between 2 and 32 symbols")
    private String makeName;

    public MakeDtoIn() {
    }

    public String getMakeName() {
        return makeName;
    }

    public void setMakeName(String makeName) {
        this.makeName = makeName;
    }
}
