package com.example.smartgarage.models.dtos;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

public class UserDtoIn {

    public static final int USER_FIRST_NAME_MIN_LENGTH = 4;
    public static final int USER_FIRST_NAME_MAX_LENGTH = 32;
    public static final int USER_LAST_NAME_MIN_LENGTH = 4;
    public static final int USER_LAST_NAME_MAX_LENGTH = 32;
    public static final int PHONE_MIN_LENGTH = 4;
    public static final int PHONE_MAX_LENGTH = 32;
    public static final int EMAIL_MIN_LENGTH = 4;
    public static final int EMAIL_MAX_LENGTH = 32;

    @NotNull(message = "First name can not be empty!")
    @Size(min = USER_FIRST_NAME_MIN_LENGTH, max = USER_FIRST_NAME_MAX_LENGTH,
            message = "First name should be between 4 and 32 symbols")
    private String firstName;

    @NotNull(message = "Last name can not be empty!")
    @Size(min = USER_LAST_NAME_MIN_LENGTH, max = USER_LAST_NAME_MAX_LENGTH,
            message = "Last name should be between 4 and 32 symbols")
    private String lastName;

    @NotNull(message = "Phone can not be empty!")
    @Size(min = PHONE_MIN_LENGTH, max = PHONE_MAX_LENGTH,
            message = "Phone should be between 4 and 32 symbols")
    private String phone;

    @NotNull(message = "E-mail can not be empty!")
    @Size(min = EMAIL_MIN_LENGTH, max = EMAIL_MAX_LENGTH,
            message = "E-mail should be between 4 and 32 symbols")
    private String email;

    @NotNull
    @Positive(message = "RoleId should be positive")
    private int roleId;
    @NotNull
    @Positive(message = "levelId should be positive")
    private int levelId;

    public UserDtoIn() {
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getRoleId() {
        return roleId;
    }

    public void setRoleId(int roleId) {
        this.roleId = roleId;
    }

    public int getLevelId() {
        return levelId;
    }

    public void setLevelId(int levelId) {
        this.levelId = levelId;
    }
}
