package com.example.smartgarage.models.dtos;

import javax.validation.constraints.*;

public class RepairDtoIn {

    public static final int REPAIR_NAME_MIN_LENGTH = 2;
    public static final int REPAIR_NAME_MAX_LENGTH = 50;

    @NotNull(message = "Repair name cannot be null.")
    @Size(min = REPAIR_NAME_MIN_LENGTH, max = REPAIR_NAME_MAX_LENGTH, message = "Repair name should be between 2 and 50 symbols.")
    private String name;

    @Positive
    @Digits(integer = 10, fraction = 2, message = "Price must be a valid number.")
    @NotNull(message = "Repair price cannot be null.")
    private Double price;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }
}
