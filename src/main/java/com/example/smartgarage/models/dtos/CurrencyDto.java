package com.example.smartgarage.models.dtos;

import javax.validation.constraints.Positive;

public class CurrencyDto {

    @Positive
    private int currencyId;

    public int getCurrencyId() {
        return currencyId;
    }

    public void setCurrencyId(int currencyId) {
        this.currencyId = currencyId;
    }
}
