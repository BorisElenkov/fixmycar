package com.example.smartgarage.models;

import javax.persistence.*;

@Entity
@Table(name = "makes")
public class Make {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "make_id")
    private int makeId;

    @Column(name = "name")
    private String makeName;

    public Make() {
    }

    public Make(int makeId, String makeName) {
        this.makeId = makeId;
        this.makeName = makeName;
    }

    public int getMakeId() {
        return makeId;
    }

    public void setMakeId(int makeId) {
        this.makeId = makeId;
    }

    public String getMakeName() {
        return makeName;
    }

    public void setMakeName(String makeName) {
        this.makeName = makeName;
    }
}
