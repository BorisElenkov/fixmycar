package com.example.smartgarage.models;

import java.util.Optional;

public class RepairFilterOptions {
    private Optional<String> name;
    private Optional<Double> minPrice;
    private Optional<Double> maxPrice;
    private Optional<Boolean> isActive;
    private Optional<String> sortBy;
    private Optional<String> sortOrder;

    public RepairFilterOptions() {
        this(null, null, null, null, null, null);
    }

    public RepairFilterOptions(String name, Double minPrice, Double maxPrice,
                               Boolean isActive,
                               String sortBy, String sortOrder) {
        this.name = Optional.ofNullable(name);
        this.minPrice = Optional.ofNullable(minPrice);
        this.maxPrice = Optional.ofNullable(maxPrice);
        this.isActive = Optional.ofNullable(isActive);
        this.sortBy = Optional.ofNullable(sortBy);
        this.sortOrder = Optional.ofNullable(sortOrder);
    }

    public Optional<String> getName() {
        return name;
    }

    public void setName(Optional<String> name) {
        this.name = name;
    }

    public Optional<Double> getMinPrice() {
        return minPrice;
    }

    public void setMinPrice(Optional<Double> minPrice) {
        this.minPrice = minPrice;
    }

    public Optional<Double> getMaxPrice() {
        return maxPrice;
    }

    public Optional<Boolean> getIsActive() {
        return isActive;
    }

    public void setIsActive(Optional<Boolean> isActive) {
        this.isActive = isActive;
    }

    public void setMaxPrice(Optional<Double> maxPrice) {
        this.maxPrice = maxPrice;
    }

    public Optional<String> getSortBy() {
        return sortBy;
    }

    public void setSortBy(Optional<String> sortBy) {
        this.sortBy = sortBy;
    }

    public Optional<String> getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(Optional<String> sortOrder) {
        this.sortOrder = sortOrder;
    }
}