package com.example.smartgarage;

import com.example.smartgarage.models.*;
import com.example.smartgarage.models.enums.CurrencyCode;
import com.example.smartgarage.models.enums.LevelName;
import com.example.smartgarage.models.enums.StatusName;

import java.time.LocalDate;
import java.util.HashSet;

public class Helpers {
    public static User createMockEmployeeUser() {
        var mockUser = new User();
        mockUser.setUserId(1);
        mockUser.setFirstName("MockFirstName");
        mockUser.setLastName("MockLastName");
        mockUser.setPhone("0888112233");
        mockUser.setEmail("mock@user.com");
        mockUser.setUsername("MockUsername");
        mockUser.setPassword("MockPassword111.");
        mockUser.setRole(createMockEmployeeRole());
        mockUser.setLevel(createMockLevel());
        return mockUser;
    }

    public static User createMockCustomerUser() {
        var mockUser = createMockEmployeeUser();
        mockUser.setRole(createMockCustomerRole());
        return mockUser;
    }

    public static Role createMockEmployeeRole() {
        var mockEmployeeRole = new Role();
        mockEmployeeRole.setRoleId(1);
        mockEmployeeRole.setRoleName("Employee");
        return mockEmployeeRole;
    }

    public static Role createMockCustomerRole() {
        var mockCustomerRole = createMockEmployeeRole();
        mockCustomerRole.setRoleName("Customer");
        return mockCustomerRole;
    }

    public static Car createMockCar() {
        var mockCar = new Car();
        mockCar.setCarId(1);
        mockCar.setCarModel(createMockCarModel());
        mockCar.setLicensePlate("M1111CK");
        mockCar.setVin("MOCK123456789MOCK");
        mockCar.setYear(2000);
        mockCar.setUser(createMockCustomerUser());
        return mockCar;
    }

    public static Make createMockMake() {
        var mockMake = new Make();
        mockMake.setMakeId(1);
        mockMake.setMakeName("MockMake");
        return mockMake;
    }

    public static CarModel createMockCarModel() {
        var mockCarModel = new CarModel();
        mockCarModel.setModelId(1);
        mockCarModel.setModelName("MockModel");
        mockCarModel.setMake(createMockMake());
        return mockCarModel;
    }

    public static Level createMockLevel() {
        var mockLevel = new Level();
        mockLevel.setLevelId(1);
        mockLevel.setName(LevelName.valueOf("START"));
        mockLevel.setDiscount(0);
        mockLevel.setMinExpenditure(500.0);
        return mockLevel;
    }

    public static Repair createMockRepair() {
        var mockRepair = new Repair();
        mockRepair.setRepairId(1);
        mockRepair.setName("MockName");
        mockRepair.setActive(true);
        mockRepair.setPrice(100.0);
        return mockRepair;
    }

    public static Token createMockToken() {
        var mockToken = new Token();
        mockToken.setTokenId(1);
        mockToken.setToken("MOCK123456MOCK123456MOCK123456");
        mockToken.setUser(createMockCustomerUser());
        return mockToken;
    }

    public static Status createMockStatus() {
        var mockStatus = new Status();
        mockStatus.setStatusId(1);
        mockStatus.setName(StatusName.NOT_STARTED);
        return mockStatus;
    }

    public static Status createMockStatusCompleted() {
        var mockStatus = new Status();
        mockStatus.setStatusId(4);
        mockStatus.setName(StatusName.COMPLETED);
        return mockStatus;
    }

    public static Status createMockStatusCancelled() {
        var mockStatus = new Status();
        mockStatus.setStatusId(5);
        mockStatus.setName(StatusName.CANCELLED);
        return mockStatus;
    }

    public static Visit createMockVisit() {
        var mockVisit = new Visit();
        mockVisit.setVisitId(1);
        mockVisit.setVisitDate(LocalDate.now());
        mockVisit.setVisitsHistory(new HashSet<>());
        mockVisit.getVisitsHistory().add(createMockVisitsHistory());
        mockVisit.setCar(createMockCar());
        mockVisit.setStatus(createMockStatus());
        return mockVisit;
    }

    public static VisitsHistory createMockVisitsHistory() {
        var mockVisitsHistory = new VisitsHistory();
        mockVisitsHistory.setVisitsHistoryId(1);
        mockVisitsHistory.setVisitId(1);
        mockVisitsHistory.setRepairName("MockVisitsHistoryName");
        mockVisitsHistory.setPrice(500.0);
        mockVisitsHistory.setRepairId(1);
        return mockVisitsHistory;
    }

    public static VisitCurrency createMockCurrency() {
        var mockCurrency = new VisitCurrency();
        mockCurrency.setCurrencyId(1);
        mockCurrency.setCurrencyCode(CurrencyCode.EUR);
        return mockCurrency;
    }

    public static CarFilterOptions createMockCarFilterOptions() {
        return new CarFilterOptions(
                "M1111CK",
                "MOCK123456789MOCK",
                2000,
                "MockUsername",
                "MockMake",
                "MockModel",
                "0888112233",
                "sort",
                "order");
    }

    public static RepairFilterOptions createMockRepairFilterOptions() {
        return new RepairFilterOptions(
                "MockName",
                0.0,
                150.0,
                true,
                "sort",
                "order");
    }

    public static UserFilterOptions createMockUserFilterOptions() {
        return new UserFilterOptions(
                "MockFirstName",
                "MockLastName",
                "0888112233",
                "mock@user.com",
                "MockUsername",
                "M1111CK",
                "MOCK123456789MOCK",
                "MockModel",
                "MockMake",
                1,
                LocalDate.now(),
                LocalDate.now(),
                "sort",
                "order");
    }

    public static VisitFilterOptions createMockVisitFilterOptions() {
        return new VisitFilterOptions(
                1,
                "M1111CK",
                1,
                1,
                "MockUsername",
                "sort",
                "order");
    }

    public static VisitsHistoryFilterOptions createMockVisitHistoryFilterOptions() {
        return new VisitsHistoryFilterOptions(
                1,
                "M1111CK",
                LocalDate.now(),
                "MockName",
                1,
                "MockUsername",
                "sort",
                "order");
    }
}
