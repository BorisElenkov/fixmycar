package com.example.smartgarage.services;

import com.example.smartgarage.exceptions.AuthorizationException;
import com.example.smartgarage.exceptions.CustomException;
import com.example.smartgarage.exceptions.EntityNotFoundException;
import com.example.smartgarage.models.*;
import com.example.smartgarage.repositories.contracts.RepairRepository;
import com.example.smartgarage.repositories.contracts.StatusRepository;
import com.example.smartgarage.repositories.contracts.VisitRepository;
import com.example.smartgarage.repositories.contracts.VisitsHistoryRepository;
import com.example.smartgarage.services.contracts.UserService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static com.example.smartgarage.Helpers.*;

@ExtendWith(MockitoExtension.class)
public class VisitServiceImplTests {

    @Mock
    VisitRepository mockVisitRepository;

    @Mock
    VisitsHistoryRepository mockVisitsHistoryRepository;

    @Mock
    RepairRepository mockRepairRepository;

    @Mock
    StatusRepository mockStatusRepository;

    @Mock
    UserService mockUserService;

    @InjectMocks
    VisitServiceImpl visitService;

    @Test
    public void getVisits_Should_CallRepository_When_UserIsEmployee() {
        // Arrange
        VisitFilterOptions filterOptions = createMockVisitFilterOptions();
        User user = createMockEmployeeUser();
        Mockito.when(mockVisitRepository.getVisits(filterOptions)).thenReturn(null);
        // Act
        visitService.getVisits(filterOptions, user, Optional.empty());

        // Assert
        Mockito.verify(mockVisitRepository, Mockito.times(1)).getVisits(filterOptions);
    }

    @Test
    public void getVisits_Should_Throw_When_UserIsNotEmployee() {
        // Arrange
        VisitFilterOptions filterOptions = createMockVisitFilterOptions();
        User user = createMockCustomerUser();

        // Assert
        Assertions.assertThrows(AuthorizationException.class, () -> visitService
                .getVisits(filterOptions, user, Optional.empty()));
    }

    @Test
    public void getVisits_Should_Throw_When_UserIsNotOwner() {
        // Arrange
        VisitFilterOptions filterOptions = createMockVisitFilterOptions();
        User user = createMockCustomerUser();
        Car car = createMockCar();
        User anotherUser = createMockCustomerUser();
        anotherUser.setUserId(2);
        anotherUser.setUsername("new");
        car.setUser(anotherUser);

        // Assert
        Assertions.assertThrows(AuthorizationException.class, () -> visitService
                .getVisits(filterOptions, user, Optional.of(car)));
    }

    @Test
    public void getVisitById_Should_CallRepository() {
        // Act
        visitService.getVisitById(1);

        // Assert
        Mockito.verify(mockVisitRepository, Mockito.times(1)).getVisitById(1);
    }

    @Test
    public void getVisitById_Should_ReturnVisit_When_MatchByIdExists() {
        //Arrange
        Visit mockRepositoryVisit = createMockVisit();
        Mockito.when(mockVisitRepository.getVisitById(Mockito.anyInt())).thenReturn(mockRepositoryVisit);
        //Act
        Visit visit = visitService.getVisitById(1);

        //Assert
        Assertions.assertEquals(mockRepositoryVisit, visit);
    }

    @Test
    public void getVisitById_Should_Throw_When_UserIsNotEmployee() {
        //Arrange
        User user = createMockCustomerUser();

        // Act, Assert
        Assertions.assertThrows(AuthorizationException.class, () -> visitService
                .getVisitById(1, user, Optional.empty()));
    }

    @Test
    public void getVisitById_Should_Throw_When_UserIsNotOwner() {
        //Arrange
        User user = createMockCustomerUser();
        Car car = createMockCar();
        User anotherUser = createMockCustomerUser();
        anotherUser.setUserId(2);
        anotherUser.setUsername("new");
        car.setUser(anotherUser);

        // Act, Assert
        Assertions.assertThrows(AuthorizationException.class, () -> visitService
                .getVisitById(1, user, Optional.of(car)));
    }

    @Test
    public void getVisitById_Should_CallRepository_When_UserIsEmployee() {
        // Arrange
        User user = createMockEmployeeUser();

        // Act
        visitService.getVisitById(1, user, Optional.empty());

        // Assert
        Mockito.verify(mockVisitRepository, Mockito.times(1)).getVisitById(1);
    }

    @Test
    public void getVisitById_Should_ReturnVisit_When_MatchByIdExistsAndUserIsEmployee() {
        //Arrange
        Visit mockRepositoryVisit = createMockVisit();
        Mockito.when(mockVisitRepository.getVisitById(Mockito.anyInt())).thenReturn(mockRepositoryVisit);
        User user = createMockEmployeeUser();

        //Act
        Visit visit = visitService.getVisitById(1, user, Optional.empty());

        //Assert
        Assertions.assertEquals(mockRepositoryVisit, visit);
    }

    @Test
    public void createVisit_Should_Throw_When_UserIsNotEmployee() {
        //Arrange
        User user = createMockCustomerUser();
        Visit visit = createMockVisit();

        // Act, Assert
        Assertions.assertThrows(AuthorizationException.class, () -> visitService
                .createVisit(visit, user));
    }

    @Test
    public void createVisit_Should_CallRepository_When_UserIsEmployee() {
        //Arrange
        User user = createMockEmployeeUser();
        Visit visit = createMockVisit();

        //Act
        visitService.createVisit(visit, user);

        //Assert
        Mockito.verify(mockVisitRepository, Mockito.times(1)).createVisit(visit);
    }

    @Test
    public void addRepairToVisit_Should_Throw_When_UserIsNotEmployee() {
        //Arrange
        User user = createMockCustomerUser();

        // Assert
        Assertions.assertThrows(AuthorizationException.class, () -> visitService
                .addRepairToVisit(1, 1, user));
    }

    @Test
    public void addRepairToVisit_Should_Throw_When_VisitIsCompleted() {
        //Arrange
        User user = createMockEmployeeUser();
        Visit visit = createMockVisit();
        Status completed = createMockStatusCompleted();
        visit.setStatus(completed);
        Mockito.when(mockVisitRepository.getVisitById(1)).thenReturn(visit);

        // Act, Assert
        Assertions.assertThrows(IllegalArgumentException.class, () -> visitService
                .addRepairToVisit(1, 1, user));
    }

    @Test
    public void addRepairToVisit_Should_Throw_When_RepairIsNotActive() {
        //Arrange
        User user = createMockEmployeeUser();
        Visit visit = createMockVisit();
        Repair repair = createMockRepair();
        repair.setActive(false);
        Mockito.when(mockVisitRepository.getVisitById(1)).thenReturn(visit);
        Mockito.when(mockRepairRepository.getRepairById(1)).thenReturn(repair);

        // Act, Assert
        Assertions.assertThrows(CustomException.class, () -> visitService
                .addRepairToVisit(1, 1, user));
    }

    @Test
    public void removeRepairFromVisit_Should_Throw_When_UserIsNotEmployee() {
        //Arrange
        User user = createMockCustomerUser();

        // Act, Assert
        Assertions.assertThrows(AuthorizationException.class, () -> visitService
                .removeRepairFromVisit(1, 1, user));
    }

    @Test
    public void removeRepairFromVisit_Should_Throw_When_VisitIsCompleted() {
        //Arrange
        User user = createMockEmployeeUser();
        Visit visit = createMockVisit();
        Status completed = createMockStatusCompleted();
        visit.setStatus(completed);
        Mockito.when(mockVisitRepository.getVisitById(1)).thenReturn(visit);

        // Act, Assert
        Assertions.assertThrows(IllegalArgumentException.class, () -> visitService
                .removeRepairFromVisit(1, 1, user));
    }

    @Test
    public void removeRepairFromVisit_Should_Throw_When_RepairNotFound() {
        //Arrange
        User user = createMockEmployeeUser();
        Visit visit = createMockVisit();
        for (VisitsHistory vh : visit.getVisitsHistory()) {
            vh.setRepairId(2);
            vh.setVisitId(2);
        }
        Mockito.when(mockVisitRepository.getVisitById(1)).thenReturn(visit);

        // Act, Assert
        Assertions.assertThrows(EntityNotFoundException.class, () -> visitService
                .removeRepairFromVisit(1, 1, user));
    }

    @Test
    public void removeRepairFromVisit_Should_CallRepository_When_UserIsEmployee() {
        // Arrange
        Visit visit = createMockVisit();
        Mockito.when(mockVisitRepository.getVisitById(1)).thenReturn(visit);
        User user = createMockEmployeeUser();
        // Act
        visitService.removeRepairFromVisit(1, 1, user);

        // Assert
        Mockito.verify(mockVisitsHistoryRepository, Mockito.times(1)).delete(1, 1);
    }

    @Test
    public void changeStatus_Should_Throw_When_UserIsNotEmployee() {
        //Arrange
        User user = createMockCustomerUser();

        // Act, Assert
        Assertions.assertThrows(AuthorizationException.class, () -> visitService
                .changeStatus(1, 1, user));
    }

    @Test
    public void changeStatus_Should_Throw_When_VisitIsCompleted() {
        //Arrange
        User user = createMockEmployeeUser();
        Visit visit = createMockVisit();
        Status completed = createMockStatusCompleted();
        visit.setStatus(completed);
        Mockito.when(mockVisitRepository.getVisitById(1)).thenReturn(visit);

        // Act, Assert
        Assertions.assertThrows(IllegalArgumentException.class, () -> visitService
                .changeStatus(1, 1, user));
    }

    @Test
    public void changeStatus_Should_CallUserService_When_NewStatusIsCompleted() {
        // Arrange
        User user = createMockEmployeeUser();
        Visit visit = createMockVisit();
        Status completed = createMockStatusCompleted();
        Mockito.when(mockVisitRepository.getVisitById(1)).thenReturn(visit);
        Mockito.when(mockStatusRepository.getStatusById(1)).thenReturn(completed);

        // Act
        visitService.changeStatus(1, 1, user);

        // Assert
        Mockito.verify(mockUserService, Mockito.times(1)).reevaluateLevel(visit.getCar().getUser());
    }

    @Test
    public void changeStatus_Should_CallRepository_When_UserIsEmployee() {
        // Arrange
        User user = createMockEmployeeUser();
        Visit visit = createMockVisit();
        Status newStatus = createMockStatusCompleted();
        Mockito.when(mockVisitRepository.getVisitById(1)).thenReturn(visit);
        Mockito.when(mockStatusRepository.getStatusById(1)).thenReturn(newStatus);

        // Act
        visitService.changeStatus(1, 1, user);

        // Assert
        Mockito.verify(mockVisitRepository, Mockito.times(1)).updateVisit(visit);
    }

    @Test
    public void updateVisit_Should_Throw_When_UserIsNotEmployee() {
        //Arrange
        User user = createMockCustomerUser();
        Visit visit = createMockVisit();
        // Act, Assert
        Assertions.assertThrows(AuthorizationException.class, () -> visitService
                .updateVisit(visit, user));
    }

    @Test
    public void updateVisit_Should_Throw_When_VisitIsCompleted() {
        //Arrange
        User user = createMockEmployeeUser();
        Visit visit = createMockVisit();
        Status completed = createMockStatusCompleted();
        visit.setStatus(completed);

        // Act, Assert
        Assertions.assertThrows(IllegalArgumentException.class, () -> visitService
                .updateVisit(visit, user));
    }

    @Test
    public void updateVisit_Should_CallRepository_When_UserIsEmployee() {
        // Arrange
        User user = createMockEmployeeUser();
        Visit visit = createMockVisit();

        // Act
        visitService.updateVisit(visit, user);

        // Assert
        Mockito.verify(mockVisitRepository, Mockito.times(1)).updateVisit(visit);
    }

    @Test
    public void deleteVisit_Should_Throw_When_UserIsNotEmployee() {
        //Arrange
        User user = createMockCustomerUser();
        Visit visit = createMockVisit();
        // Act, Assert
        Assertions.assertThrows(AuthorizationException.class, () -> visitService
                .deleteVisit(visit.getVisitId(), user));
    }

    @Test
    public void deleteVisit_Should_Throw_When_VisitIsCompleted() {
        //Arrange
        User user = createMockEmployeeUser();
        Visit visit = createMockVisit();
        Status completed = createMockStatusCompleted();
        visit.setStatus(completed);
        Mockito.when(mockVisitRepository.getVisitById(visit.getVisitId())).thenReturn(visit);

        // Act, Assert
        Assertions.assertThrows(IllegalArgumentException.class, () -> visitService
                .deleteVisit(visit.getVisitId(), user));
    }

    @Test
    public void deleteVisit_Should_DeleteVisitsHistory_When_UserIsEmployee() {
        //Arrange
        User user = createMockEmployeeUser();
        Visit visit = createMockVisit();
        Mockito.when(mockVisitRepository.getVisitById(visit.getVisitId())).thenReturn(visit);

        // Act
        visitService.deleteVisit(visit.getVisitId(), user);
        // , Assert
        Mockito.verify(mockVisitsHistoryRepository, Mockito.times(1)).delete(visit.getVisitId());
    }

    @Test
    public void deleteVisit_Should_CallRepository_When_UserIsEmployee() {
        //Arrange
        User user = createMockEmployeeUser();
        Visit visit = createMockVisit();
        Mockito.when(mockVisitRepository.getVisitById(visit.getVisitId())).thenReturn(visit);

        // Act
        visitService.deleteVisit(visit.getVisitId(), user);
        // , Assert
        Mockito.verify(mockVisitRepository, Mockito.times(1)).updateVisit(visit);
    }

    @Test
    public void deleteVisit_Should_CallUserService_When_UserIsEmployee() {
        //Arrange
        User user = createMockEmployeeUser();
        Visit visit = createMockVisit();
        Mockito.when(mockVisitRepository.getVisitById(visit.getVisitId())).thenReturn(visit);

        // Act
        visitService.deleteVisit(visit.getVisitId(), user);
        // , Assert
        Mockito.verify(mockUserService, Mockito.times(1)).reevaluateLevel(visit.getCar().getUser());
    }

    @Test
    public void getTotalPrice_Should_CallRepository() {
        //Act
        visitService.getTotalPrice(1);

        // , Assert
        Mockito.verify(mockVisitRepository, Mockito.times(1)).getTotalPrice(1);
    }
}
