package com.example.smartgarage.services;

import com.example.smartgarage.exceptions.AuthorizationException;
import com.example.smartgarage.exceptions.EntityDuplicateException;
import com.example.smartgarage.exceptions.EntityNotFoundException;
import com.example.smartgarage.models.*;
import com.example.smartgarage.repositories.contracts.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;


import java.util.ArrayList;
import java.util.List;

import static com.example.smartgarage.Helpers.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class UserServiceImplTests {
    @Mock
    UserRepository mockUserRepository;
    @Mock
    TokenRepository mockTokenRepository;
    @Mock
    CarRepository mockCarRepository;
    @Mock
    VisitsHistoryRepository mockVisitsHistoryRepository;
    @Mock
    LevelRepository mockLevelRepository;
    @InjectMocks
    UserServiceImpl userService;

    @Test
    public void getUser_Should_ReturnUser_When_MatchByIdExist() {
        // Arrange
        User mockUser = createMockCustomerUser();
        when(mockUserRepository.getUserById(Mockito.anyInt()))
                .thenReturn(mockUser);
        // Act
        User result = userService.getUserById(mockUser.getUserId());

        // Assert
        Assertions.assertEquals(mockUser, result);
    }

    @Test
    public void getUser_Should_ReturnUser_When_MatchByUsernameExist() {
        // Arrange
        User mockUser = createMockCustomerUser();
        when(mockUserRepository.getUserByUsername(Mockito.anyString()))
                .thenReturn(mockUser);
        // Act
        User result = userService.getUserByUsername(mockUser.getUsername());

        // Assert
        Assertions.assertEquals(mockUser, result);
    }

    @Test
    public void getUser_Should_ReturnUser_When_MatchByEmailExist() {
        // Arrange
        User mockUser = createMockCustomerUser();
        when(mockUserRepository.getUserByEmail(Mockito.anyString()))
                .thenReturn(mockUser);
        // Act
        User result = userService.getUserByEmail(mockUser.getEmail());

        // Assert
        Assertions.assertEquals(mockUser, result);
    }

    @Test
    public void getUser_Should_ReturnUser_When_MatchByTokenExist() {
        // Arrange
        User mockUser = createMockCustomerUser();
        when(mockUserRepository.getUserByToken(Mockito.anyString()))
                .thenReturn(mockUser);
        // Act
        User result = userService.getUserByToken(createMockToken().getToken());

        // Assert
        Assertions.assertEquals(mockUser, result);
    }

    @Test
    public void getUser_Should_ReturnUser_When_Employee() {
        // Arrange
        User mockUser = createMockCustomerUser();
        User employee = createMockEmployeeUser();
        when(mockUserRepository.getUserById(Mockito.anyInt()))
                .thenReturn(mockUser);
        // Act
        User result = userService.getUserById(mockUser.getUserId(), employee);

        // Assert
        Assertions.assertEquals(mockUser, result);
    }

    @Test
    void getWithFilter_Should_CallRepository() {
        // Arrange
        UserFilterOptions filterOptions = createMockUserFilterOptions();

        // Act
        userService.getUsers(filterOptions);

        // Assert
        verify(mockUserRepository, times(1))
                .getUsers(filterOptions);
    }

    @Test
    void getWithFilter_Should_CallRepository_WhenEmployee() {
        // Arrange
        User employee = createMockEmployeeUser();
        UserFilterOptions filterOptions = createMockUserFilterOptions();

        // Act
        userService.getUsers(filterOptions, employee);

        // Assert
        verify(mockUserRepository, times(1))
                .getUsers(filterOptions);
    }

    @Test
    public void create_Should_CallRepository_When_UserWithSameNameAndEmailDoesNotExist() {
        // Arrange
        User mockUser = createMockCustomerUser();
        User mockUserEmployee = createMockEmployeeUser();

        when(mockUserRepository.getUserByUsername(mockUser.getUsername()))
                .thenThrow(EntityNotFoundException.class);
        when(mockUserRepository.getUserByEmail(mockUser.getEmail()))
                .thenThrow(EntityNotFoundException.class);
        // Act
        userService.createUser(mockUser, mockUserEmployee);

        // Assert
        verify(mockUserRepository, times(1))
                .createUser(mockUser);
    }

    @Test
    public void create_Should_ThrowException_When_UserUsernameIsTaken() {
        // Arrange
        User mockUser = createMockCustomerUser();
        User mockUserEmployee = createMockEmployeeUser();

        when(mockUserRepository.getUserByUsername(mockUser.getUsername()))
                .thenThrow(EntityDuplicateException.class);

        // Act, Assert
        Assertions.assertThrows(
                EntityDuplicateException.class,
                () -> userService.createUser(mockUser, mockUserEmployee));

    }

    @Test
    public void create_Should_ThrowException_When_UserIsNotEmployeeOrSame() {
        // Arrange
        User mockUser = createMockCustomerUser();
        User mockUserCustomer = createMockCustomerUser();

        // Act, Assert
        Assertions.assertThrows(
                AuthorizationException.class,
                () -> userService.createUser(mockUser, mockUserCustomer));
    }

    @Test
    public void create_Should_ThrowException_When_UserEmailIsTaken() {
        // Arrange
        User mockUser = createMockCustomerUser();
        User mockUserEmployee = createMockEmployeeUser();

        when(mockUserRepository.getUserByUsername(mockUser.getUsername()))
                .thenThrow(EntityNotFoundException.class);
        when(mockUserRepository.getUserByEmail(mockUser.getEmail()))
                .thenReturn(mockUser);

        // Act, Assert
        Assertions.assertThrows(
                EntityDuplicateException.class,
                () -> userService.createUser(mockUser, mockUserEmployee));
    }

    @Test
    void update_Should_CallRepository_When_UserIsCreator() {
        // Arrange
        User mockUser = createMockCustomerUser();
        User mockUserCreator = createMockEmployeeUser();

        when(mockUserRepository.getUserByUsername(Mockito.anyString()))
                .thenThrow(EntityNotFoundException.class);

        when(mockUserRepository.getUserByEmail(Mockito.anyString()))
                .thenThrow(EntityNotFoundException.class);

        // Act
        userService.updateUser(mockUser, mockUserCreator);

        // Assert
        verify(mockUserRepository, times(1))
                .updateUser(mockUser);
    }

    @Test
    void update_Should_CallRepository_When_UserIsEmployee() {
        // Arrange
        User mockUser = createMockCustomerUser();
        User mockUserEmployee = createMockEmployeeUser();

        when(mockUserRepository.getUserByUsername(Mockito.anyString()))
                .thenThrow(EntityNotFoundException.class);

        when(mockUserRepository.getUserByEmail(Mockito.anyString()))
                .thenThrow(EntityNotFoundException.class);

        // Act
        userService.updateUser(mockUser, mockUserEmployee);

        // Assert
        verify(mockUserRepository, times(1))
                .updateUser(mockUser);
    }

    @Test
    public void update_Should_ThrowException_When_UserIsNotEmployeeOrSame() {
        // Arrange
        User mockUser = createMockCustomerUser();
        User mockUserCustomer = createMockCustomerUser();

        // Act, Assert
        Assertions.assertThrows(
                AuthorizationException.class,
                () -> userService.updateUser(mockUser, mockUserCustomer));
    }

    @Test
    public void update_Should_ThrowException_When_UserNameIsTaken() {
        // Arrange
        User mockUser = createMockCustomerUser();
        User mockUserEmployee = createMockEmployeeUser();

        when(mockUserRepository.getUserByUsername(mockUser.getUsername()))
                .thenThrow(EntityDuplicateException.class);

        // Act, Assert
        Assertions.assertThrows(
                EntityDuplicateException.class,
                () -> userService.updateUser(mockUser, mockUserEmployee));
    }

    @Test
    void delete_Should_ThrowException_When_UserIsNotEmployee() {
        // Arrange
        User mockUser = createMockCustomerUser();
        User mockUserCustomer = createMockCustomerUser();

        // Act, Assert
        Assertions.assertThrows(
                AuthorizationException.class,
                () -> userService.deleteUser(mockUser.getUserId(), mockUserCustomer));
    }

    @Test
    void delete_Should_CallRepository_When_UserIsEmployee() {
        // Arrange
        User mockUser = createMockCustomerUser();
        User mockUserEmployee = createMockEmployeeUser();
        List<Car> cars = new ArrayList<>();

        // Act
        Mockito.when(mockCarRepository.getCarsByUserId(Mockito.anyInt()))
                .thenReturn(cars);
        userService.deleteUser(mockUser.getUserId(), mockUserEmployee);

        // Assert
        Mockito.verify(mockUserRepository, Mockito.times(1))
                .deleteUser(mockUser.getUserId());
    }

    @Test
    public void getUsersCount_Should_CallRepository() {
        //Arrange

        //Act
        userService.getUsersCount();

        //Assert
        verify(mockUserRepository, times(1))
                .getUsersCount();
    }

    @Test
    public void getCustomerCount_Should_CallRepository() {
        //Arrange

        //Act
        userService.getCustomerCount();

        //Assert
        verify(mockUserRepository, times(1))
                .getCustomerCount();
    }

    @Test
    public void getEmployeeCount_Should_CallRepository() {
        //Arrange

        //Act
        userService.getEmployeeCount();

        //Assert
        verify(mockUserRepository, times(1))
                .getEmployeeCount();
    }

    @Test
    public void changePassword_User_Should_CallRepository() {
        // Arrange
        int mockUserId = 123;
        User mockUser = createMockCustomerUser();
        User mockAuthentication = createMockCustomerUser();
        mockAuthentication.setUserId(mockUserId);
        String mockPassword = "Pass1234.";
        Mockito.when(mockUserRepository.getUserById(mockUserId)).thenReturn(mockUser);

        // Act
        User updatedUser = userService.changePassword(mockUserId, mockAuthentication, mockPassword);

        // Assert
        verify(mockUserRepository, times(1)).updateUser(updatedUser);

        Assertions.assertSame(mockUser, updatedUser);
    }

    @Test
    public void changePassword_Should_ThrowException_When_UserIsNotSame() {
        // Arrange
        int mockUserId = 2;
        int mockAuthenticationUserId = 3;
        User mockUser = createMockCustomerUser();
        User mockAuthentication = createMockCustomerUser();
        mockAuthentication.setUserId(mockAuthenticationUserId);
        Mockito.when(mockUserRepository.getUserById(mockUserId)).thenReturn(mockUser);

        // Act & Assert
        Assertions.assertThrows(AuthorizationException.class, () ->
                userService.changePassword(mockUserId, mockAuthentication, "Pass1234."));

        verify(mockUserRepository, never()).updateUser(any());
    }

    @Test
    public void resetForgottenPassword_Should_CallRepository() {
        // Arrange
        User mockUser = createMockCustomerUser();
        when(mockUserRepository.getUserById(mockUser.getUserId())).thenReturn(mockUser);

        // Act
        userService.resetForgottenPassword(mockUser.getUserId(), "Pass1234.");

        // Assert
        verify(mockUserRepository, times(1)).updateUser(mockUser);
        verify(mockTokenRepository, times(1)).deleteToken(mockUser.getUserId());
    }

    @Test
    public void updateToken_Should_CallRepository() {
        // Arrange
        User mockUser = createMockCustomerUser();
        Token mockToken = createMockToken();

        // Act
        Mockito.when(mockUserRepository.getUserByEmail(mockUser.getEmail()))
                .thenReturn(mockUser);
        userService.updateToken(mockToken.getToken(), mockUser.getEmail());

        // Assert
        verify(mockTokenRepository, times(1)).createToken(argThat(newToken ->
                newToken.getToken().equals(mockToken.getToken()) && newToken.getUser().equals(mockUser)
        ));
    }

    @Test
    public void reevaluateLevel_User_Should_CallRepository() {
        // Arrange
        User mockUser = createMockCustomerUser();
        Level mockLevel = createMockLevel();
        double mockExpenditure = 123.45;

        // Act
        Mockito.when(mockVisitsHistoryRepository.getTotalPaidByUserId(mockUser.getUserId())).thenReturn(mockExpenditure);
        Mockito.when(mockLevelRepository.getLevelByExpenditure(mockExpenditure)).thenReturn(mockLevel);
        userService.reevaluateLevel(mockUser);

        // Assert
        verify(mockUserRepository, times(1)).updateUser(argThat(newUser ->
                newUser.getUserId() == (mockUser.getUserId()) && newUser.getLevel().equals(mockLevel)
        ));
    }
}
