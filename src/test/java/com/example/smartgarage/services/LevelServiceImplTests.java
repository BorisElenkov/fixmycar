package com.example.smartgarage.services;

import com.example.smartgarage.models.Level;
import com.example.smartgarage.repositories.contracts.LevelRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static com.example.smartgarage.Helpers.*;

@ExtendWith(MockitoExtension.class)
public class LevelServiceImplTests {
    @Mock
    LevelRepository mockLevelRepository;

    @InjectMocks
    LevelServiceImpl levelService;

    @Test
    public void getLevels_Should_CallRepository() {
        // Act
        levelService.getLevels();

        // Assert
        Mockito.verify(mockLevelRepository, Mockito.times(1)).
                getLevels();
    }

    @Test
    public void getLevelById_Should_CallRepository() {
        // Act
        levelService.getLevelById(1);

        // Assert
        Mockito.verify(mockLevelRepository, Mockito.times(1)).getLevelById(1);
    }

    @Test
    public void getLevelById_Should_ReturnLevel_When_MatchByIdExists() {
        //Arrange
        Level mockRepositoryLevel = createMockLevel();
        Mockito.when(mockLevelRepository.getLevelById(Mockito.anyInt())).thenReturn(mockRepositoryLevel);
        //Act
        Level level = levelService.getLevelById(1);

        //Assert
        Assertions.assertEquals(mockRepositoryLevel, level);
    }
}
