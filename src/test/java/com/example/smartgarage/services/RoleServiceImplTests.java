package com.example.smartgarage.services;

import com.example.smartgarage.models.Role;
import com.example.smartgarage.repositories.contracts.RoleRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static com.example.smartgarage.Helpers.createMockCustomerRole;

@ExtendWith(MockitoExtension.class)
public class RoleServiceImplTests {

    @Mock
    RoleRepository mockRoleRepository;

    @InjectMocks
    RoleServiceImpl roleService;

    @Test
    void getRoles_Should_CallRepository() {
        // Arrange
        Mockito.when(mockRoleRepository.getRoles()).thenReturn(null);
        // Act
        roleService.getRoles();
        // Assert
        Mockito.verify(mockRoleRepository, Mockito.times(1))
                .getRoles();
    }

    @Test
    public void getRoles_Should_ReturnRole_When_MatchByIdExist() {
        // Arrange
        Role mockRole = createMockCustomerRole();
        Mockito.when(mockRoleRepository.getRoleById(Mockito.anyInt()))
                .thenReturn(mockRole);
        // Act
        Role result = roleService.getRoleById(mockRole.getRoleId());

        // Assert
        Assertions.assertEquals(mockRole, result);
    }

    @Test
    public void getRoles_Should_ReturnRole_When_MatchByNameExist() {
        // Arrange
        Role mockRole = createMockCustomerRole();
        Mockito.when(mockRoleRepository.getRoleByName(Mockito.anyString()))
                .thenReturn(mockRole);
        // Act
        Role result = roleService.getRoleByName(mockRole.getRoleName());

        // Assert
        Assertions.assertEquals(mockRole, result);
    }
}
