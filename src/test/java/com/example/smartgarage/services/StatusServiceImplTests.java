package com.example.smartgarage.services;

import com.example.smartgarage.models.Status;
import com.example.smartgarage.repositories.contracts.StatusRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static com.example.smartgarage.Helpers.*;

@ExtendWith(MockitoExtension.class)
public class StatusServiceImplTests {
    @Mock
    StatusRepository mockStatusRepository;

    @InjectMocks
    StatusServiceImpl statusService;

    @Test
    public void getStatuses_Should_CallRepository() {
        // Act
        statusService.getStatuses();

        // Assert
        Mockito.verify(mockStatusRepository, Mockito.times(1)).
                getStatuses();
    }

    @Test
    public void getStatusById_Should_CallRepository() {
        // Act
        statusService.getStatusById(1);

        // Assert
        Mockito.verify(mockStatusRepository, Mockito.times(1)).getStatusById(1);
    }

    @Test
    public void getStatusById_Should_ReturnStatus_When_MatchByIdExists() {
        //Arrange
        Status mockRepositoryStatus = createMockStatus();
        Mockito.when(mockStatusRepository.getStatusById(Mockito.anyInt())).thenReturn(mockRepositoryStatus);
        //Act
        Status status = statusService.getStatusById(1);

        //Assert
        Assertions.assertEquals(mockRepositoryStatus, status);
    }
}
