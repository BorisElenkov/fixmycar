package com.example.smartgarage.services;

import com.example.smartgarage.models.VisitCurrency;
import com.example.smartgarage.repositories.contracts.CurrencyRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static com.example.smartgarage.Helpers.*;

@ExtendWith(MockitoExtension.class)
public class CurrencyServiceImplTests {
    @Mock
    CurrencyRepository mockCurrencyRepository;

    @InjectMocks
    CurrencyServiceImpl currencyService;

    @Test
    public void getCurrencies_Should_CallRepository() {
        // Act
        currencyService.getCurrencies();

        // Assert
        Mockito.verify(mockCurrencyRepository, Mockito.times(1)).
                getCurrencies();
    }

    @Test
    public void getCurrencyById_Should_CallRepository() {
        // Act
        currencyService.getCurrencyById(1);

        // Assert
        Mockito.verify(mockCurrencyRepository, Mockito.times(1)).getCurrencyById(1);
    }

    @Test
    public void getCurrencyById_Should_ReturnCurrency_When_MatchByIdExists() {
        //Arrange
        VisitCurrency mockRepositoryCurrency = createMockCurrency();
        Mockito.when(mockCurrencyRepository.getCurrencyById(Mockito.anyInt())).thenReturn(mockRepositoryCurrency);
        //Act
        VisitCurrency currency = currencyService.getCurrencyById(1);

        //Assert
        Assertions.assertEquals(mockRepositoryCurrency, currency);
    }
}
