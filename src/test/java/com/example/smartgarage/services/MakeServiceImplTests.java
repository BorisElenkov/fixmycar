package com.example.smartgarage.services;

import com.example.smartgarage.exceptions.AuthorizationException;
import com.example.smartgarage.exceptions.EntityDuplicateException;
import com.example.smartgarage.exceptions.EntityNotFoundException;
import com.example.smartgarage.models.CarModel;
import com.example.smartgarage.models.Make;
import com.example.smartgarage.models.User;
import com.example.smartgarage.repositories.contracts.MakeRepository;
import com.example.smartgarage.repositories.contracts.ModelRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static com.example.smartgarage.Helpers.*;

@ExtendWith(MockitoExtension.class)
public class MakeServiceImplTests {
    @Mock
    MakeRepository mockMakeRepository;

    @Mock
    ModelRepository mockModelRepository;

    @InjectMocks
    MakeServiceImpl makeService;

    @Test
    void getMakes_Should_CallRepository() {
        // Arrange
        Mockito.when(mockMakeRepository.getMakes()).thenReturn(null);
        // Act
        makeService.getMakes();
        // Assert
        Mockito.verify(mockMakeRepository, Mockito.times(1))
                .getMakes();
    }

    @Test
    public void getMakes_Should_ReturnMake_When_MatchByIdExist() {
        // Arrange
        Make mockMake = createMockMake();
        Mockito.when(mockMakeRepository.getMakeById(Mockito.anyInt()))
                .thenReturn(mockMake);
        // Act
        Make result = makeService.getMakeById(mockMake.getMakeId());

        // Assert
        Assertions.assertEquals(mockMake, result);
    }

    @Test
    public void getMakes_Should_ReturnMake_When_MatchByNameExist() {
        // Arrange
        Make mockMake = createMockMake();
        Mockito.when(mockMakeRepository.getMakeByName(Mockito.anyString()))
                .thenReturn(mockMake);
        // Act
        Make result = makeService.getMakeByName(mockMake.getMakeName());

        // Assert
        Assertions.assertEquals(mockMake, result);
    }

    @Test
    public void create_Should_CallRepository_When_MakeWithSameNameDoesNotExist() {
        // Arrange
        Make mockMake = createMockMake();
        User mockUserEmployee = createMockEmployeeUser();

        Mockito.when(mockMakeRepository.getMakeByName(mockMake.getMakeName()))
                .thenThrow(EntityNotFoundException.class);
        // Act
        makeService.createMake(mockMake, mockUserEmployee);

        // Assert
        Mockito.verify(mockMakeRepository, Mockito.times(1))
                .createMake(mockMake);
    }

    @Test
    public void create_Should_Throw_When_MakeWithSameNameExists() {
        // Arrange
        Make mockMake = createMockMake();
        User mockUserEmployee = createMockEmployeeUser();

        Mockito.when(mockMakeRepository.getMakeByName(mockMake.getMakeName()))
                .thenReturn(mockMake);

        // Act, Assert
        Assertions.assertThrows(
                EntityDuplicateException.class,
                () -> makeService.createMake(mockMake, mockUserEmployee));
    }

    @Test
    void update_Should_CallRepository_When_UserIsEmployee() {
        // Arrange
        Make mockMake = createMockMake();
        User mockUserCreator = createMockEmployeeUser();

        Mockito.when(mockMakeRepository.getMakeByName(Mockito.anyString()))
                .thenThrow(EntityNotFoundException.class);

        // Act
        makeService.updateMake(mockMake, mockUserCreator);

        // Assert
        Mockito.verify(mockMakeRepository, Mockito.times(1))
                .updateMake(mockMake);
    }

    @Test
    public void update_Should_ThrowException_When_MakeNameIsTaken() {
        // Arrange
        Make mockMake = createMockMake();
        User mockUserEmployee = createMockEmployeeUser();

        Make mockExistingMakeWithSameName = createMockMake();
        mockExistingMakeWithSameName.setMakeId(2);

        Mockito.when(mockMakeRepository.getMakeByName(Mockito.anyString()))
                .thenReturn(mockExistingMakeWithSameName);

        // Act, Assert
        Assertions.assertThrows(
                EntityDuplicateException.class,
                () -> makeService.updateMake(mockMake, mockUserEmployee));
    }

    @Test
    public void update_Should_ThrowException_When_UserIsNotEmployee() {
        // Arrange
        Make mockMake = createMockMake();
        User mockUserCustomer = createMockCustomerUser();

        // Act, Assert
        Assertions.assertThrows(
                AuthorizationException.class,
                () -> makeService.updateMake(mockMake, mockUserCustomer));
    }

    @Test
    void delete_Should_CallRepository_When_UserIsEmployee() {
        // Arrange
        Make mockMake = createMockMake();
        User employee = createMockEmployeeUser();
        List<CarModel> models = new ArrayList<>();

        // Act
        Mockito.when(mockMakeRepository.getMakeById(Mockito.anyInt()))
                .thenReturn(mockMake);
        Mockito.when(mockModelRepository.getAllModelsByMake(mockMake.getMakeName()))
                .thenReturn(models);
        makeService.deleteMake(mockMake.getMakeId(), employee);

        // Assert
        Mockito.verify(mockMakeRepository, Mockito.times(1))
                .deleteMake(mockMake.getMakeId());
    }

    @Test
    void delete_Should_ThrowException_When_UserIsNotEmployee() {
        // Arrange
        Make mockMake = createMockMake();
        User customer = createMockCustomerUser();

        // Act, Assert
        Assertions.assertThrows(
                AuthorizationException.class,
                () -> makeService.deleteMake(mockMake.getMakeId(), customer));
    }


    @Test
    void delete_Should_ThrowException_When_ModelsWithThisMakeExist() {
        // Arrange
        Make mockMake = createMockMake();
        CarModel mockModel = createMockCarModel();
        User employee = createMockEmployeeUser();
        List<CarModel> models = new ArrayList<>();
        models.add(mockModel);

        // Act
        Mockito.when(mockMakeRepository.getMakeById(Mockito.anyInt()))
                .thenReturn(mockMake);
        Mockito.when(mockModelRepository.getAllModelsByMake(mockMake.getMakeName()))
                .thenReturn(models);

        // Assert
        Assertions.assertThrows(
                IllegalArgumentException.class,
                () -> makeService.deleteMake(mockMake.getMakeId(), employee));
    }
}
