package com.example.smartgarage.services;

import com.example.smartgarage.exceptions.AuthorizationException;
import com.example.smartgarage.exceptions.CustomException;
import com.example.smartgarage.exceptions.EntityDuplicateException;
import com.example.smartgarage.exceptions.EntityNotFoundException;
import com.example.smartgarage.models.*;
import com.example.smartgarage.repositories.contracts.RepairRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static com.example.smartgarage.Helpers.*;

import static com.example.smartgarage.Helpers.createMockEmployeeUser;

@ExtendWith(MockitoExtension.class)
public class RepairServiceImplTests {

    @Mock
    RepairRepository mockRepairRepository;

    @InjectMocks
    RepairServiceImpl repairService;

    @Test
    public void getRepairs_Should_CallRepository() {
        // Arrange
        RepairFilterOptions filterOptions = createMockRepairFilterOptions();

        // Act
        repairService.getRepairs(filterOptions);
        // Assert
        Mockito.verify(mockRepairRepository, Mockito.times(1)).getRepairs(filterOptions);
    }

    @Test
    public void getRepairById_Should_CallRepository() {
        // Act
        repairService.getRepairById(1);
        // Assert
        Mockito.verify(mockRepairRepository, Mockito.times(1)).getRepairById(1);
    }

    @Test
    public void getRepairById_Should_ReturnRepair_When_MatchByIdExists() {
        //Arrange
        Repair mockRepositoryRepair = createMockRepair();
        Mockito.when(mockRepairRepository.getRepairById(Mockito.anyInt())).thenReturn(mockRepositoryRepair);
        //Act
        Repair repair = repairService.getRepairById(1);

        //Assert
        Assertions.assertEquals(mockRepositoryRepair, repair);
    }

    @Test
    public void getRepairById_Should_CallRepository_When_UserIsEmployee() {
        //Arrange
        User user = createMockEmployeeUser();

        // Act
        repairService.getRepairById(1, user);
        // Assert
        Mockito.verify(mockRepairRepository, Mockito.times(1)).getRepairById(1);
    }

    @Test
    public void getRepairById_Should_ReturnRepair_When_MatchByIdExistsAndUserIsEmployee() {
        //Arrange
        User user = createMockEmployeeUser();
        Repair mockRepositoryRepair = createMockRepair();
        Mockito.when(mockRepairRepository.getRepairById(Mockito.anyInt())).thenReturn(mockRepositoryRepair);
        //Act
        Repair repair = repairService.getRepairById(1, user);

        //Assert
        Assertions.assertEquals(mockRepositoryRepair, repair);
    }

    @Test
    public void getRepairById_Should_Throw_When_UserIsNotEmployee() {
        //Arrange
        User user = createMockCustomerUser();

        // Act, Assert
        Assertions.assertThrows(AuthorizationException.class,
                () -> repairService.getRepairById(1, user));
    }

    @Test
    public void getRepairByName_Should_CallRepository() {
        // Act
        repairService.getRepairByName("mockName");
        // Assert
        Mockito.verify(mockRepairRepository, Mockito.times(1)).getRepairByName("mockName");
    }

    @Test
    public void getRepairByName_Should_ReturnRepair_When_MatchByIdExists() {
        //Arrange
        Repair mockRepositoryRepair = createMockRepair();
        Mockito.when(mockRepairRepository.getRepairByName(Mockito.anyString())).thenReturn(mockRepositoryRepair);
        //Act
        Repair repair = repairService.getRepairByName(mockRepositoryRepair.getName());

        //Assert
        Assertions.assertEquals(mockRepositoryRepair, repair);
    }

    @Test
    public void createRepair_Should_Throw_When_UserIsNotEmployee() {
        //Arrange
        User user = createMockCustomerUser();
        Repair repair = createMockRepair();

        // Act, Assert
        Assertions.assertThrows(AuthorizationException.class,
                () -> repairService.createRepair(repair, user));
    }

    @Test
    public void create_Should_Throw_When_RepairWithSameNameExists() {
        //Arrange
        Repair mockRepositoryRepair = createMockRepair();
        Repair newRepair = createMockRepair();
        newRepair.setRepairId(2);
        newRepair.setPrice(50.0);
        User user = createMockEmployeeUser();
        Mockito.when(mockRepairRepository.getRepairByName(Mockito.anyString())).thenReturn(mockRepositoryRepair);

        //Act, Assert
        Assertions.assertThrows(EntityDuplicateException.class, () -> repairService.createRepair(newRepair, user));
    }

    @Test
    public void createRepair_Should_CallRepository_When_RepairWithSameNameDoesNotExist() {
        //Arrange
        User user = createMockEmployeeUser();
        Repair repair = createMockRepair();
        Mockito.when(mockRepairRepository.getRepairByName(Mockito.anyString()))
                .thenThrow(EntityNotFoundException.class);

        //Act
        repairService.createRepair(repair, user);

        //Assert
        Mockito.verify(mockRepairRepository, Mockito.times(1)).createRepair(repair);
    }

    @Test
    public void updateRepair_Should_Throw_When_UserIsNotEmployee() {
        //Arrange
        User user = createMockCustomerUser();
        Repair repair = createMockRepair();

        // Act, Assert
        Assertions.assertThrows(AuthorizationException.class,
                () -> repairService.updateRepair(repair, user));
    }

    @Test
    public void update_Should_Throw_When_RepairWithSameNameExists() {
        //Arrange
        Repair mockRepositoryRepair = createMockRepair();
        Repair newRepair = createMockRepair();
        newRepair.setRepairId(2);
        newRepair.setPrice(50.0);
        User user = createMockEmployeeUser();
        Mockito.when(mockRepairRepository.getRepairByName(Mockito.anyString())).thenReturn(mockRepositoryRepair);

        //Act, Assert
        Assertions.assertThrows(EntityDuplicateException.class, () -> repairService.updateRepair(newRepair, user));
    }

    @Test
    public void updateRepair_Should_CallRepository_When_RepairWithSameNameDoesNotExist() {
        //Arrange
        User user = createMockEmployeeUser();
        Repair repair = createMockRepair();
        Mockito.when(mockRepairRepository.getRepairByName(Mockito.anyString()))
                .thenThrow(EntityNotFoundException.class);

        //Act
        repairService.updateRepair(repair, user);

        //Assert
        Mockito.verify(mockRepairRepository, Mockito.times(1)).updateRepair(repair);
    }

    @Test
    public void update_Should_Throw_When_RepairIsInactive() {
        //Arrange
        User user = createMockEmployeeUser();
        Repair repair = createMockRepair();
        repair.setActive(false);
        Mockito.when(mockRepairRepository.getRepairByName(Mockito.anyString()))
                .thenThrow(EntityNotFoundException.class);

        //Act, Assert
        Assertions.assertThrows(CustomException.class, () -> repairService.updateRepair(repair, user));
    }

    @Test
    public void deactivateRepair_Should_Throw_When_UserIsNotEmployee() {
        //Arrange
        User user = createMockCustomerUser();

        // Act, Assert
        Assertions.assertThrows(AuthorizationException.class,
                () -> repairService.deactivateRepair(1, user));
    }

    @Test
    public void deactivateRepair_Should_CallRepository_When_UserIsEmployee() {
        //Arrange
        User user = createMockEmployeeUser();
        Repair repair = createMockRepair();
        Mockito.when(mockRepairRepository.getRepairById(Mockito.anyInt()))
                .thenReturn(repair);

        //Act
        repairService.deactivateRepair(repair.getRepairId(), user);

        //Assert
        Mockito.verify(mockRepairRepository, Mockito.times(1)).updateRepair(repair);
    }

    @Test
    public void activateRepair_Should_Throw_When_UserIsNotEmployee() {
        //Arrange
        User user = createMockCustomerUser();

        // Act, Assert
        Assertions.assertThrows(AuthorizationException.class,
                () -> repairService.activateRepair(1, user));
    }

    @Test
    public void activateRepair_Should_CallRepository_When_UserIsEmployee() {
        //Arrange
        User user = createMockEmployeeUser();
        Repair repair = createMockRepair();
        Mockito.when(mockRepairRepository.getRepairById(Mockito.anyInt()))
                .thenReturn(repair);

        //Act
        repairService.activateRepair(repair.getRepairId(), user);

        //Assert
        Mockito.verify(mockRepairRepository, Mockito.times(1)).updateRepair(repair);
    }
}
