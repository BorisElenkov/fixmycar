package com.example.smartgarage.services;

import com.example.smartgarage.exceptions.AuthorizationException;
import com.example.smartgarage.models.Car;
import com.example.smartgarage.models.User;
import com.example.smartgarage.models.VisitsHistoryFilterOptions;
import com.example.smartgarage.repositories.contracts.VisitsHistoryRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static com.example.smartgarage.Helpers.*;

import static com.example.smartgarage.Helpers.createMockEmployeeUser;

@ExtendWith(MockitoExtension.class)
public class VisitsHistoryImplTests {
    @Mock
    VisitsHistoryRepository mockVisitsHistoryRepository;

    @InjectMocks
    VisitsHistoryServiceImpl visitsHistoryService;

    @Test
    public void getVisitsHistory_Should_CallRepository_When_UserIsEmployee() {
        // Arrange
        VisitsHistoryFilterOptions filterOptions = createMockVisitHistoryFilterOptions();
        User user = createMockEmployeeUser();
        Mockito.when(mockVisitsHistoryRepository.getVisitsHistory(filterOptions)).thenReturn(null);
        // Act
        visitsHistoryService.getVisitsHistory(filterOptions, user, Optional.empty());

        // Assert
        Mockito.verify(mockVisitsHistoryRepository, Mockito.times(1)).getVisitsHistory(filterOptions);
    }

    @Test
    public void getVisitsHistory_Should_Throw_When_UserIsNotEmployee() {
        // Arrange
        VisitsHistoryFilterOptions filterOptions = createMockVisitHistoryFilterOptions();
        User user = createMockCustomerUser();

        // Assert
        Assertions.assertThrows(AuthorizationException.class, () -> visitsHistoryService
                .getVisitsHistory(filterOptions, user, Optional.empty()));
    }

    @Test
    public void getVisits_Should_Throw_When_UserIsNotOwner() {
        // Arrange
        VisitsHistoryFilterOptions filterOptions = createMockVisitHistoryFilterOptions();
        User user = createMockCustomerUser();
        Car car = createMockCar();
        User anotherUser = createMockCustomerUser();
        anotherUser.setUserId(2);
        anotherUser.setUsername("new");
        car.setUser(anotherUser);

        // Assert
        Assertions.assertThrows(AuthorizationException.class, () -> visitsHistoryService
                .getVisitsHistory(filterOptions, user, Optional.of(car)));
    }
}
