package com.example.smartgarage.services;

import com.example.smartgarage.exceptions.AuthorizationException;
import com.example.smartgarage.exceptions.EntityDuplicateException;
import com.example.smartgarage.exceptions.EntityNotFoundException;
import com.example.smartgarage.models.Car;
import com.example.smartgarage.models.CarFilterOptions;
import com.example.smartgarage.models.User;
import com.example.smartgarage.repositories.contracts.CarRepository;
import com.example.smartgarage.repositories.contracts.VisitRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static com.example.smartgarage.Helpers.*;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
public class CarServiceImplTests {
    @Mock
    CarRepository mockCarRepository;

    @Mock
    VisitRepository mockVisitRepository;

    @InjectMocks
    CarServiceImpl carService;

    @Test
    void getCars_Should_CallRepository() {
        // Arrange
        CarFilterOptions filterOptions = createMockCarFilterOptions();
        User mockUserEmployee = createMockEmployeeUser();

        // Act
        carService.getCars(filterOptions, mockUserEmployee);

        // Assert
        verify(mockCarRepository, Mockito.times(1))
                .getCars(filterOptions);
    }

    @Test
    void getCarsByUserId_Should_CallRepository() {
        // Arrange
        User mockUserCustomer = createMockCustomerUser();

        // Act
        carService.getCarsByUserId(mockUserCustomer.getUserId());

        // Assert
        verify(mockCarRepository, Mockito.times(1))
                .getCarsByUserId(mockUserCustomer.getUserId());
    }

    @Test
    public void getCar_Should_ReturnCar_When_MatchByIdExist() {
        // Arrange
        Car mockCar = createMockCar();
        Mockito.when(mockCarRepository.getCarById(Mockito.anyInt()))
                .thenReturn(mockCar);

        // Act
        Car result = carService.getCarById(mockCar.getCarId());

        // Assert
        Assertions.assertEquals(mockCar, result);
    }

    @Test
    public void getCarById_Should_ReturnCar_When_UserIsEmployee() {
        // Arrange
        Car mockCar = createMockCar();
        User mockUserEmployee = createMockEmployeeUser();
        Mockito.when(mockCarRepository.getCarById(Mockito.anyInt()))
                .thenReturn(mockCar);

        // Act
        Car result = carService.getCarById(mockCar.getCarId(), mockUserEmployee);

        // Assert
        Assertions.assertEquals(mockCar, result);
    }

    @Test
    public void getCarByLicensePlate_Should_ReturnCar_When_MatchExist() {
        // Arrange
        Car mockCar = createMockCar();
        Mockito.when(mockCarRepository.getCarByLicensePlate(Mockito.anyString()))
                .thenReturn(mockCar);

        // Act
        Car result = carService.getCarByLicensePlate(mockCar.getLicensePlate());

        // Assert
        Assertions.assertEquals(mockCar, result);
    }

    @Test
    public void create_Should_CallRepository_When_CarWithSameNameDoesNotExist() {
        // Arrange
        Car mockCar = createMockCar();
        User mockUserEmployee = createMockEmployeeUser();

        Mockito.when(mockCarRepository.getCarByLicensePlate(mockCar.getLicensePlate()))
                .thenThrow(EntityNotFoundException.class);

        // Act
        carService.createCar(mockCar, mockUserEmployee);

        // Assert
        verify(mockCarRepository, Mockito.times(1))
                .createCar(mockCar);
    }

    @Test
    public void create_Should_Throw_When_CarWithSameNameExists() {
        // Arrange
        Car mockCar = createMockCar();
        User mockUserEmployee = createMockEmployeeUser();

        Mockito.when(mockCarRepository.getCarByLicensePlate(mockCar.getLicensePlate()))
                .thenReturn(mockCar);

        // Act, Assert
        Assertions.assertThrows(EntityDuplicateException.class,
                () -> carService.createCar(mockCar, mockUserEmployee));
    }

    @Test
    public void create_Should_Throw_When_CreateCarForEmployee() {
        // Arrange
        Car mockCar = createMockCar();
        User mockUserEmployee = createMockEmployeeUser();
        mockCar.setUser(mockUserEmployee);

        Mockito.when(mockCarRepository.getCarByLicensePlate(mockCar.getLicensePlate()))
                .thenThrow(EntityNotFoundException.class);

        // Act, Assert
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> carService.createCar(mockCar, mockUserEmployee));
    }

    @Test
    void update_Should_CallRepository_When_UserIsEmployee() {
        // Arrange
        Car mockCar = createMockCar();
        User mockUserCreator = createMockEmployeeUser();

        Mockito.when(mockCarRepository.getCarByLicensePlate(Mockito.anyString()))
                .thenThrow(EntityNotFoundException.class);

        // Act
        carService.updateCar(mockCar, mockUserCreator);

        // Assert
        verify(mockCarRepository, Mockito.times(1))
                .updateCar(mockCar);
    }

    @Test
    public void update_Should_ThrowException_When_CarLicensePlateIsTaken() {
        // Arrange
        Car mockCar = createMockCar();
        User mockUserEmployee = createMockEmployeeUser();

        Car mockExistingCarWithSameLicensePlate = createMockCar();
        mockExistingCarWithSameLicensePlate.setCarId(2);

        Mockito.when(mockCarRepository.getCarByLicensePlate(Mockito.anyString()))
                .thenReturn(mockExistingCarWithSameLicensePlate);

        // Act, Assert
        Assertions.assertThrows(
                EntityDuplicateException.class,
                () -> carService.updateCar(mockCar, mockUserEmployee));
    }

    @Test
    public void update_Should_ThrowException_When_UserIsNotEmployee() {
        // Arrange
        Car mockCar = createMockCar();
        User mockUserCustomer = createMockCustomerUser();

        // Act, Assert
        Assertions.assertThrows(
                AuthorizationException.class,
                () -> carService.updateCar(mockCar, mockUserCustomer));
    }

    @Test
    void delete_Should_CallRepository_When_UserIsEmployee() {
        // Arrange
        Car mockCar = createMockCar();
        User employee = createMockEmployeeUser();

        // Act
        mockVisitRepository.deleteVisits(mockCar.getCarId());
        carService.deleteCar(mockCar.getCarId(), employee);

        // Assert
        Mockito.verify(mockCarRepository, Mockito.times(1))
                .deleteCar(1);
    }

    @Test
    void delete_Should_ThrowException_When_UserIsNotEmployee() {
        // Arrange
        Car mockCar = createMockCar();
        User customer = createMockCustomerUser();

        // Act, Assert
        Assertions.assertThrows(
                AuthorizationException.class,
                () -> carService.deleteCar(mockCar.getCarId(), customer));
    }

    @Test
    public void getCarCount_Should_CallRepository() {
        //Arrange

        //Act
        carService.getCarCount();

        //Assert
        verify(mockCarRepository, Mockito.times(1))
                .getCarCount();
    }
}
