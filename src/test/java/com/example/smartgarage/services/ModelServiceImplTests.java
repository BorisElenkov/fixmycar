package com.example.smartgarage.services;

import com.example.smartgarage.exceptions.AuthorizationException;
import com.example.smartgarage.exceptions.EntityDuplicateException;
import com.example.smartgarage.exceptions.EntityNotFoundException;
import com.example.smartgarage.models.Car;
import com.example.smartgarage.models.CarModel;
import com.example.smartgarage.models.Make;
import com.example.smartgarage.models.User;
import com.example.smartgarage.repositories.contracts.CarRepository;
import com.example.smartgarage.repositories.contracts.ModelRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static com.example.smartgarage.Helpers.*;

@ExtendWith(MockitoExtension.class)
public class ModelServiceImplTests {
    @Mock
    ModelRepository mockModelRepository;

    @Mock
    CarRepository mockCarRepository;

    @InjectMocks
    ModelServiceImpl modelService;

    @Test
    void getModels_Should_CallRepository() {
        // Arrange
        Mockito.when(mockModelRepository.getModels()).thenReturn(null);
        // Act
        modelService.getModels();
        // Assert
        Mockito.verify(mockModelRepository, Mockito.times(1))
                .getModels();
    }

    @Test
    void getModelsByMake_Should_CallRepository() {
        // Arrange
        Make mockMake = createMockMake();
        Mockito.when(mockModelRepository.getAllModelsByMake(mockMake.getMakeName())).thenReturn(null);
        // Act
        modelService.getAllModelsByMake(mockMake.getMakeName());
        // Assert
        Mockito.verify(mockModelRepository, Mockito.times(1))
                .getAllModelsByMake(mockMake.getMakeName());
    }

    @Test
    public void getModels_Should_ReturnModel_When_MatchByIdExist() {
        // Arrange
        CarModel mockModel = createMockCarModel();
        Mockito.when(mockModelRepository.getModelById(Mockito.anyInt()))
                .thenReturn(mockModel);
        // Act
        CarModel result = modelService.getModelById(mockModel.getModelId());

        // Assert
        Assertions.assertEquals(mockModel, result);
    }

    @Test
    public void getModelByMakeId_Should_ReturnModel_When_MatchByIdExist() {
        // Arrange
        CarModel mockModel = createMockCarModel();
        Make mockMake = createMockMake();
        Mockito.when(mockModelRepository.getModelById(Mockito.anyInt(), Mockito.anyInt()))
                .thenReturn(mockModel);
        // Act
        CarModel result = modelService.getModelById(mockMake.getMakeId(), mockModel.getModelId());

        // Assert
        Assertions.assertEquals(mockModel, result);
    }

    @Test
    public void getModels_Should_ReturnModel_When_MatchByNameExist() {
        // Arrange
        CarModel mockModel = createMockCarModel();
        Mockito.when(mockModelRepository.getModelByName(Mockito.anyString()))
                .thenReturn(mockModel);
        // Act
        CarModel result = modelService.getModelByName(mockModel.getModelName());

        // Assert
        Assertions.assertEquals(mockModel, result);
    }

    @Test
    public void create_Should_CallRepository_When_ModelWithSameNameDoesNotExist() {
        // Arrange
        CarModel mockModel = createMockCarModel();
        User mockUserEmployee = createMockEmployeeUser();

        Mockito.when(mockModelRepository.getModelByName(mockModel.getModelName()))
                .thenThrow(EntityNotFoundException.class);
        // Act
        modelService.createModel(mockModel, mockUserEmployee);

        // Assert
        Mockito.verify(mockModelRepository, Mockito.times(1))
                .createModel(mockModel);
    }

    @Test
    public void create_Should_Throw_When_ModelWithSameNameExists() {
        // Arrange
        CarModel mockModel = createMockCarModel();
        User mockUserEmployee = createMockEmployeeUser();

        Mockito.when(mockModelRepository.getModelByName(mockModel.getModelName()))
                .thenReturn(mockModel);

        // Act, Assert
        Assertions.assertThrows(
                EntityDuplicateException.class,
                () -> modelService.createModel(mockModel, mockUserEmployee));
    }

    @Test
    void update_Should_CallRepository_When_ModelIsEmployee() {
        // Arrange
        CarModel mockModel = createMockCarModel();
        User mockUserCreator = createMockEmployeeUser();

        Mockito.when(mockModelRepository.getModelByName(Mockito.anyString()))
                .thenThrow(EntityNotFoundException.class);

        // Act
        modelService.updateModel(mockUserCreator, mockModel);

        // Assert
        Mockito.verify(mockModelRepository, Mockito.times(1))
                .updateModel(mockModel);
    }

    @Test
    public void update_Should_ThrowException_When_ModelNameIsTaken() {
        // Arrange
        CarModel mockModel = createMockCarModel();
        User mockUserEmployee = createMockEmployeeUser();

        CarModel mockExistingModelWithSameName = createMockCarModel();
        mockExistingModelWithSameName.setModelId(2);

        Mockito.when(mockModelRepository.getModelByName(Mockito.anyString()))
                .thenReturn(mockExistingModelWithSameName);

        // Act, Assert
        Assertions.assertThrows(
                EntityDuplicateException.class,
                () -> modelService.updateModel(mockUserEmployee, mockModel));
    }

    @Test
    public void update_Should_ThrowException_When_UserIsNotEmployee() {
        // Arrange
        CarModel mockModel = createMockCarModel();
        User mockUserCustomer = createMockCustomerUser();

        // Act, Assert
        Assertions.assertThrows(
                AuthorizationException.class,
                () -> modelService.updateModel(mockUserCustomer, mockModel));
    }

    @Test
    void delete_Should_ThrowException_When_CarWithThisModelExist() {
        // Arrange
        CarModel mockModel = createMockCarModel();
        Car mockCar = createMockCar();
        User employee = createMockEmployeeUser();
        List<Car> cars = new ArrayList<>();
        cars.add(mockCar);

        // Act
        Mockito.when(mockCarRepository.getCarsByModelId(mockModel.getModelId()))
                .thenReturn(cars);

        // Assert
        Assertions.assertThrows(
                IllegalArgumentException.class,
                () -> modelService.deleteModel(mockModel.getModelId(), employee));
    }

    @Test
    void delete_Should_ThrowException_When_UserIsNotEmployee() {
        // Arrange
        CarModel mockModel = createMockCarModel();
        User customer = createMockCustomerUser();

        // Act, Assert
        Assertions.assertThrows(
                AuthorizationException.class,
                () -> modelService.deleteModel(mockModel.getModelId(), customer));
    }

    @Test
    void delete_Should_CallRepository_When_UserIsEmployee() {
        // Arrange
        CarModel mockModel = createMockCarModel();
        User employee = createMockEmployeeUser();
        List<Car> cars = new ArrayList<>();

        // Act
        Mockito.when(mockCarRepository.getCarsByModelId(Mockito.anyInt()))
                .thenReturn(cars);

        modelService.deleteModel(mockModel.getModelId(), employee);

        // Assert
        Mockito.verify(mockModelRepository, Mockito.times(1))
                .deleteModel(mockModel.getModelId());
    }
}
